 //Remove patient function
        
        function remove_patient(pt_num){
             
            if (confirm('This action will delete patient')){
                    $.ajax({
                        url:"deactivate_pt/"+pt_num, 
                        success: function(result){
                        $("#msg").html('<div class="alert alert-info">Patient number with  number '+pt_num+' removed from records</div>');
                        $('#tr_'+pt_num).fadeOut(200);
                        },
                        error: function(error){
                          $('#msg').html("<div class='alert alert-info'>Sorry</div>");
                        }
                    });
            }
        }
        
        
        //This function sets a doctors request to completed
        
        function set_lab_request_as_completed(request_id){
                    $.ajax({
                        url:"request_completed/"+request_id, 
                        success: function(result){
                        $("#msg").html('<div class="alert alert-info">Request  number '+request_id+' accepted</div>');
                        $('#request_'+request_id).fadeOut(500);
                        },
                        error: function(error){
                          $('#msg').html("<div class='alert alert-info'>Sorry</div>");
                          console.log('Opss cudnt set report as active');
                        }
                    });
        }
        
        //Remove patient function
        
        function save_lab_request(request_id){
         $('#lab_report_requested').submit(function(event){
                event.preventDefault();
           
                    $.ajax({
                        url: "save_report/",
                        type: 'POST',
                        data:{
                            patient_num:$('#patient_num').val(),
                            requesting_doctor:$('#requesting_doctor').val(), 
                            report_type:$('#report_type').val(),
                            
                            result_description:$('#result_description').val(),
                            recommendation:$('#recommendation').val(),
                            save_report:$('#save_report').val(),
                            attending_technician_id:$('#attending_technician_id').val()
                        },
                        beforeSend: function(){
                                    $("#form-inputs").slideUp();
                                     $("button.btn").html("Saving...");
                                   },
                        success: function(result){
                        if(result=='successfull'){
                            $("#msg").html('<div class="alert alert-info">Lab report saved successfully</div>');
                            $("button.btn").html("Successfull");
                            
                                setTimeout(function(){
                                $('.modal').modal('hide');
                                $("#form-inputs").slideDown();
                                $("button.btn").html("Save");
                                
                                set_lab_request_as_completed($('#request_id').val())
                              }, 1500);
                            
                            
                            }else{
                            console.log(result);
                            $("#msg").html(result);
                            $("#form-inputs").slideDown();
                        }
                        },
                        error: function(error){
                            $("#form-inputs").slideDown();
                            $("button.btn").html("Save");
                          $('#msg').html("<div class='alert alert-info'>Sorry</div>");
                        }
                    });
            
          });
        }
        
        function mark_patient_as_present(patient_number){
            $.ajax({
                        url:"receptionist/attendance/"+patient_number, 
                        success: function(result){
                            if(result=='Marked'){
                                $("#msg").html('<div class="alert alert-success"><i class="fa fa-thumbs-o-up"></i> Patient  number '+patient_number+' has been added to the attendance table for today</div>');
                                console.log('Alright! Patient hass been marked present')  
                            }else{
                                $("#msg").html('<div class="alert alert-danger"><i class="fa fa-thumbs-o-up"></i> Oopss! Patient  number '+patient_number+' could not be added to the attendance table for today</div>');
                                console.log('Sorry! Patient marking failed. Buh Url is on point')  
                            }   
                        
                        },
                        error: function(error){
                          $('#msg').html("<div class='alert alert-danger'>Sorry</div>");
                          console.log('Opss cudnt set report as active');
                        }
                    });
        }
        
        function print_button(message){
            if (window.print) {
                document.write('<button class="btn btn-info" type=button name=print value="Print" onClick="window.print()"><i class="fa fa-print" aria-hidden="true"></i> '+message+'</button>');
            }else{
                document.write('<div class="alert alert-warning"> Your Browser does not support print </div>');
            }
        }
        
        
        function get_attendance(){
            var datex= $('#date_range').val();
            $.ajax({
                        url:"get_attendance/",
                        type: 'POST',
                        data:{
                            datex:datex,
                        },
                        success: function(result){
                        $('#attendance_table').html(result).fadeIn(500);
                        console.log(datex);
                        },
                        error: function(error){
                          $('#msg').html("<div class='alert alert-info'>Sorry</div>");
                          console.log('Opss an error occured');
                        }
                    });
        }
       