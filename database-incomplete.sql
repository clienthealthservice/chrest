/*
Navicat MySQL Data Transfer

Source Server         : Localhost-MYSQL
Source Server Version : 100113
Source Host           : 127.0.0.1:3306
Source Database       : chrest

Target Server Type    : MYSQL
Target Server Version : 100113
File Encoding         : 65001

Date: 2017-06-30 09:37:36
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for activity_log
-- ----------------------------
DROP TABLE IF EXISTS `activity_log`;
CREATE TABLE `activity_log` (
  `pk_act_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `activity` text NOT NULL,
  `time` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`pk_act_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of activity_log
-- ----------------------------
INSERT INTO `activity_log` VALUES ('1', 'New patient added', '2017-06-10 17:45:03');
INSERT INTO `activity_log` VALUES ('2', 'New patient added', '2017-06-10 17:53:22');
INSERT INTO `activity_log` VALUES ('3', 'New patient added', '2017-06-10 18:03:24');
INSERT INTO `activity_log` VALUES ('4', 'New patient added', '2017-06-10 18:05:41');
INSERT INTO `activity_log` VALUES ('5', 'New patient added', '2017-06-10 18:07:14');

-- ----------------------------
-- Table structure for attendance_patients
-- ----------------------------
DROP TABLE IF EXISTS `attendance_patients`;
CREATE TABLE `attendance_patients` (
  `fk_pt_number` varchar(32) NOT NULL,
  `date_of_attendance` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of attendance_patients
-- ----------------------------

-- ----------------------------
-- Table structure for nhis
-- ----------------------------
DROP TABLE IF EXISTS `nhis`;
CREATE TABLE `nhis` (
  `pk_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `nhis_number` varchar(32) NOT NULL,
  `fk_pt_number` varchar(32) NOT NULL,
  `nhis_next_renewal` date NOT NULL,
  `nhis_expiry_date` date NOT NULL,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`pk_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of nhis
-- ----------------------------

-- ----------------------------
-- Table structure for patients_next_of_kin
-- ----------------------------
DROP TABLE IF EXISTS `patients_next_of_kin`;
CREATE TABLE `patients_next_of_kin` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `patient_number` varchar(32) NOT NULL,
  `name` varchar(52) NOT NULL,
  `occupation` varchar(32) DEFAULT NULL,
  `residence` varchar(62) DEFAULT NULL,
  `nationality` varchar(84) DEFAULT NULL,
  `phone_number` int(25) DEFAULT NULL,
  `address` text,
  `date_of_birth` date NOT NULL,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of patients_next_of_kin
-- ----------------------------
INSERT INTO `patients_next_of_kin` VALUES ('1', 'Aa1390648', 'TEsds', 'dasddfa', 'alsdasda', 'jdkasd', '2147483647', 'aklsdasl', '2017-06-10', '2017-06-10 18:05:41', '2017-06-10 18:05:41');
INSERT INTO `patients_next_of_kin` VALUES ('2', 'jK8619542', 'AASDS', 'JKNJKSNKSJ', 'ajksdask', 'ASKDJCDS', '2147483647', 'JKASJKDSD', '2017-06-10', '2017-06-10 18:07:14', '2017-06-10 18:07:14');

-- ----------------------------
-- Table structure for patient_info
-- ----------------------------
DROP TABLE IF EXISTS `patient_info`;
CREATE TABLE `patient_info` (
  `pk_pt_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `pt_number` varchar(32) NOT NULL,
  `pt_first_name` varchar(32) NOT NULL,
  `pt_last_name` varchar(32) NOT NULL,
  `pt_middle_name` varchar(32) DEFAULT NULL,
  `pt_occupation` varchar(32) DEFAULT NULL,
  `pt_place_of_birth` varchar(62) DEFAULT NULL,
  `pt_place_of_residence` varchar(62) DEFAULT NULL,
  `pt_nationality` varchar(84) DEFAULT NULL,
  `pt_first_attendance` date DEFAULT NULL,
  `pt_image_name` varchar(225) DEFAULT NULL,
  `pt_image_thumb_name` varchar(225) DEFAULT NULL,
  `pt_mobile` int(25) DEFAULT NULL,
  `pt_address` text,
  `pt_date_of_birth` date NOT NULL,
  `pt_gender` enum('male','female') NOT NULL,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`pk_pt_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of patient_info
-- ----------------------------
INSERT INTO `patient_info` VALUES ('1', 'Aa6381702', 'Andrew', 'asdaklsdasl', 'Asdasdf', 'Sdss', 'dssssjew', 'alsasasi', 'jdkasd', '2017-06-10', null, null, '2556548', 'sasdakls', '2017-06-10', 'male', '2017-06-10 18:03:24', '2017-06-10 18:03:24');
INSERT INTO `patient_info` VALUES ('2', 'Aa1390648', 'Andrew', 'asdaklsdasl', 'Asdasdf', 'Sdss', 'dssssjew', 'alsasasi', 'jdkasd', '2017-06-10', null, null, '2556548', 'sasdakls', '2017-06-10', 'male', '2017-06-10 18:05:41', '2017-06-10 18:05:41');

-- ----------------------------
-- Table structure for staff
-- ----------------------------
DROP TABLE IF EXISTS `staff`;
CREATE TABLE `staff` (
  `pk_staff_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `staff_first_name` varchar(32) NOT NULL,
  `staff_last_name` varchar(32) NOT NULL,
  `staff_username` varchar(32) NOT NULL,
  `staff_email` varchar(225) NOT NULL,
  `staff_password` varchar(225) NOT NULL,
  `staff_image_name` varchar(225) DEFAULT NULL,
  `staff_image_thumb_name` varchar(225) DEFAULT NULL,
  `staff_phone_number` int(16) DEFAULT NULL,
  `access_level` smallint(6) NOT NULL,
  `staff_role` varchar(32) NOT NULL,
  `active` enum('0','1') DEFAULT '0',
  PRIMARY KEY (`pk_staff_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of staff
-- ----------------------------
INSERT INTO `staff` VALUES ('1', 'Admin', 'User', 'admin', 'admin@mail.com', 'd033e22ae348aeb5660fc2140aec35850c4da997', null, null, '267979001', '1', 'administrator', '1');
INSERT INTO `staff` VALUES ('2', '', '', '', '', '', null, null, null, '0', '', '0');
