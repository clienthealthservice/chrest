<?php
class MY_Model extends CI_Model{
    const DB_TABLE_NAME='activity_log';
    const DB_TABLE_PK='pk_act_id';

    public function __construct() {
        parent::__construct();
     
    }
    
    public static function activity_log($what){
        $ci =& get_instance();
        $data['activity'] = $what;
        $ci->db->insert(self::DB_TABLE_NAME,$data);
    }
    
    public static function get_activity_log(){
        $ci =& get_instance();
        $ci->db->limit(10);
        $ci->db->order_by('time','desc');
        $query=$ci->db->get(self::DB_TABLE_NAME);//limit the results to 10
        
        if ($query->num_rows()>0){
            
            return $query->result() ;
            
        }else{
            return FALSE;
        }
    }
}
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

