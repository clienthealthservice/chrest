<?php

class Patient extends CI_Controller{
    public function __construct() {
        parent::__construct();
        $this->load->model('patient_m');
    }



    public function check_pt_number($number){
       $number= html_escape($number);

       if(Patient_m::check_pt_number($number)){
           return 'Patient Number is valid';
       }else{
           return 'Invalid Patient Number';
       }

   }

   public function remove_patient($pt_number){
      $delete= Patient_m::remove_patient($pt_number);
      if($delete){
          return 'User deleted';
      }else{
          return 'Deletion not successful';
      }
   }

}


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
