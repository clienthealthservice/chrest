<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lab_technician extends CI_Controller {

    public function __construct() {
        parent::__construct();
        
        $access_level=  $this->session->userdata('access_level');
        if($access_level!=5){
            redirect('login');
        }
        $this->load->model('patient_m');
        $this->load->model('staff_m');
        $this->load->model('lab_records_m');
        $this->load->model('lab_request_m');
        $this->load->model('doctors_records_m'); 
    }
    
    
    public function index(){
        $this->dashboard();
    }
    
    public function dashboard(){
        
        $data['main_view']='lab_technician/home';
        $data['page_title']='Dashboard';
        
        $this->load->view('includes/backend/template',$data);
    }

    public function create_report($patient_number=NULL){
        $data['patient_number']=$patient_number;
        $data['main_view']='lab_technician/create_report';
        $data['page_title']='Create Record';
        
        $this->load->view('includes/backend/template',$data);
                
    }
    public function patients(){
        //get all active patients
        $patients=Patient_m::get_patients_general();
        
        //set data to be passed to view
        $data['page_title']= 'patients';
        $data['main_view']='lab_technician/patients';
        $data['patients']=$patients;
        
        //load the view
        $this->load->view('includes/backend/template',$data);
        
        
    }
    
    public function save_report($report_number=NULL){
            
        if($report_number){
            $this->update_report($report_number);
        }else{
            $this->add_report();
        }
       
    }
    private function add_report(){
            $data['page_title']='Patients';
             $data['main_view']='lab_technician/patients';
             $data['patient_number']=$this->input->post('patient_num');
        //Validate user inputs
            $this->form_validation->set_rules('patient_num', 'Patient Number', 'required|min_length[2]|max_length[15]');
            $this->form_validation->set_rules('requesting_doctor', 'Requesting Doctor', 'required');
            $this->form_validation->set_rules('report_type', 'Report type', 'required');
            //$this->form_validation->set_rules('recommendation', 'Recommendations', 'required');
            //$this->form_validation->set_rules('result_description', 'Results', 'required');
            

            //Delimiter for output
            $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');

            if ($this->form_validation->run()){

            //get and assign inputs
            $report= new Lab_records_m();
            $report->patient_number=  $this->input->post('patient_num');
            $report->requesting_doctor_id=  $this->input->post('requesting_doctor');
            $report->attending_technician_id=  $this->input->post('attending_technician_id');
            $report->recommendations=  $this->input->post('recommendation');
            $report->report_type=  $this->input->post('report_type');
            $report->result_description=  $this->input->post('result_description');
            
             if($report->save()){
                 
                 //check if the request was an ajax
                 if(!$this->input->is_ajax_request()){
                    $data['page_title']='Patients';
                    $data['main_view']='lab_technician/patients';
                    $data['message']='<div class="alert alert-success"> Alright '.$this->session->userdata('staff_name').'! We are good to go</div>';  
                 }else{
                     echo 'successfull';
                 }
                 
             
            }else{
                //check if the request was an ajax
                 if(!$this->input->is_ajax_request()){
                 $data['page_title']='Create Report';
                 $data['main_view']='lab_technician/create_report';
                 $data['message']='<div class="alert alert-danger"> Oops '.$this->session->userdata('staff_name').'! Operation Failed</div>';
                }else{
                     echo 'Failed';
                 }
            
            }
            }else{//Form Validation Failed
                    if(!$this->input->is_ajax_request()){
                        $data['page_title']='Create Report';
                        $data['main_view']='lab_technician/create_report';
                        $data['patient_number']=$this->input->post('patient_num');
                    }else{
                        echo "Form Validation Failed. Please populate all fields";
                    }
             }
         
            //load the view
             if(!$this->input->is_ajax_request()){
            $this->load->view('includes/backend/template',$data);
             }
    }
    
    private function update_report($report_number){
        //Validate user inputs
            $this->form_validation->set_rules('patient_num', 'Patient Number', 'required|min_length[2]|max_length[15]');
            $this->form_validation->set_rules('requesting_doctor', 'Requesting Doctor', 'required|max_length[3]');
            $this->form_validation->set_rules('recommendation', 'Recommendations', 'required');
            $this->form_validation->set_rules('report_type', 'Report type', 'required');
            $this->form_validation->set_rules('result_description', 'Results', 'required');
            

            //Delimiter for output
            $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');

        if ($this->form_validation->run()){
            
        //get and assign inputs
        $report= new Lab_records_m();
        $report->patient_number=  $this->input->post('patient_num');
        $report->requesting_doctor_id=  $this->input->post('requesting_doctor');
        $report->attending_technician_id=  $this->input->post('attending_technician_id');
        $report->recommendations=  $this->input->post('recommendation');
        $report->report_type=  $this->input->post('report_type');
        $report->result_description=  $this->input->post('result_description');
        
        
         $report->save($report_number);
        }else{
            $data['page_title']='Create Report';
            $data['main_view']='lab_technician/create_report';
            $this->load->view('includes/backend/template',$data);
        }
    }
    
    /**
     * Controller is for viewing all reports that a paient has
     * 
     */
    public function view_reports($patient_number){
        $data['main_view']='lab_technician/view_reports';
        $data['page_title']='Patient`s Lab Reports';
        $reports=Lab_records_m::get_patient_lab_reports($patient_number);
        
        $data['reports']=$reports;

        //load the view
        $this->load->view('includes/backend/template',$data);
        
    }
    
    public function request_completed($request_id){
        Lab_request_m::set_request_status_to_1($request_id);
    }
}