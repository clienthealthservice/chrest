<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account extends CI_Controller {

    public function __construct() {
        
        parent::__construct();
        
        $this->load->model('patient_m');
        $this->load->model('staff_m');
        $this->load->model('account_m');
         $access_level=  $this->session->userdata('access_level');
        if(empty($access_level)){
            redirect('login');
        }
    }
    
    public function index(){
        $this->update_profile();
    }
    
    
    public function update_profile(){
            //set data to be passed to the view
            $data['page_title']='Update Account';
            $data['main_view']='account/update_profile';
            
            //check form submittion
            if ($this->input->post('stf_update')){
                //then validate and go ahead and update the info of the staff
                $this->form_validation->set_rules('stf_f_name', 'Firstname', 'required|min_length[2]|max_length[32]|alpha');
                $this->form_validation->set_rules('stf_l_name', 'Lastname', 'required|min_length[2]|max_length[32]|alpha');
                $this->form_validation->set_rules('stf_gender', 'Gender', 'required|alpha');
                $this->form_validation->set_rules('stf_email', 'Email', 'required|valid_email');
                $this->form_validation->set_rules('stf_mobile', 'Mobile', 'required|numeric|min_length[10]|max_length[15]');
  
                //set the form validation delimeter
                $this->form_validation->set_error_delimiters('<div class="alert alert-danger">','</div>');
                
                //check if validations are met
                if($this->form_validation->run()){//validation success
                    //instantiate a new staff model
                    $staff= new Staff_m;
                   //Assign the form values to the properties
                    $staff->staff_first_name=  $this->input->post('stf_f_name');
                    $staff->staff_last_name= $this->input->post('stf_l_name');
                    $staff->staff_username= $this->input->post('stf_username');
                    $staff->staff_email= $this->input->post('stf_email');
                    $staff->staff_gender= $this->input->post('stf_gender');
                    $staff->staff_phone_number= $this->input->post('stf_mobile');
                    
                    //Upload image
                        $this->upload_image();//upload file to the folder
                        $image_info=  $this->upload->data();//get the uploaded image`s info
                        $staff->staff_image_name = $image_info['raw_name'].$image_info['file_ext'];//name to be saved in db

                        //image thumbnail path... Combination of image constant + raw_name of file+ the file extention...
                        //I wonder how i came by this long method tho
                        $staff->staff_image_thumb_name=$image_info['raw_name'].'_thumb'.$image_info['file_ext'];
                    //update
                    if($staff->update_account_profile($this->session->userdata('staff_id'))){
                    
                        $data['message']='<div class="alert alert-success">Updated successfully</div>';
                    }
                   }else{
                       $data['message']='<div class="alert alert-warning">Oops</div>';
                   }
                       
                }
                //load the view
                $this->load->view('includes/backend/template',$data);
               
            
            
        }
        
     public function change_password(){
            $data['page_title']='Change Password';
            $data['main_view']='account/change_password';
            
            if($this->input->post('stf_change_password')){
                //validate passwords
                $this->form_validation->set_rules('stf_current_password', 'Current PAssword', 'required');
                $this->form_validation->set_rules('stf_new_password', 'Password', 'required|matches[stf_new_passwordconf]');
                $this->form_validation->set_rules('stf_new_passwordconf', 'Password Confirmation', 'required');
                
                $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
                if($this->form_validation->run()){
                    $old_pass=$this->input->post('stf_current_password');
                    $new_pass=$this->input->post('stf_new_password');
                    //check if old password is valid
                   if($this->checkOldPass($old_pass)){//old password is valid
                       //change password
                       if($this->do_change_password($old_pass,$new_pass)){
                           $data['message']='<div class="alert alert-success">Password changed successfully </div>';    
                       }
                   }else{
                       $data['message']='<div class="alert alert-warning">Wrong old password</div>';
                   }
                       
                }
                $this->load->view('includes/backend/template',$data);
               
            }else{
                $this->load->view('includes/backend/template',$data);
            }
        } 
        
        
        
    private static function checkOldPass($old_password)
        {
            $ci =& get_instance();
            $ci->db->where('pk_staff_id', $ci->session->userdata('staff_id'));
            $ci->db->where('staff_password', sha1($old_password));
            $query = $ci->db->get('staff');
            if($query->num_rows() >0)
                return 1;
            else
                return 0;
            
        }
        
    public static function do_change_password($old_password,$new_password){
            $ci =& get_instance();
            $ci->db->where('pk_staff_id', $ci->session->userdata('staff_id'));
            $ci->db->where('staff_password', sha1($old_password));
            
            $data=array('staff_password'=>sha1($new_password));
            $query = $ci->db->update('staff',$data);
            
            if($query){
                MY_Model::activity_log(printf('%s has changed password', $ci->session->userdata('staff_name')));
                return TRUE;
            }else{
                return FALSE;
            }
    }
    
    
    
          /**
     * This function is for image upload in this event controller Class
     */
    private function upload_image(){
            //seeting
		$config['upload_path'] ='assets/uploads/images';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']	= '5120';//5MB
		//$config['max_width']  = '1024';//px
		//$config['max_height']  = '768';//px
                // $config['file_name']='event.jpg';
                $config['encrypt_name']= TRUE;

		$this->upload->initialize($config);
                $field_name = "stf_image";

		if ( ! $this->upload->do_upload($field_name))
		{
			$error = array('error' => $this->upload->display_errors());
			echo var_dump($error);
		}
		else
		{
			$image_info = $this->upload->data();
                        $source_path=$image_info['full_path'];

                        //Go ahead to create thumbnail
                        $config['image_library'] = 'gd2';
                        $config['source_image']	= $source_path;
                        $config['create_thumb'] = TRUE;
                        $config['maintain_ratio'] = TRUE;
                        //$config['width']	= 500;
                        //$config['height']	= 500;

                        $this->load->library('image_lib', $config);
                        $this->image_lib->resize();

		}
	}
}