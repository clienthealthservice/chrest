<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Receptionist extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('patient_m');
        $this->load->model('staff_m');
        $this->load->model('next_of_kin_m');
         $this->load->model('attendance_patients');
        
        //check if access level is 6
        // if it then the controller request is from a receptionist
        // if not then redirect back to login controller
         $access_level=  $this->session->userdata('access_level');
        if($access_level!=6){
            redirect('login');
        }
    }
    
    public function index(){
        Patient_m::get_patients_general();
            $data['page_title']='Receptionist';
            $data['main_view']='receptionist/receptionist';
            
        $this->load->view('/includes/backend/template',$data);
    }
    
    
    public function add_patient(){
        $data['page_title']='New Patient';
        $data['main_view']='receptionist/add_patient_form';
        
         $CI =& get_instance();
         //check if a form has been submitted
         if($CI->input->post('pt_submit')){
         
            //Validate user inputs
                $CI->form_validation->set_rules('pt_f_name', 'Firstname', 'required|min_length[2]|max_length[32]|alpha');
                $CI->form_validation->set_rules('pt_l_name', 'Lastname', 'required|min_length[2]|max_length[32]|alpha');
                $CI->form_validation->set_rules('pt_gender', 'Gender', 'required|alpha');
                $CI->form_validation->set_rules('pt_mobile', 'Mobile', 'required|numeric|min_length[10]|max_length[15]');
                $CI->form_validation->set_rules('pt_address', 'Address', 'required');
                $CI->form_validation->set_rules('pt_date_of_birth', 'Date', 'required');
                $CI->form_validation->set_rules('pt_nationality', 'Nationality', 'required|alpha');
                $CI->form_validation->set_rules('pt_first_attendance', 'First Attendance', 'required');
                
                //next of kin
                $CI->form_validation->set_rules('pt_nok_f_name', 'Next of Kin First Name', 'min_length[2]|max_length[32]required|alpha');
                $CI->form_validation->set_rules('pt_nok_l_name', 'Next of Kin Last Name', 'min_length[2]|max_length[32]required|alpha');
                $CI->form_validation->set_rules('pt_nok_date_of_birth', 'Next of Kin Date of Birth', 'required');
                $CI->form_validation->set_rules('pt_nok_occupation', 'Next of Kin Occupation', 'required|alpha');
                $CI->form_validation->set_rules('pt_nok_address', 'Next of Kin Address', 'required');
                $CI->form_validation->set_rules('pt_nok_residence', 'Next of Kin residence', 'required');
                $CI->form_validation->set_rules('pt_nok_phone', 'Next of Kin Phone', 'required');
                $CI->form_validation->set_rules('pt_nok_nationality', 'Next of Kin Nationlity', 'required|alpha');
                
                
                
                //Delimiter for output
                $CI->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
                
            if ($CI->form_validation->run()){
                    //Form Validation
                // instantiate a new patient
                $patient=new Patient_m;
                //set patient properties values
                $patient->patient_first_name=$CI->input->post('pt_f_name');
                $patient->patient_last_name=$CI->input->post('pt_l_name');
                $patient->patient_middle_name=$CI->input->post('pt_m_name');       
                $patient->patient_gender=$CI->input->post('pt_gender');
                $patient->patient_occupation=$CI->input->post('pt_occupation');
                $patient->patient_address=$CI->input->post('pt_address');
                $patient->patient_mobile=$CI->input->post('pt_mobile');
                $patient->patient_date_of_birth=$CI->input->post('pt_date_of_birth');
                $patient->patient_place_of_birth=$CI->input->post('pt_place_of_birth');
                $patient->patient_place_of_residence=$CI->input->post('pt_place_of_residence');
                $patient->patient_nationality=$CI->input->post('pt_nationality');
                $patient->patient_first_attendance=$CI->input->post('pt_first_attendance');
                
                if($this->input->post("nhis")){
                    $patient->nhis=TRUE;
                    $patient->nhis_number=$this->input->post('nhis_membership_number');
                    $patient->nhis_next_renewal=$this->input->post('nhis_next_renewal');
                    $patient->nhis_expiry_date=$this->input->post('nhis_expiry_date');
                }
                
                //Upload image
                
                $this->upload_image();//upload file to the folder
                $data=  $this->upload->data();//get the uploaded image`s info
                $patient->patient_image_name = $data['raw_name'].$data['file_ext'];//name to be saved in db

                //image thumbnail path... Combination of image constant + raw_name of file+ the file extention...
                //I wonder how i came by this long method tho
                $patient->image_thumb_name=$data['raw_name'].'_thumb'.$data['file_ext'];
                
                if($patient->add_patient()){
                    
                    //insert next of kin
                    $pt_nok = new Next_of_kin_m();
                    $pt_nok->pt_number= $patient->patient_number;
                    $pt_nok->firstname=$CI->input->post('pt_nok_f_name');
                    $pt_nok->lastname=$CI->input->post('pt_nok_l_name');
                    $pt_nok->address=$CI->input->post('pt_nok_address');
                    $pt_nok->name=$CI->input->post('pt_nok_name');
                    $pt_nok->date_of_birth=$CI->input->post('pt_nok_date_of_birth');
                    $pt_nok->nationality=$CI->input->post('pt_nationality');
                    $pt_nok->occupation=$CI->input->post('pt_nok_occupation');
                    $pt_nok->phone=$CI->input->post('pt_nok_phone');
                    $pt_nok->residence=$CI->input->post('pt_nok_residence');
                    
                    $pt_nok->add();
                    
                    $data['message']='<div class="alert alert-success"> Patient added successfully. The patient\'s number is <strong>'.$patient->patient_number.'</strong> </div>';
                    
                    $data['page_title']='New Patient';
                    $data['main_view']='receptionist/receptionist';
                    
                    $CI->load->view('includes/backend/template',$data);

                }else{
                    //redirect($_SERVER["HTTP_REFERER"]);
                    $CI->load->view('includes/backend/template',$data);
                }

         }else{
             //form not valid
             $CI->load->view('includes/backend/template',$data);
             
         }
            
         }else{
             //if no form submitted then load the add_patatient form
             
            $CI->load->view('includes/backend/template',$data);
         
         }
         
    }
    
    public function edit_patient($patient_number){
        if (isset($patient_number)){
            $data['page_title']='Patient Info';
            $data['patient_number']= $patient_number;
            $data['main_view']='receptionist/edit_patient';
        
            $CI =& get_instance();
            if($CI->input->post('pt_submit')){
         
            //Validate user inputs
                $CI->form_validation->set_rules('pt_f_name', 'Firstname', 'required|min_length[2]|max_length[15]');
                $CI->form_validation->set_rules('pt_l_name', 'Lastname', 'required|min_length[2]|max_length[15]');
                $CI->form_validation->set_rules('pt_gender', 'Gender', 'required');
                $CI->form_validation->set_rules('pt_address', 'Address', 'required');
                $CI->form_validation->set_rules('pt_date_of_birth', 'Date', 'required');
                $CI->form_validation->set_rules('pt_nationality', 'Nationality', 'required');
                $CI->form_validation->set_rules('pt_first_attendance', 'Nationality', 'required');
                
                //Delimiter for output
                $CI->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
                
            if ($CI->form_validation->run()){
                    //Form Validation
                // instantiate a new patient
                $patient=new Patient_m;
                //set patient properties values
                $patient->patient_first_name=$CI->input->post('pt_f_name');
                $patient->patient_last_name=$CI->input->post('pt_l_name');
                $patient->patient_middle_name=$CI->input->post('pt_m_name');       
                $patient->patient_gender=$CI->input->post('pt_gender');
                $patient->patient_occupation=$CI->input->post('pt_occupation');
                $patient->patient_address=$CI->input->post('pt_address');
                $patient->patient_mobile=$CI->input->post('pt_mobile');
                $patient->patient_date_of_birth=$CI->input->post('pt_date_of_birth');
                $patient->patient_place_of_birth=$CI->input->post('pt_place_of_birth');
                $patient->patient_place_of_residence=$CI->input->post('pt_place_of_residence');
                $patient->patient_nationality=$CI->input->post('pt_nationality');
                $patient->patient_first_attendance=$CI->input->post('pt_first_attendance');
                if($this->input->post("nhis")){
                    $patient->nhis=TRUE;
                    $patient->nhis_number=$this->input->post('nhis_membership_number');
                    $patient->nhis_next_renewal=$this->input->post('nhis_next_renewal');
                    $patient->nhis_expiry_date=$this->input->post('nhis_expiry_date');
                }

                if($patient->update_patient($patient_number)){
                    $data['message']='<div class="alert alert-success"> Patient info updated successfully. patient\'s number is <strong>'.$patient_number.'</strong> </div>';
                    
                    $CI->load->view('includes/backend/template',$data);

                }else{
                    //redirect($_SERVER["HTTP_REFERER"]);
                    $CI->load->view('includes/backend/template',$data);
                }

         }else{
             $CI->load->view('includes/backend/template',$data);
             
         }
            
         }  else {
             
            $this->load->view('includes/backend/template',$data);
         }
        }
    }
    
    
    /**
     * This function is for image upload in this event controller Class
     */
    private function upload_image()
	{
            //seeting
		$config['upload_path'] ='assets/uploads/images';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']	= '5120';//5MB
		//$config['max_width']  = '1024';//px
		//$config['max_height']  = '768';//px
                // $config['file_name']='event.jpg';
                $config['encrypt_name']= TRUE;

		$this->upload->initialize($config);
                $field_name = "pt_image";

		if ( ! $this->upload->do_upload($field_name))
		{
			$error = array('error' => $this->upload->display_errors());
			echo var_dump($error);
		}
		else
		{
			$data = $this->upload->data();
                        $source_path=$data['full_path'];

                        //Go ahead to create thumbnail
                        $config['image_library'] = 'gd2';
                        $config['source_image']	= $source_path;
                        $config['create_thumb'] = TRUE;
                        $config['maintain_ratio'] = FALSE;
                        $config['width']	= 460;
                        $config['height']	= 320;

                        $this->load->library('image_lib', $config);
                        $this->image_lib->resize();

		}
	}
    
        /**
         * deactivates a specific patient
         * @param type $patient_number
         */
    public function deactivate_pt($patient_number){
        Patient_m::deactivate_pt($patient_number); 
    }
    
    /**
     * This functions send a patient number to the "mark_as_attending" model to be marked present
     * This function should only accept ajax, Preffered
     * Because there is no view set for it
     * @param type $patient_number
     */
    public function attendance($patient_number){
        $mark= attendance_patients::add_attendance($patient_number);
        if($mark){
            echo "Marked";
        }  else {
            echo 'Failed to be mark as present today';
        }
    }
    
  
    
}
    