<?php

class Logout extends CI_Controller{
    public function __construct() {
        parent::__construct();
        }
        
    public function index(){
        $this->do_logout();
        redirect('welcome');
    }
    
    private function do_logout(){
        $this->session->unset_userdata('is_logged_in');
        $this->session->sess_destroy();
    }
}
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

