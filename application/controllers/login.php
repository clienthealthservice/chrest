<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class login extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('Staff_m');

    }

    public function index(){
        $data['page_title']='Login';
        $data['main_view']='login';
        $data['nav_enabled']=FALSE;//will not display top-navigation


        if($this->input->post('staff_login')){
            $username= html_escape($this->input->post('staff_username'));
            $password= html_escape($this->input->post('staff_password'));

            $this->do_login( $username,$password);
        }else{
             $this->load->view('includes/frontend/template',$data);
        }
    }
    private function do_login($username,$password){
        $data['main_view']='login';
        $data['nav_enabled']=FALSE;//will not display top-navigation
        //Set form Validation rules
            $this->form_validation->set_rules('staff_username','Username','required');
            $this->form_validation->set_rules('staff_password','Password','required');

            //set delimiter ie the container in which the form will be displayed;
            // Displaying Errors In Div
            $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');

            if($this->form_validation->run()){
                
                $staff_username= html_escape($username);
                $staff_password= sha1(html_escape($password));

                //Instantiate user Class
                $login_staff = new Staff_m();

                //set properties for verification

                $login_staff->staff_username = $staff_username;
                $login_staff->staff_password = $staff_password;


            if($login_staff->login()){
                //get user session data
                //redirect('/user/account');
                $data['main_view']='dashboard';
                $this->page_redirector($this->session->userdata('access_level'));

            }else{
                echo '<div class="alert alert-danger">Username or password not correct</div>';

                $this->load->view('includes/frontend/template',$data);

            }
            }else{
               $this->load->view('includes/frontend/template',$data);
            }

    }

    /**
     * This redirects users to their appriopraite controller after successfull login
     * @param int $access_level
     * @return string
     */
    private function page_redirector($access_level){
        switch ($access_level){
        case 1:// then is admin
            $controller='admin';
            break;
        case 2:// then is a doctor
            $controller='doctor';
            break;
        case 3://its a pharmacist
            $controller='pharmacist';
            break;
        case 4://its a nurse
            $controller='nurse';
            break;
        case 5://its a Lab tech
            $controller='lab_technician';
            break;
        case 6://its a receptionist
            $controller='receptionist';
            break;

        }

        redirect ($controller);
    }
}
