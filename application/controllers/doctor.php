<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Doctor extends CI_Controller {

    public function __construct() {
        parent::__construct();
        
        $access_level=  $this->session->userdata('access_level');
        if($access_level!=2){
            redirect('login');
        }
        $this->load->model('patient_m');
        $this->load->model('lab_records_m');
        $this->load->model('nurse_records');
        $this->load->model('staff_m');
        $this->load->model('doctors_records_m');   
    }
    
    
    public function index(){
        $this->patients();
    }
    
    public function patients($patient_number=NULL){
        $patient_number=  html_escape(trim($patient_number));
        if (empty($patient_number)){
            
            $data['main_view']= 'doctor/patients';
            $data['page_title']= 'All Patients';
            $this->load->view('includes/backend/template',$data);
        }else{  
            if(Nurse_records::get_patient_records($patient_number)!=FALSE){
            $patients_previous_record= Nurse_records::get_patient_records($patient_number);
            $data['patient_previous_records']= $patients_previous_record;
            }
            $data['main_view']= 'doctor/view_prescription';
            $data['page_title']= 'Prescription';
            $data['pt_number']= $patient_number;
            
            $this->load->view('includes/backend/template',$data);
        }
    
    }
    
    public function records($pt_number){
        
        if(isset($pt_number)){
            
            
            $records=Doctors_records_m::get_patients_records($pt_number);
        
                if ($records){
                    $data['records']=$records;
                }else{
                    $data['records']='No record found';
                }
            $data['main_view']='doctor/pt_records';
            $data['page_title']='Records';
            $data['patient_number']=$pt_number;
            
            //load the view
            $this->load->view('includes/backend/template',$data);
                
        }else{
             die('Please enter a patient number');
        }
    }
    
    /**
     * 
     * @param type $record_number
     */
    public function record($record_number){
        
        if(isset($record_number)){
            $record=Doctors_records_m::get_record($record_number);
        
                if ($record){
                    $data['record']=$record;
                }else{
                    $data['record']='Record not found';
                }
            $data['main_view']='doctor/view_record';
            $data['page_title']='Record';
            
            //load the view
            $this->load->view('includes/backend/template',$data);
                
        }else{
             die('Please enter a patient number');
        }
    }
    
    /**
     * 
     */
    
    public function add_prescription($patient_number){
        
            $data['page_title']='Doctor';
            $data['main_view']='doctor/add_prescription/'.$patient_number;
             
            //Delimiter for output
            $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
        if(!empty($this->input->post('submit_prescriptions'))){
            //VALIDATIONS
            $this->form_validation->set_rules('attending_doctor_id', 'Attending Doctor', 'required');
            $this->form_validation->set_rules('patient_number', 'Patient Number', 'required');
            
            $this->form_validation->set_rules('symptoms', 'Symptoms', 'required');
            $this->form_validation->set_rules('prescription', 'prescription', 'required');
            
            if($this->form_validation->run()){
                    $pres= new Doctors_records_m;
            
                    $pres->fk_attending_doctor=  $this->input->post('attending_doctor_id');
                    $pres->fk_patient_number=$this->input->post('patient_number');
                    $pres->fk_attending_doctor=$this->input->post('attending_doctor_id');
                    $pres->date_of_attendance=$this->input->post('date_of_attendance');
                    $pres->diagnosis=$this->input->post('diagnosis');
                    $pres->symptoms=$this->input->post('symptoms');
                    $pres->precription=$this->input->post('prescription');
                    $pres->status=$this->input->post('status');

                    if($pres->create()){
                        $data['message']='<div class="alert alert-success">Patient prescriptions added successfully</div>';
                        $data['page_title']='Doctor';
                        $data['main_view']='doctor/patients';

                         $data['patients']= $patients=Patient_m::get_patients_general();
                        $this->load->view('includes/backend/template',$data);
                    }else{
                        $data['message']='Operation Failed. Please try Again';
                        $data['page_title']='Doctor';
                        $data['main_view']='doctor/add_prescription/'.$patient_number;
                        $this->load->view('includes/backend/template',$data);
                    }
            }  else {
                $this->load->view('includes/backend/template',$data);
            }
            
                
        }  else {
            
            $data['patient_number']=$patient_number;
            $data['main_view']='doctor/add_prescription_form';
            $data['page_title']='Add Prescription';
            
            $this->load->view('includes/backend/template',$data);
        }
        
    }
    
    public function delete($record_number){
            $record=new Doctors_records_m();

            //delete the record by calling the delete function in the doctors_records_m model
           if( $record->delete($record_number)){
            $data['message']='<div class="alert alert-success"> Record successfully deleted</div>';

            $data['main_view']='doctor/pt_records';
            $data['page_title']='Patients Records';

            $this->load->view('includes/backend/template',$data);
            
            }else{

                echo 'Record not be deleted';
            }
    
    }
    
    public function request_lab(){
            $data['main_view']= 'doctor/patients';
            $data['page_title']= 'All Patients';
            $data['message']= '<div class="alert alert-success"> Lab Request has been sent </div>';
            
        if($this->input->post('submit_lab_request')){
            $patient_number=$this->input->post('rqt_pt_number');
            $patient_name=$this->input->post('rqt_pt_name');
            $request_type=$this->input->post('rqt_lab_test_type');
            $request_staff_id=$this->session->userdata('staff_id');
            
            $data_sql=array(
                'request_type'=>$request_type,
                'requesting_staff_id'=>$request_staff_id,
                'patient_number'=>$patient_number,
                'patient_name'=>$patient_name
            );
            
            $this->db->insert('lab_request',$data_sql);
            
        }
        
        $this->load->view('includes/backend/template',$data);
    }
}