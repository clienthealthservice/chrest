<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pharmacist extends CI_Controller {

    public function __construct() {
        parent::__construct();
        
        $access_level=  $this->session->userdata('access_level');
        if($access_level!=3){
            redirect('login');
        }
        $this->load->model('patient_m');
        $this->load->model('staff_m');
        $this->load->model('doctors_records_m'); 
        $this->load->model('pharmacists_records'); 
    }
    
    
    public function index(){
        $this->patients();
    }
    
    
    private function patients(){
        //get all active patients
        $patients=Patient_m::get_patients_general();
        
        //set data to be passed to view
        $data['page_title']= 'patients';
        $data['main_view']='pharmacist/patients';
        $data['patients']=$patients;
        
        //load the view
        $this->load->view('includes/backend/template',$data);
        
        
    }
    
    public function prescriptions($patient_number,$message=NULL){
        //get the prescriptions of patient
       $issued_prescriptions= Pharmacists_records::get_issued_precriptions($patient_number);
       $pending_prescriptions= Pharmacists_records::get_pending_precriptions($patient_number);
       
       //set data to be passed to template file
        $data['main_view']='pharmacist/prescriptions';
        $data['page_title']='Patient Prescriptions';
        $data['issued_prescriptions']=$issued_prescriptions;
        $data['pending_prescriptions']=$pending_prescriptions;
        $data['message']=$message;
        
        //Now load the view
        $this->load->view('includes/backend/template',$data);
           
        //Is that not easy? lol  :)
        
        
        
      
    }
    
    public function issue(){
        //check if its form submition
        if($this->input->post('submit_issue')){// then save
            $this->issue_prescription_process();
        }else{//give the issue form interface
            $this->patients();
        }
    }
    
    private function issue_prescription_process(){
        $prescription= new Pharmacists_records;
        
        //get and assign inputs
        $prescription->doctors_record_number=  $this->input->post('dct_record_num');
        $prescription->attending_doctor_id=  $this->input->post('attending_doctor_id');
        $prescription->prescription=  $this->input->post('prescription');
        $prescription->description=  $this->input->post('description');
        $prescription->lab_record_number=  $this->input->post('lb_record_num');
        $prescription->attending_pharmacist_id=  $this->session->userdata('staff_id');
        $prescription->patient_number=  $this->input->post('patient_num');
        
        //add to database
        if( $prescription->add_record()){
            $message='<div class="alert alert-success">Issue Record has been stored</div>';
            $pateint_number=$this->input->post('patient_num');
            
            //load the prescriptions of the user again and send success message
            $this->prescriptions($pateint_number,$message);
        }else
        {
            $message= '<div class="alert alert-warning">Ooops! issue not saved</div>';
            $patient_number=$this->input->post('patient_num');
            
            //Go back to the prescriptoin and deliver failure message
            $this->prescriptions($patient_number, $message);
            
            
        }
         
        
        
    }
    private function issue_prescription_form(){
        //Set data to view
        $data['main_view']='pharmacist/prescriptions';
        $data['page_title']='Issue Prescription';
        
        //load the view
        $this->load->view('includes/backend/template',$data);
        
        
    }
    
    public function decline_prescription(){
        
    }
}