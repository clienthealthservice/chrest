<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Nurse extends CI_Controller {
    
    public function __construct() {
        parent::__construct();
        
        $access_level=  $this->session->userdata('access_level');
        if($access_level!=4){
            redirect('login');
        }
        $this->load->model('patient_m');
        $this->load->model('staff_m');
        $this->load->model('nurse_records');
    }
    /**
     * This the default function/page 
     * it calls the patient function
     */
    public function index(){
        
        $this->patients();
    }
    
    
    
    public function patients($patient_number=NULL){
        $patient_number=  html_escape(trim($patient_number));
        
        //check if patient number is not is set
        if (empty($patient_number)){
            //get patients data
            $patients=Patient_m::get_patients_general();
            
            //set data to be passed to views
            $data['patients']=  $patients;
            $data['main_view']= 'nurse/patients';
            $data['page_title']= 'All Patients';
            $this->load->view('includes/backend/template',$data);
        }else{  
            //then patient number is set
            //then get patient's nurse records
            if(Nurse_records::get_patient_records($patient_number)!=FALSE){
            $patients_previous_record= Nurse_records::get_patient_records($patient_number);
            $data['patient_previous_records']= $patients_previous_record;
            }
            $data['main_view']= 'nurse/update_patient_record';
            $data['page_title']= 'Update Patient record';
            $data['pt_number']= $patient_number;
            
            $this->load->view('includes/backend/template',$data);
        }
    
    }
    
    public function save(){
        if(!empty($this->input->post('save_record'))){
        //Set form Validation rules
            $this->form_validation->set_rules('pt_temperature','Temperature','required|numeric|max_length[3]|min_length[1]');
            $this->form_validation->set_rules('pt_weight','Weight','required|numeric|max_length[3]|min_length[1]');
            $this->form_validation->set_rules('pt_blood_pressure','Blood Pressure','required|numeric|max_length[3]|min_length[1]');

            //set delimiter ie the container inwhich the form will be displayed;
            // Displaying Errors In Div
            $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');

            if($this->form_validation->run()){
                $this->do_save();
            }else{
                
                redirect($this->agent->referrer());
            }
        }else{
            redirect($this->agent->referrer());
        }
        
    }
    
    private function do_save(){
        $record = new Nurse_records();
        
        
        $record->weight=  $this->input->post('pt_weight');
        $record->temperature= $this->input->post('pt_temperature');
        $record->blood_pressure= $this->input->post('pt_blood_pressure');
        $record->fk_staff_id=  $this->session->userdata('staff_id');
        $record->fk_patient_number= $this->input->post('pt_number');
        
        if($record->save_record()){
            $data['message']='<div class="alert alert-success" role="alert">Patient record has been added succesfully</div>';
            $this->session->set_flashdata('message',$data['message']);
            redirect('nurse/patients/');
             
        }else{
            $data['message']='<div class="alert alert-danger" role="alert">Actions Failed</div>';
            redirect('/nurse/patients/', $data);
        }
        
    }
}
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

