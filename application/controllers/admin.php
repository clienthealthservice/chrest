<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

    public function __construct() {
        
        parent::__construct();
        //load the models needed
        $this->load->model('patient_m');
        $this->load->model('staff_m');
        $this->load->model('account_m');
        $this->load->model('next_of_kin_m');
        $this->load->model('receptionist_m');
        $this->load->model('attendance_patients');
        
         $access_level=  $this->session->userdata('access_level');
        if($access_level!=1){
            redirect('login');
        }
    }
	public function index()
	{
            $this->dashboard();
	}
        
        
        public function dashboard()
	{
            $data['page_title']='Dashboard';
            $data['main_view']='admin/dashboard';
		$this->load->view('/includes/backend/template',$data);
                
	}
        

      
        
        public function receptionist(){
            $data['page_title']='Receptionist';
            $data['main_view']='admin/receptionists';
            
            $this->load->view('includes/backend/template',$data);
        }
        
        public function nurse(){
            $data['page_title']='Nurse';
            $data['main_view']='admin/nurse';
            
            $this->load->view('includes/backend/template',$data);
        }
        
        public function doctor(){
            $data['page_title']='Doctor';
            $data['main_view']='admin/doctor';
            
            $this->load->view('includes/backend/template',$data);
            
           
        }
        public function lab_technician(){
            $data['page_title']='Lab Technician';
            $data['main_view']='admin/lab_tech';
            
            $this->load->view('includes/backend/template',$data);
            
           
        }
        public function pharmacist(){
            $data['page_title']='Pharmacist';
            $data['main_view']='admin/pharmacist';
            
            $this->load->view('includes/backend/template',$data);
            
           
        }
        
        public function patient($action=NULL,$patient_number=NULL){
            $data['page_title']='Patient';
            $data['main_view']='admin/patient';
            
            //get all patients from the database using a static function from the patient model
            //This will be passed on to the default admin patient page to be listed in the table
            $patients=Patient_m::get_patients_general();
            $data['patients']=  $patients;
            
            
            if ($action=='edit'){
               $data['patient_number']=$patient_number;
               $data['main_view']='admin/edit_patient'; 
               
               
               $CI =& get_instance();
               if($CI->input->post('pt_submit')){
         
                        //Validate user inputs
                            $CI->form_validation->set_rules('pt_f_name', 'Firstname', 'required|min_length[2]|max_length[15]');
                            $CI->form_validation->set_rules('pt_l_name', 'Lastname', 'required|min_length[2]|max_length[15]');
                            $CI->form_validation->set_rules('pt_gender', 'Gender', 'required');
                            $CI->form_validation->set_rules('pt_address', 'Address', 'required');
                            $CI->form_validation->set_rules('pt_date_of_birth', 'Date', 'required');
                            $CI->form_validation->set_rules('pt_nationality', 'Nationality', 'required');
                            $CI->form_validation->set_rules('pt_first_attendance', 'Nationality', 'required');

                            //Delimiter for output
                            $CI->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');

                        if ($CI->form_validation->run()){
                                //Form Validation
                            // instantiate a new patient
                            $patient=new Patient_m;
                            //set patient properties values
                            $patient->patient_first_name=$CI->input->post('pt_f_name');
                            $patient->patient_last_name=$CI->input->post('pt_l_name');
                            $patient->patient_middle_name=$CI->input->post('pt_m_name');       
                            $patient->patient_gender=$CI->input->post('pt_gender');
                            $patient->patient_occupation=$CI->input->post('pt_occupation');
                            $patient->patient_address=$CI->input->post('pt_address');
                            $patient->patient_mobile=$CI->input->post('pt_mobile');
                            $patient->patient_date_of_birth=$CI->input->post('pt_date_of_birth');
                            $patient->patient_place_of_birth=$CI->input->post('pt_place_of_birth');
                            $patient->patient_place_of_residence=$CI->input->post('pt_place_of_residence');
                            $patient->patient_nationality=$CI->input->post('pt_nationality');
                            $patient->patient_first_attendance=$CI->input->post('pt_first_attendance');

                            if($patient->add_patient($patient_number)){
                                $data['message']='<div class="alert alert-success"> Patient info updated successfully. patient\'s number is <strong>'.$patient_number.'</strong> </div>';

                                $CI->load->view('includes/backend/template',$data);

                            }else{
                                //redirect($_SERVER["HTTP_REFERER"]);
                                $CI->load->view('includes/backend/template',$data);
                            }

                     }else{
                         $CI->load->view('includes/backend/template',$data);

                     }

                     } else{
                         $this->load->view('includes/backend/template',$data);
                     }
                
            }else if($action=='delete'){
                $this->db->delete('patient_info',array('pt_number'=>$patient_number));
                $data['message']='<div class="alert alert-success"> Patient deletion success ';
                $this->load->view('includes/backend/template',$data);
                
            }
            else if($action=='add'){
                $data['main_view']='admin/add_patient';
                
                        $CI =& get_instance();
                 //check if a form has been submitted
                 if($CI->input->post('pt_submit')){

                    //Validate user inputs
                        $CI->form_validation->set_rules('pt_f_name', 'Firstname', 'required|min_length[2]|max_length[15]');
                        $CI->form_validation->set_rules('pt_l_name', 'Lastname', 'required|min_length[2]|max_length[15]');
                        $CI->form_validation->set_rules('pt_gender', 'Gender', 'required');
                        $CI->form_validation->set_rules('pt_address', 'Address', 'required');
                        $CI->form_validation->set_rules('pt_date_of_birth', 'Date', 'required');
                        $CI->form_validation->set_rules('pt_nationality', 'Nationality', 'required');
                        $CI->form_validation->set_rules('pt_first_attendance', 'Nationality', 'required');

                        //Delimiter for output
                        $CI->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');

                    if ($CI->form_validation->run()){
                            //Form Validation
                        // instantiate a new patient
                        $patient=new Patient_m;
                        //set patient properties values
                        $patient->patient_first_name=$CI->input->post('pt_f_name');
                        $patient->patient_last_name=$CI->input->post('pt_l_name');
                        $patient->patient_middle_name=$CI->input->post('pt_m_name');       
                        $patient->patient_gender=$CI->input->post('pt_gender');
                        $patient->patient_occupation=$CI->input->post('pt_occupation');
                        $patient->patient_address=$CI->input->post('pt_address');
                        $patient->patient_mobile=$CI->input->post('pt_mobile');
                        $patient->patient_date_of_birth=$CI->input->post('pt_date_of_birth');
                        $patient->patient_place_of_birth=$CI->input->post('pt_place_of_birth');
                        $patient->patient_place_of_residence=$CI->input->post('pt_place_of_residence');
                        $patient->patient_nationality=$CI->input->post('pt_nationality');
                        $patient->patient_first_attendance=$CI->input->post('pt_first_attendance');

                        if($patient->add_patient()){

                            //insert next of kin
                            $pt_nok = new Next_of_kin_m();
                            $pt_nok->pt_number= $patient->patient_number;
                            $pt_nok->address=$CI->input->post('pt_nok_address');
                            $pt_nok->name=$CI->input->post('pt_nok_name');
                            $pt_nok->date_of_birth=$CI->input->post('pt_nok_date_of_birth');
                            $pt_nok->nationality=$CI->input->post('pt_nationality');
                            $pt_nok->occupation=$CI->input->post('pt_nok_occupation');
                            $pt_nok->phone=$CI->input->post('pt_nok_phone');
                            $pt_nok->residence=$CI->input->post('pt_nok_residence');

                            $pt_nok->add();

                            $data['info']='<div class="alert alert-success"> Patient added successfully. The patient\'s number is <strong>'.$patient->patient_number.'</strong> </div>';
                            $data['main_view']='alerts';
                            $data['page_title']='New Patient';
                            $CI->load->view('includes/backend/template',$data);

                        }else{
                            //redirect($_SERVER["HTTP_REFERER"]);
                            $CI->load->view('includes/backend/template',$data);
                        }

                 }else{
                     $CI->load->view('includes/backend/template',$data);

                 }

                 }else{
                    $data['info']='<div class="alert alert-info">No form submited</div>';

                    $CI->load->view('includes/backend/template',$data);

                 }
            }
            else{
                
            $this->load->view('includes/backend/template',$data);
            
            }
            function add(){
                $patient= new Patient_m();
                
                $patient->generate_patient_number;
                $patient->pt_f_name= $this->input->post('pt_f_name') ;
                echo 'hii';
           }
        }
        
        
        
        /**
         * This function only deactivates the stafe in the database
         * @param type $id
         * @return boolean
         */
        
        public function staff_deactivate($id){
            $query=$this->db->where(array('pk_staff_id'=>intval($id)));
                $this->db->update('staff',array('active'=>0));
            if($query){
                MY_Model::activity_log('Staff deactivated');
               
                redirect($_SERVER["HTTP_REFERER"]);
            }else{
                return FALSE;
            }
        }
        
        public function staff_edit($id){
            $this->db->where('pk_staff_id',  intval($id));
            $query=$this->db->get('staff');
            
            if($query->num_rows()==1){
                $data['fields']= $query->result();
                $data['main_view']='edit_staff';
            }else{
                die('Staff id unkown');
            }
        }
        
        
        
        public function staff($staff_id=NULL){
            if(!empty($staff_id)){
                
                $this->staff_update($staff_id);
            }
            else{
                $this->staff_add();
            }
            
        }


        private function staff_update($staff_id){
            //get the staff info
            $data['staff_info']= Staff_m::get_a_staff_info($staff_id);
            $data['main_view']="admin/edit_staff";
            $data['page_title']="Edit Staff Info";
            //check if a form is submitted
            if ($this->input->post('stf_submit')){
                //then validate and go ahead and update the info of the staff
                $this->form_validation->set_rules('stf_f_name', 'Firstname', 'required|min_length[2]|max_length[32]|alpha');
                $this->form_validation->set_rules('stf_l_name', 'Lastname', 'required|min_length[2]|max_length[32]|alpha');
                $this->form_validation->set_rules('stf_gender', 'Gender', 'required|alpha');
                $this->form_validation->set_rules('stf_email', 'Email', 'required|valid_email');
                $this->form_validation->set_rules('stf_mobile', 'Mobile', 'required|numeric|min_length[10]|max_length[15]');
                $this->form_validation->set_rules('stf_role', 'Role', 'required|numeric');
                
                //set the form validation delimeter
                $this->form_validation->set_error_delimiters('<div class="alert alert-danger">','</div>');
                if($this->form_validation->run()){
                        
                    //instantiate a the staff_m class
                    $staff= new Staff_m();
                    
                    //Assign the form values to the properties
                    $staff->staff_first_name=  $this->input->post('stf_f_name');
                    $staff->staff_last_name= $this->input->post('stf_l_name');
                    $staff->staff_username= $this->input->post('stf_username');
                    $staff->staff_email= $this->input->post('stf_email');
                    $staff->staff_gender= $this->input->post('stf_gender');
                    $staff->staff_password= $this->input->post('stf_password');
                    $staff->staff_phone_number= $this->input->post('stf_mobile');
                    $staff->access_level= $this->input->post('stf_role');
                    
                    if($staff->update($staff_id)){
                        $data['message']="<div class='alert alert-success'> Update Successful!</div>";
                        
                        $this->load->view('includes/backend/template',$data);
                    }else{
                        $data['message']="<div class='alert alert-danger'> Update not Successful!</div>";
                        
                        $this->load->view('includes/backend/template',$data);
                    }
                }else{
                    // form validation failed
                    $data['message']="<div class='alert alert-danger'> Update not Successful!</div>";
                        
                    $this->load->view('includes/backend/template',$data);
                    
                }
                
            }else{
                //else view staff forms with details;
                $this->load->view('includes/backend/template',$data);
                
            }
        }
        
        /**
         * This function adds a new staff
         * 
         */
        private function staff_add(){
                //check if there is a form submission
            if(!empty($this->input->post('stf_submit'))){
                //then do validation     
                $this->form_validation->set_rules('stf_f_name', 'Firstname', 'required|min_length[2]|max_length[32]|alpha');
                $this->form_validation->set_rules('stf_l_name', 'Lastname', 'required|min_length[2]|max_length[32]|alpha');
                $this->form_validation->set_rules('stf_gender', 'Gender', 'required|alpha');
                $this->form_validation->set_rules('stf_email', 'Email', 'required|valid_email');
                $this->form_validation->set_rules('stf_mobile', 'Mobile', 'required|numeric|min_length[10]|max_length[15]');
                $this->form_validation->set_rules('stf_role', 'Role', 'required|numeric');
                
                //set the form validation delimeter
                $this->form_validation->set_error_delimiters('<div class="alert alert-danger">','</div>');
                
                //Check if form validation was successful
                if($this->form_validation->run()){
                    //instantiate a the staff_m class
                    $staff= new Staff_m();
                    
                    //Assign the form values to the properties
                    $staff->staff_first_name=  $this->input->post('stf_f_name');
                    $staff->staff_last_name= $this->input->post('stf_l_name');
                    $staff->staff_username= $this->input->post('stf_username');
                    $staff->access_level = $this->input->post('stf_role');
                    $staff->staff_email= $this->input->post('stf_email');
                    $staff->staff_gender= $this->input->post('stf_gender');
                    $staff->staff_password= $this->input->post('stf_password');
                    
                    $staff->staff_phone_number= $this->input->post('stf_mobile');
                    
                    //Upload image
                        
                        $this->upload_image();//upload file to the folder
                        $data=  $this->upload->data();//get the uploaded image`s info
                        $staff->staff_image_name = $data['raw_name'].$data['file_ext'];//name to be saved in db

                        //image thumbnail path... Combination of image constant + raw_name of file+ the file extention...
                        //I wonder how i came by this long method tho
                        $staff->staff_image_thumb_name=$data['raw_name'].'_thumb'.$data['file_ext'];
                    
                    //now add staff the staff
                       //But we have to check if the operation is successfull
                    if($staff->add_staff()){
                        //staff added so prepare success message and sent it over to the view
                        $data['message']='<div class="alert alert-success"> Staff successfully added</div>';
                        $data['main_view']='admin/add_staff';
                        $data['page_title']='Add Staff';
         
                        $this->load->view('includes/backend/template',$data);
                    }  else {
                        $this->session->set_flashdata('message', '<div class="alert alert-danger">Operation Failed! Please try agaian</div>');
                        redirect(current_url());
                    }
                        
                   
                }else{
                    $data['main_view']='admin/add_staff';
                    $data['page_title']='Add staff';
                    $this->load->view('includes/backend/template',$data);
                }
            }
            else{
                // if there is no form submition then view the add staff form
                $data['page_title']= 'Add Staff';
                $data['main_view']='admin/add_staff';
                $this->load->view('includes/backend/template',$data);
            }
            
            
            
        }




        /**
         * 
         * @param int $access_level
         */
        public static function count_staff($access_level){
            $ci=& get_instance();
            $ci->db->where('access_level', intval($access_level));
            $ci->db->from('staff');
            echo $ci->db->count_all_results();
        }
        
         /**
         * 
         * @param int $access_level
         */
        public static function count_inactive_staff(){
            $ci=& get_instance();
            $ci->db->where('active',0);
            $ci->db->from('staff');
            echo $ci->db->count_all_results();
        }
        
          /**
     * This function is for image upload in this event controller Class
     */
    private function upload_image()
	{
            //seeting
		$config['upload_path'] ='assets/uploads/images';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']	= '5120';//5MB
		//$config['max_width']  = '1024';//px
		//$config['max_height']  = '768';//px
                // $config['file_name']='event.jpg';
                $config['encrypt_name']= TRUE;

		$this->upload->initialize($config);
                $field_name = "stf_image";

		if ( ! $this->upload->do_upload($field_name))
		{
			$error = array('error' => $this->upload->display_errors());
			echo var_dump($error);
		}
		else
		{
			$data = $this->upload->data();
                        $source_path=$data['full_path'];

                        //Go ahead to create thumbnail
                        $config['image_library'] = 'gd2';
                        $config['source_image']	= $source_path;
                        $config['create_thumb'] = TRUE;
                        $config['maintain_ratio'] = FALSE;
                        $config['width']	= 460;
                        $config['height']	= 320;

                        $this->load->library('image_lib', $config);
                        $this->image_lib->resize();

		}
	}
        
        
    /**
     * This function take the list of all inactive staffs and passes it to the view
     * 
     */
    public function staff_inactive(){
        $inactive_staffs=Staff_m::get_inactive_staffs();
        $data['inactive_staff']=$inactive_staffs;
        $data['main_view']='admin/inactive_staffs';
        $data['page_title']='Inactive Staffs';
        
        $this->load->view('includes/backend/template',$data);
    }
    
    /**
     * Thiis function reactivateds stafs into the system
     */
    
    public function reactivate_staff($staff_id){
        $id= intval($staff_id)  ;
        if (Staff_m::reactivate_staff($id)){
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }
    
    public function get_attendance(){
        $date=$this->input->post('datex');
        $attendees =  attendance_patients::get_attendance($date);
       
        
        if( is_array($attendees)){
          $results='  <table class="table no-margin">';
          $results.='      <thead>';
          $results.='          <tr>';
          $results.='              <th>Patient Number</th>';
          $results.='              <th>Name</th>';
          $results.='              <th>Gender</th>';
          $results.='              <th>Date</th>';
          $results.='          </tr>';
          $results.='      </thead>';
          $results.='      <tbody>';

                foreach($attendees as $patient){
                    
                  $results.='  <tr id="tr_'. $patient->fk_pt_number.'">';
                  $results.='      <td>'.$patient->fk_pt_number.'</td>';
                  $results.='      <td>'.$patient->pt_first_name.' '.$patient->pt_last_name.' </td>';
                  
                  $results.='      <td> <span class="label label-success">'. $patient->pt_gender.'</span> </td>';
                   $results.='      <td>'. $patient->date_of_attendance.' </td>';
                 
                  $results.='  </tr>';
                }
              $results.='  </tbody';
          $results.='  </table>';
           echo $results;
           
        }else{
            echo "No match found for selected date";
        }
    
                  
        }
}