
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Staff extends CI_Controller {
    
    
    public function doctor(){
        $data=array(
            'main_view'=>'doctor',
            'page_title'=>'Staff-Doctor'
        );
        $this->load->view('includes/backend/template',$data);
    }
    
    public function pharmacist(){
        $data=array(
            'main_view'=>'pharmacist',
            'page_title'=>'Staff-Pharmacist'
        );
        $this->load->view('includes/backend/template',$data);
    }
    public function nurse(){
        $data=array(
            'main_view'=>'nurse',
            'page_title'=>'Staff-Nurse'
        );
        $this->load->view('includes/backend/template',$data);
    }
    public function lab_tecnician(){
        $data=array(
            'main_view'=>'lab_technician',
            'page_title'=>'Staff-Lab Technician'
        );
        $this->load->view('includes/backend/template',$data);
    }
    
}


