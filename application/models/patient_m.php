<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Patient_m extends MY_Model{
    const DB_TABLE_PK='pk_pt_id';
    const DB_TABLE_NAME='patient_info';


    public static $is_valid_pt_number;

    /**
     *Patient Properties
     * @var type
     */
    public $patient_number;
    public $patient_first_name;
    public $patient_last_name;
    public $patient_middle_name;
    public $patient_date_of_birth;
    public $patient_gender;
    public $patient_occupation;
    
    
    public $nhis;
    public $nhis_number;
    public $nhis_next_renewal;
    public $nhis_expiry_date;
   
    public $patient_address;
    public $patient_place_of_birth;
    public $patient_place_of_residence;
    public $patient_mobile;
    public $patient_nationality;
    public $patient_first_attendance;
    public $patient_image_name;
    public $patient_image_thumb_name;
    
    public function add_patient(){
        $this->generate_patient_number();
        $data=array(
            'pt_number'=> $this->patient_number,
            'pt_first_name'=>$this->patient_first_name,
            'pt_last_name'=>$this->patient_last_name,
            'pt_middle_name'=>  $this->patient_middle_name,
            'pt_gender'=>  $this->patient_gender,
            'pt_occupation'=>$this->patient_occupation,
            'pt_address'=>  $this->patient_address,
            'pt_date_of_birth'=>$this->patient_date_of_birth,
            'pt_place_of_birth'=>$this->patient_place_of_birth,
            'pt_place_of_residence'=>$this->patient_place_of_residence,
            'pt_nationality'=>$this->patient_nationality,
            'pt_mobile'=>$this->patient_mobile,
            'pt_image_name'=>$this->patient_image_name,
            'pt_image_thumb_name'=>$this->patient_image_thumb_name,
            'pt_first_attendance'=>$this->patient_first_attendance,
        );
        
        //Check if NHIS is set
        if($this->nhis == TRUE){
                $data_nhis=array(
                    'fk_pt_number'=>  $this->patient_number,
                    'membership_number'=>  $this->nhis_number,
                    'next_renewal_date'=> $this->nhis_next_renewal,
                    'expiry_date'=>  $this->nhis_expiry_date
                );
        
        }
        
        //insert into database
        $query=$this->db->insert($this::DB_TABLE_NAME,$data);//patients_table
                //nhis
            if($this->nhis==TRUE){
               $this->db->insert('nhis',$data_nhis);
            }
        
        if(!$query){
            return FALSE;
        }else{
            $this->activity_log('New patient added');
            return TRUE;
        }
    }
    
    public function update_patient($patient_number){
        $data=array(
            'pt_first_name'=>$this->patient_first_name,
            'pt_last_name'=>$this->patient_last_name,
            'pt_middle_name'=>  $this->patient_middle_name,
            'pt_gender'=>  $this->patient_gender,
            'pt_occupation'=>$this->patient_occupation,
            'pt_address'=>  $this->patient_address,
            'pt_date_of_birth'=>$this->patient_date_of_birth,
            'pt_place_of_birth'=>$this->patient_place_of_birth,
            'pt_place_of_residence'=>$this->patient_place_of_residence,
            'pt_nationality'=>$this->patient_nationality,
            'pt_mobile'=>$this->patient_mobile,
            'pt_first_attendance'=>$this->patient_first_attendance,

        );
        //Check if NHIS is set
        if($this->nhis==TRUE){
                $data_nhis=array(
                    'membership_number'=>  $this->nhis_number,
                    'next_renewal_date'=> $this->nhis_next_renewal,
                    'expiry_date'=>  $this->nhis_expiry_date
                );
        
        }
        //insert into database
        
        $this->db->where('pt_number', $patient_number);
        $query=$this->db->update($this::DB_TABLE_NAME,$data);
        
        
        if($this->nhis==TRUE){
            $this->db->where('fk_pt_number', $patient_number);
            $this->db->update('nhis',$data_nhis);
        }
        
        //Updated?
        if(!$query){//no
            return FALSE;
        }else{//updated
            $this->activity_log('New patient info updated');
            return TRUE;
        }
    }

   private function generate_patient_number(){
       $pt_num=substr($this->patient_first_name,0,1).substr($this->patient_last_name,0,1).random_string('numeric', 7);
       $this->patient_number=$pt_num;
   }
   
   
   

   /**
    * Gets all the rows or columns of the patient_info table
    * this result will be displayed in the view in tables
    * @param string $where
    * @param string int $equal
    * @return string
    */
   public static function get_patients($where=null,$equal=null){
       $ci =& get_instance();
       if(!empty($where)&& !empty($equal)){
           $data=array($where=>$equal);
           $ci->db->select('*');   
            $ci->db->from('patient_info d ');
            $ci->db->join('nhis n', 'n.fk_pt_number= d.pt_number', 'left');
            $ci->db->where('is_active',1);
            $ci->db->where($where,$equal);
            $query=$ci->db->get();
       }else{ 
            $query=$ci->db->get('patient_info');
       }

       if($query->num_rows()>0){
          $results='  <table id="chres_table" class="table table-stripped">';
          $results.='      <thead>';
          $results.='          <tr>';
          $results.='              <th>Name</th>';
          $results.='              <th>NHIS Status</th>';
          $results.='              <th>Patient Number</th>';
          $results.='              <th> Address </th>';
          $results.='              <th> Gender </th>';
          $results.='              <th>Action</th>';
          $results.='          </tr>';
          $results.='      </thead>';
          $results.='      <tbody>';

                foreach($query->result() as $patient){
                    
                  $results.='  <tr id="tr_'. $patient->pt_number.'">';
                  $results.='      <td>'.$patient->pt_first_name.' '.$patient->pt_last_name.' </td>';
                  
                  if(!empty($patient->next_renewal_date)&&$patient->next_renewal_date >date("Y-m-d")){
                      $results.='      <td> <span class="label label-pill label-success">Active </span>';
                  }
                  if(!empty($patient->next_renewal_date)&& $patient->next_renewal_date <= date("Y-m-d")){
                      $results.='      <td> <span class="label label-pill label-danger">Expired </span> </td>';
                  };
                  if(empty($patient->next_renewal_date)){
                      $results.='      <td><span class="label label-pill label-warning"> Not set </span></td>';
                  };
                  
                  $results.='      <td>'. $patient->pt_number.' </td>';
                  $results.='      <td>'. $patient->pt_address.' </td>';
                  $results.='      <td>'. $patient->pt_gender.' </td>';
                  $results.="      <td><div class='btn-group btn-group-sm' role='group' ><button onclick='remove_patient(`".$patient->pt_number."`)' class='btn btn-danger'>Remove</button><a href='".site_url($ci->router->fetch_class()."/edit_patient/".$patient->pt_number)."' class='btn btn-info'>Edit</a></div></td>";
                  $results.='  </tr>';
                }

              $results.='  </tbody';
          $results.='  </table>';
           return $results;

        }else{
            echo "No match found";
        }
   }
   
   
   /**
    * Gets all the rows or columns of the patient_info table
    * @param strinf $where
    * @param strin/int $equal
    * @return string
    */
  
   public static function get_patients_general($where=null,$equal=null){
       $ci =& get_instance();
       if(!empty($where)&& !empty($equal)){
           
           $ci->db->select('*');   
            $ci->db->from('patient_info p ');
            $ci->db->join('nhis n', 'n.fk_pt_number= p.pt_number', 'left');
            $ci->db->where($where,$equal);
            $query=$ci->db->get();
           
       }else{
          
           $ci->db->select('*');   
            $ci->db->from('patient_info p ');
            $ci->db->join('nhis n', 'n.fk_pt_number= p.pt_number', 'left');
            $query=$ci->db->get();

       }

       if($query->num_rows()>0){
           return $query->result();

        }else{
            echo "No match found";
        }
   }
   
   /**
    * This function returns data of a particular patient using the patient number
    * Eg. If you need a patient firstname just enter the column name and the patients nmber
    * @param type $what
    * @param type $patient_number
    * @return string
    */
    public static function get_patient($what,$patient_number){
       $ci =& get_instance();
       if(!empty($what)&& !empty($patient_number)){
           $data=array('pt_number'=>$patient_number);
           $query=$ci->db->get_where(self::DB_TABLE_NAME,$data);
       }else{
            return "No parameter";
       }

       if($query->num_rows()>0){
            foreach($query->result() as $patient){
              $result=$patient->$what;
            }

           return $result;

        }else{
            
            return "No match found";
        }
   }
   
  
   
   /**
    * count all patients
    */
   public static function count_patients(){
        $ci =& get_instance();
        $query=$ci->db->get(self::DB_TABLE_NAME);

        return $query->num_rows();
    }



   /**
    * Get from patient
    * @param type $patient_num
    * @param type $what
    */
   public static function get($patient_number,$col){
       $ci =& get_instance();

       $data['pt_number']=  html_escape($patient_number);
       $col=  html_escape($col);
       $ci->db->order_by('pk_pt_id','asc');

       $query=$ci->db->get_where(self::DB_TABLE_NAME,$data);

       if($query->num_rows()>0){
           foreach ($query->result_array() as $row){
            return $row[$col];
           }
       }  else {
           return FALSE;
       }

   }
   public static function deactivate_pt($patient_number){
        $ci =& get_instance();
        $ci->db->where('pt_number', $patient_number);
        
        $ci->db->update('patient_info',array('is_active'=>0));
        
    }
   
  
    /**
     * Removes Patient from the database
     * @param type $patient_number
     * @return boolean
     */
   public static function remove_patient($patient_number){
        $ci =& get_instance();
        $data['pt_number']=$patient_number;
        if($ci->db->delete(self::DB_TABLE_NAME,$data)){
            self::activity_log("Patient has been removed");
            return TRUE;
        }else{
            return FALSE;
        }

   }


   public static function check_pt_number($pt_number){
       $ci =& get_instance();

       $ci->db->where('pt_number',$pt_number);
       $query=$ci->db->get(self::DB_TABLE_NAME);

       if($query->num_rows()>0){
           return TRUE;
       }else{
           return FALSE;
       }
   }


   public static function get_image_url($pt_num){
       $ci =& get_instance();
       
       $ci->db->where(array('pt_number'=>$pt_num));
       $ci->db->select('pt_image_name');
       $query=$ci->db->get('patient_info');
       
        $row= $query->row();
       return $row->pt_image_name;
   }
   
   
   /**
    * This function adds a patient in to the attendance table  as present today
    * @param int $member_id
    * @return boolean
    */
   public static function mark_as_attending($patient_number){
       $ci =& get_instance();
       $data=array(
           'fk_pt_number'=> $patient_number
       );
       
       $query=$ci->db->insert('attendance_patients',$data);
       if($query){
           $this->activity_log('Patient added to attendance');
           return TRUE;  
       }  else {
          return FALSE; 
       }
   }
   
  
}
