<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Receptionist_m extends MY_Model{
    
    
    const DB_TABLE_PK='pk_pt_id';
    const DB_TABLE_NAME='receptionist';

    /**
     *
     * @var string 
     */
    public $name;
    
    /**
     *
     * @var string
     */
    public $email;
    /**
     *
     * @var string
     */
    
    public $address;
    
    /**
     *
     * @var int
     */
    public $phone;
    
    /**
     *
     * @var string
     */
    public $password;
    
    /**
     *
     * @var int(0/1) 
     */
    public $active;
    
    
    
    
    
    //-----------------Getter----------------------//
    
    
    /**
     * This function gets all the activated receptionists
     * Outputs are rendered in tables
     */
    public static function get_receptionists(){
        $CI= & get_instance();
        $data['active']=1;
        $CI->db->where($data);
        $query=$CI->db->get(self::DB_TABLE_NAME);
        
        if($query->num_rows()>0){
          $results='  <table class="table table-bordered">';
          $results.='      <thead>';
          $results.='          <tr>';
          $results.='              <th>Name</th>';
          $results.='              <th>Address</th>';
          $results.='              <th>Phone</th>';
          $results.='              <th>Action</th>';
          $results.='          </tr>';
          $results.='      </thead>';
          $results.='      <tbody>';

        foreach ($query->result() as $receptionist) {
          
            $results.='  <tr id="tr_'. $receptionist->pk_id.'">';
            $results.='      <td>'.$receptionist->name.'</td>';
            $results.='      <td>'. $receptionist->address.' </td>';
            $results.='      <td>'. $receptionist->phone.' </td>';
            $results.="      <td><div class='btn-group btn-group-sm' role='group' ><button onclick='remove_patient(`".$receptionist->pk_id."`)' class='btn btn-danger'>Remove</button><a href='".site_url($CI->router->fetch_class()."/edit_receptionist/".$receptionist->pk_id)."' class='btn btn-info'>Edit</a></div></td>";
            $results.='  </tr>';
         
        }
        $results.='  </tbody';
          $results.='  </table>';
           return $results;
            
        }else{
            echo "No match found";
        }
        
    } 
    
}