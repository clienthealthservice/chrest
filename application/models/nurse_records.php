<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Nurse_records extends MY_Model{
    const DB_TABLE_PK='pk_id';
    const DB_TABLE_NAME='nurses_records';


    public $weight;
    public $temperature;
    public $blood_pressure;
    
    public $fk_patient_number;
    public $fk_staff_id;
    
    /**
     * Add a nurse record of a patient into the nurse record table
     * @return boolean
     */
    public function save_record(){
        $data =array(
            'fk_staff_id'=>  $this->fk_staff_id,
            'fk_patient_number'=> $this->fk_patient_number,
            'temperature'=> $this->temperature,
            'weight'=>  $this->weight,
            'blood_pressure'=>  $this->blood_pressure
        );
        
        //insert into database
        $query=$this->db->insert($this::DB_TABLE_NAME,$data);
        if($query){
            return TRUE;
        }else
        {
            return FALSE;
        }
    } 
    
    public function delete_record($record_id){
        $ci =& get_instance();
        
        $this->db->where('pk_id','$record_id');
        $query=$this->db->delete($this::DB_TABLE_NAME);
        if($query){
            $row= $query->row();
            MY_Model::activity_log("Patient  id ".$row->fk_pt_number." nurse record deleted");
            return TRUE;
        }else{
            return FALSE;
        }
    }
    
    
    public static function get_patient_records($pt_number){
        $ci =& get_instance();
        $ci->db->where('fk_patient_number',$pt_number);
        $ci->db->order_by('pk_id', 'DESC');
        $ci->db->limit(0,9);
       $query= $ci->db->get(self::DB_TABLE_NAME);
       
       if($query->num_rows()>0){
           $patient_records= $query->result();
           return $patient_records;
       }else{
           return FALSE;
     
       }
    }
    
    /**
     * This function retrieves the last patient entry
     * @param type $pt_number
     * @return boolean
     */
    public static function get_patient_last_entry($pt_number){
        $ci =& get_instance();
        $ci->db->where('fk_patient_number',$pt_number);
        $ci->db->order_by('date_created', 'DESC');
        $ci->db->limit(1);
       $query= $ci->db->get(self::DB_TABLE_NAME);
       
       if($query->num_rows()>0){
           $patient_records= $query->result();
           return $patient_records;
       }else{
           return FALSE;
     
       }
    }
}
