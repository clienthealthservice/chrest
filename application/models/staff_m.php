<?php
class Staff_m extends MY_Model{
    const DB_TABLE_PK='pk_staff_id';
    const DB_TABLE_NAME='staff';
    

    public $staff_first_name;
    public $staff_last_name;
    public $staff_username;
    public $staff_password;
    public $staff_email;
    public $staff_gender;
    public $staff_image_name;
    public $staff_image_thumb_name;
    public $staff_phone_number;
    public $access_level;
    private $staff_role;
   
    
    /**
     * Adds a new staff info to the database
     * @return boolean
     */
    public function add_staff(){
        switch ($this->access_level){
        case 1:
            $this->staff_role='admin';
            break;
        case 2:
            $this->staff_role='doctor';
            break;
        case 3:
            $this->staff_role='pharmacist';
            break;
        case 4:
            $this->staff_role='nurse';
            break;
        case 5:
            $this->staff_role='lab technician';
            break;
        case 6:
            $this->staff_role='receptionist';
            break;
        default :
            $this->staf_role='null';
            
        }
        
        $data=array(
            //staff number is randomly generated numbers plus first letters of each name
            'staff_first_name'=>  $this->staff_first_name,
            'staff_last_name'=>  $this->staff_last_name,
            'staff_username'=>  $this->staff_username,
            'staff_password'=>  sha1($this->staff_password),
            'staff_email'=>  $this->staff_email,
            'staff_gender'=>  $this->staff_gender,
            'staff_phone_number'=>  $this->staff_phone_number,  
            'access_level'=>  $this->access_level,
            'staff_role'=>  $this->staff_role,
            'staff_username'=>  $this->staff_username,
            'staff_image_name'=>  $this->staff_image_name,
            'staff_image_thumb_name'=>  $this->staff_image_thumb_name
            
        );
        if($this->db->insert($this::DB_TABLE_NAME,$data)){
            
            $this::activity_log('A new '.$this->staff_role.'Staff has been added');
            return TRUE;
        }else{
            return FALSE;
        }
    }
    
    /**
     * This function is used to update staff details on the db
     * this is only accessible by the admin
     * @param int $staff_id
     * @return boolean
     */
    public function update($staff_id){
        switch ($this->access_level){
        case 1:
            $this->staff_role='admin';
            break;
        case 2:
            $this->staff_role='doctor';
            break;
        case 3:
            $this->staff_role='pharmacist';
            break;
        case 4:
            $this->staff_role='nurse';
            break;
        case 5:
            $this->staff_role='lab technician';
            break;
        case 6:
            $this->staff_role='receptionist';
            break;
        default :
            $this->staf_role='null';
            
        }
        
        $data=array(
            //staff number is randomly generated numbers plus first letters of each name
            'staff_first_name'=>  $this->staff_first_name,
            'staff_last_name'=>  $this->staff_last_name,
            'staff_username'=>  $this->staff_username,
            'staff_password'=>  sha1($this->staff_password),
            'staff_email'=>  $this->staff_email,
            'staff_gender'=>  $this->staff_gender,
            'staff_phone_number'=>  $this->staff_phone_number,  
            'access_level'=>  $this->access_level,
            'staff_role'=>  $this->staff_role,
            'staff_username'=>  $this->staff_username,
            'staff_image_name'=>  $this->staff_image_name,
            'staff_image_thumb_name'=>  $this->staff_image_thumb_name
        );
        
        $this->db->where('pk_staff_id',$staff_id );
        
        if($this->db->update($this::DB_TABLE_NAME,$data)){
            
            $this::activity_log('Staff Info Updated');
            return TRUE;
        }else{
            return FALSE;
        }
    }
    
    /**
     * this function is just like the  update function but just that its is used when staffs are 
     * updating their own profile info
     * This time there is no password,gender,and role field 
     * @return boolean
     */
    public function update_account_profile($staff_id){
        
        $data=array(
            //staff number is randomly generated numbers plus first letters of each name
            'staff_first_name'=>  $this->staff_first_name,
            'staff_last_name'=>  $this->staff_last_name,
            'staff_username'=>  $this->staff_username,
            'staff_email'=>  $this->staff_email,
            'staff_phone_number'=>  $this->staff_phone_number,  
            'staff_username'=>  $this->staff_username,
            'staff_image_name'=>  $this->staff_image_name,
            'staff_image_thumb_name'=>  $this->staff_image_thumb_name
        );
        
        $this->db->where('pk_staff_id',$staff_id );
        
        if($this->db->update($this::DB_TABLE_NAME,$data)){
            
            $this::activity_log('Staff Info Updated');
            return TRUE;
        }else{
            return FALSE;
        }
    }
    public function login(){
       //Valdations
       $data= array('staff_username'=>  $this->staff_username,'staff_password'=> $this->staff_password);
       $query= $this->db->get_where($this::DB_TABLE_NAME,$data);
       if($query->num_rows() > 0){ 
           $row=$query->row_array();

           $session_array=array(
               'staff_id'=>$row['pk_staff_id'],
               'staff_name'=>$row['staff_first_name'],
               'access_level'=>$row['access_level'],
               'staff_role'=>$row['staff_role'],
               'is_logged_in'=>TRUE
           );
           //set session data
           $this->session->set_userdata($session_array);
           //return true login success
           return TRUE ;
       }else{
           return FALSE;   
       }
    } 
    
   /**
    * Gets all the rows or columns of the patient_info table
    * @param string $where
    * @param string/int $equal
    * @return string
    */
   public static function get_staff($what,$where,$equal){
       $ci =& get_instance();
       if(!empty($where)&& !empty($equal)){
           $data=array($where=>$equal);
           $query=$ci->db->get_where(self::DB_TABLE_NAME,$data);
       }else{
            return "No parameter";
       }

       if($query->num_rows()>0){
            foreach($query->result() as $staff){
              $result=$staff->$what;
            }

           return $result;

        }else{
            
            return "No match found";
        }
   }
   
   /**
    * Gets all the rows or columns of the patient_info table
    * @param string $where
    * @param string/int $equal
    * @return string
    */
   public static function get_staff_($what,$staff_id){
       $ci =& get_instance();
       
           $data=array('pk_staff_id'=>$staff_id);
           $query=$ci->db->get_where(self::DB_TABLE_NAME,$data);
       

       if($query->num_rows()>0){
            foreach($query->result() as $staff){
              $result=$staff->$what;
            }

           return $result;

        }else{
            
            return "No match found";
        }
   }
   
   
   /**
    * This function get all info of a specific staff using the staff id
    * @param type $staff_id
    * @return string
    */
   
   public static function get_a_staff_info($staff_id){
       $ci =& get_instance();
       $data['pk_staff_id']= $staff_id;
       $query=$ci->db->get_where(self::DB_TABLE_NAME,$data);
       
       if($query->num_rows()>0){
            $result=$query->result();

           return $result;

        }else{
            return "No match found";
        }
       
   }
    
   /**
    * This function returns an array of staff with a common access leve
    * @param type $access_level
    * @return string
    */
   public static function get_staffs($access_level=NULL){
       $ci =& get_instance();
       if(!empty($access_level)){
           $ci->db->where('access_level',$access_level);
           $ci->db->where('active',1);
           $ci->db->order_by('pk_staff_id','asc');
           $query=$ci->db->get(self::DB_TABLE_NAME);
       

       if($query->num_rows()>0){
            $result=$query->result();

           return $result;

        }else{
            return "No match found";
        }
        }else{
            //Parameter not set
            return "No parameter";
       }
   }
   
   public static function get_inactive_staffs(){
       $ci =& get_instance();
       $ci->db->where('active',0); //where staffs are actives
       $ci->db->order_by('pk_staff_id','asc');
       $query=$ci->db->get(self::DB_TABLE_NAME);
       
       return $query->result();
   }
   
   
   public static function reactivate_staff($staff_id){
       $ci =& get_instance();
       
       $ci->db->where('pk_staff_id',$staff_id); //where staffs are actives
       
       $data['active']=1;
       
       $query=$ci->db->update(self::DB_TABLE_NAME,$data);
       
       if ($query) {
           
           return true;   
       }  else {
           return FALSE;
       }
   }
}

