<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lab_records_m extends MY_Model{
    const DB_TABLE_NAME='lab_records';
    const DB_TABLE_PK='pk_id';
    
    public $attending_technician_id;
    public $patient_number;
    public $report_type;
    public $requesting_doctor_id;
    public $requesting_date;
    public $sending_date;
    public $result_description;
    public $recommendations;
   
    
    /**
     * Save the record
     * It decides whether to update or add a record dependin on the argument`s state
     * if $record number is passed then its an update() else its add()
     * 
     * @param string $record_number
     */
    public function save($record_number=NULL){
        if(empty($record_number)){
            $this->add();
            return TRUE;
        }else{
            $this->update($record_number);
        }
    }
    
    /**
     * Adds a record tot the data base
     */
    private function add(){
        $data=array(
       'fk_patient_number'=>  $this->patient_number,
       'fk_attending_technician'=>$this->attending_technician_id,
       'report_type'=>$this->report_type,
       'fk_requesting_doctor_id'=>$this->requesting_doctor_id,
       //'request_date'=>  $this->requesting_date,
       //'sending_date'=>$this->sending_date,
       'result_description'=>$this->result_description,
       'recommendations'=>  $this->recommendations,
        );
        $query=$this->db->insert(self::DB_TABLE_NAME,$data);
        if($query){
            return TRUE;
        }else{ 
           // echo $this->db->_error_message();
            return FALSE;
        }
        
    }
    
 private function update($record_number){
     $data=array(
            'fk_attending_technician'=>$this->attending_technician,
            'report_type'=>$this->report_type,
            'fk_requesting_doctor_id'=>$this->requesting_doctor,
            'requestiong_date'=>  $this->requesting_date,
            'sending_date'=>$this->sending_date,
            'result_description'=>$this->result_description,
            'recommendations'=>  $this->recomendations,

        );
     
     $this->db->where('record_number',$record_number);
     if($this->db->update(self::DB_TABLE_NAME,$data)){
         
         return TRUE;
     }else{
         echo $this->db->_error_message();
         return FALSE;
     }
 }
 
 /**
  * Deletes a record from the data base
  * @param string $record_number
  */
 public function delete($record_number){
     $this->db->where('record_number',$record_number);
     if($this->db->delete(self::DB_TABLE_NAME)){
         $txt = sprintf("Lab record %s has been deleted by %s.",$record_number,  $this->session->userdata('staff_name'));
         $this->activity_log($txt);
         return TRUE;
     }else{
         $txt = sprintf("Lab record %s deletion by %s failed.",$record_number,  $this->session->userdata('staff_name'));
         $this->activity_log($txt);
         return FALSE;
     }
 }

 public function get_records($record_number=NULL){
    if(!empty($record_number)){//return a particular record from the database
     
        $this->db->where('record_number',$record_number);
        $results=$this->db->get(self::DB_TABLE_NAME);
     
    }else{//return every record on the DB
        
        $results=$this->db->get(self::DB_TABLE_NAME);
    }
    
    if($results){
        return $results;
    }else{
        echo $this->db->_error_message();
        return FALSE;
    }
 }
 
 
 
    /**
     * This function gets all the lab reports of a particular patient
     * @param type $patient_number
     * @return results
     */
    public static function get_patient_lab_reports($patient_number){
        $ci =& get_instance();
       
        
        //Join doctor record table and patient_infor table. 
            $ci->db->select('l.pk_id,l.record_number,l.fk_patient_number,l.fk_attending_technician,l.fk_requesting_doctor_id,l.report_type,l.result_description,l.recommendations,l.date_created,p.pt_first_name,p.pt_last_name,p.pt_middle_name,s.pk_staff_id,s.staff_first_name,s.staff_last_name');   
            $ci->db->from('lab_records l '); 
            
            // join patient info table to doctors record table where pt_number is equal to that of the patient info
            $ci->db->join('patient_info p', 'p.pt_number = l.fk_patient_number', 'right');
            
            // jion patient info table to doctors record table where pt_number is equal to that of the patient info
            $ci->db->join('staff s', 's.pk_staff_id=l.fk_attending_technician', 'left');
            $ci->db->where('fk_patient_number',$patient_number);
            $ci->db->order_by('date_created','desc');
            
            //get the result
            $lab_reports= $ci->db->get();
            if($lab_reports->num_rows()>0){
                return $lab_reports->result();
            }else{
                return FALSE; 
            }
     }
     
     /**
      * Gets the last requested lab for patient
      * @param type $patient_number
      * @return type
      */
     public static function get_patient_last_reports_entry($patient_number){
        $ci =& get_instance();
       
        
        //Join doctor record table and patient_infor table. 
            $ci->db->select('*');   
            $ci->db->from('lab_records'); 
            $ci->db->limit(1);
            $ci->db->where('fk_patient_number',$patient_number);
            $ci->db->order_by('date_created','desc');
            
            //get the result
            $lab_reports= $ci->db->get();
            if($lab_reports->num_rows()>0){
                return $lab_reports->result();
            }else{
                return FALSE; 
            }
     }
     
      public static function get_patient_report_by_date($patient_number,$date){
        $ci =& get_instance();
       
        
        //Join doctor record table and patient_infor table. 
            $ci->db->select('*');   
            $ci->db->from('lab_records'); 
            $ci->db->limit(1);
            $ci->db->where('fk_patient_number',$patient_number);
            $ci->db->where('date_created',$date);
            
            //get the result
            $lab_reports= $ci->db->get();
            if($lab_reports->num_rows()>0){
                return $lab_reports->result();
            }else{
                return FALSE; 
            }
     }

    public static function count_total_lab_requests(){
        $ci =& get_instance();
        $ci->db->from('lab_request');
        $total=$ci->db->count_all_results();
        return $total;
    }
 /**
  * Counts all pending lab requests
  * @return int
  * 
  */
 public static function count_pending_lab_requests(){
     $ci =& get_instance();
     $ci->db->where('request_status',0);
     $ci->db->from('lab_request');
        $total=$ci->db->count_all_results();
     
     return $total;
 }
 
 /**
  * Counts all completed lab requests
  * @return int
  * 
  */
 public static function count_completed_lab_requests(){
     $ci =& get_instance();
     $ci->db->where('request_status',1);
     $ci->db->from('lab_request');
        $total=$ci->db->count_all_results();
     
     return $total;
 }
}