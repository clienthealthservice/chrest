<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Doctors_records_m extends MY_Model{
    const DB_TABLE_PK='pk_id';
    const DB_TABLE_NAME='doctors_records';
    
    public $record_number;
    public $diagnosis;
    public $precription;
    public $symptoms;
    public $fk_attending_doctor;
    public $fk_patient_number;
    public $fk_lab_test_number;
    public $date_of_attendance;
    public $status;



    public function create(){
        $this->generate_record_number();
        $data=array(
            'record_number'=> $this->record_number ,
            'diagnosis'=> $this->diagnosis,
            'prescription'=> $this->precription,
            'symptoms'=> $this->symptoms,
            'fk_attending_doctor'=>$this->fk_attending_doctor,
            'fk_patient_number'=> $this->fk_patient_number,
            'fk_lab_test_number'=> $this->fk_lab_test_number,
            'status'=> $this->status,
        );
        
        $query=$this->db->insert(self::DB_TABLE_NAME,$data);
        if($query){
            $this->activity_log('New doctors record create');
            return TRUE;
        }else{
            return FALSE;
        }
        
    }
    private function generate_record_number(){
       $rc_num=random_string('numeric', 5);
       $this->record_number=$rc_num;
   }
    public static function get_patients_records($pt_number){
        $ci =& get_instance();
        
        $ci->db->where('fk_patient_number', html_escape($pt_number));
        $ci->db->order_by('pk_id','desc');
        $query= $ci->db->get(self::DB_TABLE_NAME);
        
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return FALSE;    
        }
    }
    
    /**
     * Gets a particular record from the doctors_record table
     * @param int $record_number
     * @return boolean
     */
    public static function get_record($record_number){
        $ci =& get_instance();
        
        $ci->db->where('record_number', html_escape($record_number));
        
        
        $query= $ci->db->get(self::DB_TABLE_NAME);
        
        if($query->num_rows()>0){
            return $query->result_array();
        }else{
            return FALSE;    
        }
    }
    public function delete($record_id){
        $this->db->where('record_number', $record_id);
        $query= $this->db->delete(self::DB_TABLE_NAME);
        
        if ($query) {
            $this->activity_log('Doctors record'.$record_id.' deleted successfully');
            return TRUE;
            
        }else{
            return FALSE;
        }
    }
    
    

}