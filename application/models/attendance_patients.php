<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class attendance_patients extends MY_Model{
    
    const DB_TABLE_NAME='attendance_patients';
    
    public static function get_attendance($date=NULL){
            $ci =& get_instance();
           if(isset($date) && $date!='all'){ 
             $date=date("Y-m-d",strtotime($date));
           }elseif(empty($date)){
             $date= date('Y-m-d');
           }
            $ci->db->select('*');   
            $ci->db->from('attendance_patients a');
            $ci->db->join('patient_info p', 'p.pt_number = a.fk_pt_number', 'left');
            if($date!='all'){
            $ci->db->like('date_of_attendance',$date,'both');
            }
            //$ci->db->limit(10, 20);
            $query=$ci->db->get();
            
            if($query->num_rows()>0){
                return $query->result();
            }  else {
                return FALSE;
            }
    } 
    
    
    public static function add_attendance($patient_number){
        $ci =& get_instance();
       $data=array(
           'fk_pt_number'=> $patient_number
       );
       
       $query=$ci->db->insert(self::DB_TABLE_NAME,$data);
       if($query){
           //$ci->activity_log('Patient added to attendance');
           return TRUE;  
       }  else {
          return FALSE; 
       }
    } 
    
    
}

