<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Next_of_kin_m extends MY_Model{
    const DB_TABLE_PK='pk_id';
    const DB_TABLE_NAME='patients_next_of_kin';

    public $pt_number;
    public $name;
    public $date_of_birth;
    public $occupation;
    
    public $address;
    public $residence;
    public $nationality;
    public $phone;
    
    
    public  function add(){
        $ci= & get_instance();
        $data=array(
            'name'=>  $this->name,
            'date_of_birth'=> $this->date_of_birth,
            'occupation'=> $this->occupation,
            'address'=>  $this->address,
            'residence'=> $this->residence,
            'nationality'=>  $this->nationality,
            'phone_number'=>$this->phone,
            'patient_number'=>$this->pt_number
        
        );
        
        $query=$this->db->insert($this::DB_TABLE_NAME,$data);
        
        if ($query){
            return TRUE;
        }else{
            return FALSE;
        }
    }
}
    