<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pharmacists_records extends MY_Model{
    const DB_TABLE_PK='pk_id';
    const DB_TABLE_NAME='pharmacist_record';

    
    public $record_number;
    public $attending_pharmacist_id;
    public $attending_doctor_id;
   
    public $doctors_record_number;
    public $patient_number;
    
    public $description;
    
    public $prescription;
    
    public $lab_record_number;
    
    
    
    public $date_issued;
    /**
     * Inserts a new record into the database
     * 
     */
    public function add_record(){
        
        
        //generate the record number
        $this->generate_record_number();
        //assign variables to database table colmnsl
        
        $data=array(
            'record_number'=>  $this->record_number,
            'fk_attending_doctor_id'=> $this->attending_doctor_id,
            'fk_doctor_record_number'=> $this->doctors_record_number,
            'description'=>  $this->description,
            'prescription'=>  $this->prescription,
            'fk_patient_number'=>  $this->patient_number,
            'fk_attending_pharmacist_id'=>  $this->attending_pharmacist_id,
            'fk_lab_record_number'=>  $this->lab_record_number,
            
        );
        
        //do insert query        
        $insert=$this->db->insert($this::DB_TABLE_NAME,$data);
        if($insert){
            //set presvription as issued in the doctors record table
            $this->db->where('record_number',$this->doctors_record_number);
            $data=array('issued'=>1,);
            $this->db->update('doctors_records',$data);
                    
            $this->activity_log('New prescription issued');
            return TRUE;
        }else{
            return FALSE;
        }
        
    }
    
    
    /**
     * Deletes record
     * @param int $record_id
     */
    public function delete_record($record_id){
        $this->db->where('pk_id',$record_id);
        $query=$this->db->delete(self::DB_TABLE_NAME);
        if($query){
            $this->activity_log('Prescription deleted');
            return TRUE;
        }else{
            return FALSE;
        }
    }
    
    /**
     * Updates a record in db
     * @param int $record_id
     */
    private function generate_record_number(){
         $number=random_string('alnum', 8);
         $this->record_number=$number;
    }
    public function update_record($record_id){
         //assign variables to database table colmnsl
         $this->generate_record_number();
        $data=array(
            'record_number'=>  $this->record_number,
            'fk_attending_doctor_id'=> $this->attending_doctor_id,
            'fk_doctor_record_number'=> $this->doctors_record_number,
            'description'=>  $this->description,
            'prescription'=>  $this->prescription,
            'fk_patient_number'=>  $this->patient_number,
            'fk_attending_pharmacist_id'=>  $this->attending_pharmacist_id,
            'fk_lab_record_number'=>  $this->lab_record_number,
        );
        
        //do insert query   
        $this->db->where('pk_id',$record_id);
        $update=$this->db->update($this::DB_TABLE_NAME,$data);
        //check if query was success
        if($update){//success
            $this->activity_log('New prescription issued');
            return TRUE;
        }else{//failure
            return FALSE;
        }
        
    }
    
    /**
     * Reads a record if id is set but returns all record if not set
     * @param int $record_id
     */
    public static function get_record($record_id=NULL){
        $ci =& get_instance();
        $result='';
        //check if record id isset
        if(!empty($record_id)){
            //fetch just a row  
            $ci->db->where('pk_id',$record_id);
            
            $result=$ci->db->get(self::DB_TABLE_NAME);
        }else{
            //fetch all all records of the patient
            $result=$ci->db->get(self::DB_TABLE_NAME);
        }
        return $result;
        
    }
    
    
    public static function get_issued_precriptions($patient_number){
        $ci =& get_instance();
        //check if record_id isset
        if(!empty($patient_number)){
            //Join doctor record table and patient_infor table. 
            $ci->db->select('d.record_number,d.diagnosis,d.prescription,d.symptoms,d.fk_lab_test_number,d.date_recorded,p.pt_number,p.pt_first_name,p.pt_last_name,p.pt_middle_name,p.pt_nationality,s.pk_staff_id,s.staff_first_name,s.staff_last_name');   
            $ci->db->from('doctors_records d '); 
            
            // join patient info table to doctors record table where pt_number is equal to that of the patient info
            $ci->db->join('patient_info p', 'p.pt_number= d.fk_patient_number', 'left');
            // jin patient info table to doctors record table where pt_number is equal to that of the patient info
            $ci->db->join('staff s', 's.pk_staff_id= d.fk_attending_doctor', 'left');
            $ci->db->where('fk_patient_number',$patient_number);
            $ci->db->where('issued',1);
            
            
            //get the result
            $prescriptions= $ci->db->get();
            
            if($prescriptions->num_rows()>0){// check is theres result
                return $prescriptions->result();
                }else{
                    return "Presciptions not found for this patient";
                }
        }else{
            return FALSE;
        }
        
        
    }
    
    public static function get_pending_precriptions($patient_number){
        $ci =& get_instance();
        //check if record_id isset
        if(!empty($patient_number)){
            //Join doctor record table and patient_infor table. 
            $ci->db->select('d.record_number,d.diagnosis,d.prescription,d.symptoms,d.fk_lab_test_number,d.date_recorded,p.pt_number,p.pt_first_name,p.pt_last_name,p.pt_middle_name,p.pt_nationality,s.pk_staff_id,s.staff_first_name,s.staff_last_name');   
            $ci->db->from('doctors_records d '); 
            
            // join patient info table to doctors record table where pt_number is equal to that of the patient info
            $ci->db->join('patient_info p', 'p.pt_number= d.fk_patient_number', 'left');
            // jin patient info table to doctors record table where pt_number is equal to that of the patient info
            $ci->db->join('staff s', 's.pk_staff_id= d.fk_attending_doctor', 'left');
            $ci->db->where('fk_patient_number',$patient_number);
            $ci->db->where('issued',0);
            
            
            //get the result
            $prescriptions= $ci->db->get();
            
            if($prescriptions->num_rows()>0){// check is theres result
                return $prescriptions->result();
                }else{
                    return "Presciptions not found for this patient";
                }
        }else{
            return FALSE;
        }
        
        
    }

}
