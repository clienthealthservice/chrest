<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lab_request_m extends MY_Model{
    const DB_TABLE_NAME='lab_request';
    const DB_TABLE_PK='request_id';
    
    
    public $request_type;
    public $requesting_staff_id;
    public $requesting_date;
    public $patient_number;
    public $patient_name;
    public $request_status;
    
    
    /**
     * This gets all pending requests
     * @return array
     */
    public static function get_pending_reports(){
        $ci =& get_instance();
        $ci->db->where('request_status',0);
        $results=$ci->db->get(self::DB_TABLE_NAME);
        
        if ($results->num_rows()>0) {
            return $results->result();
        }  else {
            return 'No pending request available';
        }
        
    }
    
    /**
     * This gets all pending requests
     * @return array
     */
    public static function set_request_status_to_1($request_id){
        $ci =& get_instance();
        $ci->db->where('request_id',$request_id);
        $data=array('request_status'=>1);
        $results=$ci->db->update(self::DB_TABLE_NAME,$data);
        
        if ($results) {
            return TRUE;
        }  else {
            return FALSE;
        }
        
    }
   
    
}