<?php $ci =& get_instance();?>
  <div class="row">
   <a role="button" href="patient/add" class="btn btn-warning pull-right"><i class="fa fa-wheelchair"></i> Add a Patient</a>
 </div>


<table id="chres_table" class="table table-striped">
        <thead>
            <tr>
                <th>Name</th>
                <th>Patient Number</th>
                <th>Phone</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
<?php foreach($patients as $patient){?>
       <tr id="<?php echo $patient->pk_pt_id?>">
                  <td> <?php echo $patient->pt_first_name ." ". $patient->pt_last_name ?> </td>
                  <td><?php echo $patient->pt_number?> </td>
                  <td><?php  echo $patient->pt_mobile ?> </td>
                 <td>
                     <div class='btn-group btn-group-sm' role='group' >
                         <a href='<?php echo site_url($ci->router->fetch_class()."/patient/edit/".$patient->pt_number)?>' class='btn btn-info'>Edit Info</a>
                         <a href='<?php echo site_url($ci->router->fetch_class()."/patient/delete/".$patient->pt_number)?>' class='btn btn-danger'>Delete</a>
                         
                     </div>
                 </td>
        </tr>
<?php } ?>
        </tbody>
  </table>