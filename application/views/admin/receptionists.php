<div class="panel panel-default">
  <div class="panel-body">
   <a role="button" href="<?php echo site_url('admin/staff?r=6')?>" class="btn btn-warning btn-lg pull-right"><i class="fa fa-plus-circle"></i>Add Staff</a>
 </div>
</div>

<?php $doctors= Staff_m::get_staffs(6)// Argument value=2 Bcuz doctors have access level of 2?>

<table id="chres_table" class="table table-striped">
        <thead>
            <tr>
                <th>Name</th>
                <th>Email</th>
                <th>Phone</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
<?php foreach($doctors as $doctor){?>
       <tr id="<?php echo $doctor->pk_staff_id?>">
                  <td> <?php echo $doctor->staff_first_name ." ". $doctor->staff_last_name ?> </td>
                  <td><?php echo $doctor->staff_email?> </td>
                  <td><?php  echo $doctor->staff_phone_number ?> </td>
                 <td>
                     <div class='btn-group btn-group-sm' role='group' >
                         <a href='<?php echo site_url($this->router->fetch_class()."/staff/".$doctor->pk_staff_id)?>' class='btn btn-info'>Edit Info</a>
                         <a href='<?php echo site_url($this->router->fetch_class()."/staff_deactivate/".$doctor->pk_staff_id)?>' class='btn btn-danger'>Delete</a>
                         
                     </div>
                 </td>
        </tr>
<?php } ?>
        </tbody>
  </table>