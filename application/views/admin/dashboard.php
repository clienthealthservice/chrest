                <!-- /.row -->

                <div class="row">
                    <div class="col-lg-3 col-md-6">
                        <a>
                        <div class="info-box ">
                            <span class="info-box-icon bg-aqua"><i class="fa fa-user-md"></i></span>
                            <div class="info-box-content">
                              <span class="info-box-text">Doctors</span>
                              <span class="info-box-number"><?php Admin::count_staff(2) ?></span>
                              
                            </div><!-- /.info-box-content -->
                            
                          </div><!-- /.info-box -->
                        </a>
                        
                    </div>
                    
                    <div class="col-lg-3 col-md-6">
                        <a >
                        <div class="info-box">
                            <span class="info-box-icon bg-green"><i class="fa fa-medkit"></i></span>
                            <div class="info-box-content">
                              <span class="info-box-text">Pharmacists</span>
                              <span class="info-box-number"><?php Admin::count_staff(3) ?></span>
                              
                            </div><!-- /.info-box-content -->
                            
                          </div><!-- /.info-box -->
                        </a>
                        
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <a >
                        <div class="info-box ">
                            <span class="info-box-icon bg-yellow"><i class="fa fa-plus-square"></i></span>
                            <div class="info-box-content">
                              <span class="info-box-text">Nurses</span>
                              <span class="info-box-number"><?php Admin::count_staff(4) ?></span>
                              
                            </div><!-- /.info-box-content -->
                            
                          </div><!-- /.info-box -->
                        </a>
                        
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <a >
                        <div class="info-box ">
                            <span class="info-box-icon bg-blue"><i class="fa fa-wrench"></i></span>
                            <div class="info-box-content">
                              <span class="info-box-text">Lab Technicians</span>
                              <span class="info-box-number"><?php Admin::count_staff(5) ?></span>
                              
                            </div><!-- /.info-box-content -->
                            
                          </div><!-- /.info-box -->
                        </a>
                        
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <a >
                        <div class="info-box ">
                            <span class="info-box-icon bg-fuchsia"><i class="fa fa-plus-square"></i></span>
                            <div class="info-box-content">
                              <span class="info-box-text">Receptionist</span>
                              <span class="info-box-number"><?php Admin::count_staff(6) ?></span>
                              
                            </div><!-- /.info-box-content -->
                            
                          </div><!-- /.info-box -->
                        </a>
                        
                    </div>
                    
                    <div class="col-lg-3 col-md-6">
                        <a >
                        <div class="info-box ">
                            <span class="info-box-icon bg-red"><i class="fa fa-wheelchair"></i></span>
                            <div class="info-box-content">
                              <span class="info-box-text">Patients</span>
                              <span class="info-box-number"><?php echo Patient_m::count_patients() ?></span>
                              
                            </div><!-- /.info-box-content -->
                            
                          </div><!-- /.info-box -->
                        </a>
                        
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <a href="staff_inactive" >
                        <div class="info-box ">
                            <span class="info-box-icon bg-fuchsia"><i class="fa fa-plus-square"></i></span>
                            <div class="info-box-content">
                              <span class="info-box-text">Inactive</span>
                              <span class="info-box-number"><?php Admin::count_inactive_staff(6) ?></span>
                              
                            </div><!-- /.info-box-content -->
                            
                          </div><!-- /.info-box -->
                        </a>
                        
                    </div>
                   
                </div>
                <!-- /.row -->

                

                <div class="row">
                    <div class="col col-md-8">
                        <div class="form-group">
                    
                    
                    
                    
                    <div class="box box-info">
                        <div class="box-header with-border">
                          <h3 class="box-title">Attendance</h3>
                          <div class="row">
                              <div class="col col-md-4 pull-right">
                              <div class="input-group ">
                                <div class="input-group-addon">
                                  <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" class="form-control pull-right" id="date_range">
                                <div class="input-group-addon" onclick="get_attendance()">
                                   <i class="">Go</i>
                                </div>
                              </div><!-- /.input group -->
                          </div> 
                        </div>
                  
                  
                   
                    
                    
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div id="attendance_table" class="table-responsive">
                    <table class="table no-margin">
                      <thead>
                        <tr>
                          <th>Patient Num</th>
                          <th>Name</th>
                          <th>Gender</th>
                          <th>Date</th>
                        </tr>
                      </thead>
                      <tbody>
                          <?php

                            $attendees=attendance_patients::get_attendance();
                            if(is_array($attendees)){
                                foreach( $attendees as $attendee){ ?>
                                    <tr>
                                        <td><a href="pages/examples/invoice.html"><?php echo $attendee->fk_pt_number ?></a></td>
                                        <td><?php echo $attendee->pt_first_name.' '.$attendee->pt_last_name ?></td>
                                        <td><span class="label label-success"><?php echo $attendee->pt_gender ?> </span></td>
                                        <td><?php  echo $attendee->date_of_attendance ?></td>
                                    </tr>
                          <?php 
                                     
                                }  

                            }
  
                  ?>
                        
                      </tbody>
                    </table>
                  </div><!-- /.table-responsive -->
                </div><!-- /.box-body -->
               
              </div>
                  </div>
                    </div>
                    
                    <div class="col-lg-4">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-clock-o fa-fw"></i> Activity Log</h3>
                            </div>
                            <div class="panel-body">
                                <div class="list-group">
                                    <?php 
                                    $activities=MY_Model::get_activity_log();
                                    if ($activities!=NULL){ 
                                        foreach ($activities as $activity){?>
                                            <a href="#" class="list-group-item">
                                                
                                                <span class="badge">
                                                    <?php 
                                                    $post_date = strtotime($activity->time);
                                                    $now = time();
                                                    echo timespan($post_date, $now).' ago';
                                                    ?>
                                                </span>
                                                <i class="fa fa-fw fa-calendar"></i> <?php echo $activity->activity ?>
                                            </a>  
                                <?php  }}else{
                                    echo "No match found for selected date";
                                } ?>
                                    
                                </div>
                                >
                            </div>
                        </div>
                    </div>
                    
                </div>
                <!-- /.row -->

           