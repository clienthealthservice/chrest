<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$ci =& get_instance();
$class=$ci->router->fetch_class();
?>

<?php echo validation_errors(); ?>
<h3 class="text-center">New Patient</h3>
<div>
    <div class="form">
<?php echo form_open($class."/patient/add");?>

    <div class="form-group">  
<?php echo form_label("Patient First Name*", "pt_f_name");?>
<?php echo form_input(array('type'=>'text', 'name'=>'pt_f_name','placeholder'=>'Firstname','class'=>'form-control',)); ?>
    </div>



    <div class="form-group">  
<?php echo form_label("Patient Middle Name", "pt_m_name");?>
<?php echo form_input(array('type'=>'text', 'name'=>'pt_m_name','placeholder'=>'Middle name','class'=>'form-control')); ?>
    </div>

    <div class="form-group">  
<?php echo form_label("Patient last name*", "pt_l_name");?>
<?php echo form_input(array('type'=>'text', 'name'=>'pt_l_name','placeholder'=>'Lastname','class'=>'form-control')); ?>
    </div>

    <div class="form-group">  
        <div class="row">
            <div class="col-lg-2">
                <?php echo form_label("Patient Gender *", "pt_gender");?>
            </div>
            <div class="col-lg-2">
                <div class="row">
                    <?php echo form_radio(array('type'=>'radio', 'id'=>'pt_male','name'=>'pt_gender','value'=>'male'))." Male"; ?>
                </div>
                <div class="row">
                    <?php echo form_radio(array('type'=>'radio', 'name'=>'pt_female','name'=>'pt_gender','value'=>'female'))." Female"; ?>
                </div>
            </div>
            
        </div>
    </div>

    <div class="form-group">  
<?php echo form_label("Occupation", "pt_occupation");?>
<?php echo form_input(array('type'=>'text', 'name'=>'pt_occupation','placeholder'=>'Occupation','class'=>'form-control')); ?>
    </div>
    <div class="form-group">  
<?php echo form_label("Address", "pt_l_name");?>
<?php echo form_input(array('type'=>'text', 'name'=>'pt_address','placeholder'=>'Address','class'=>'form-control')); ?>
    </div>

    
    <div class="row">
        <div class="col-md-4">
            <div class="form-group">  
        <?php echo form_label("Date of birth *", "pt_date_of_birth");?>
        <?php echo form_input(array('type'=>'date', 'name'=>'pt_date_of_birth','class'=>'form-control')); ?>
            </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">  
        <?php echo form_label("Place of Birth*", "pt_place_of_birth");?>
        <?php echo form_input(array('type'=>'text', 'name'=>'pt_place_of_birth','placeholder'=>'place of birth','class'=>'form-control')); ?>
          </div>  
        </div>
        <div class="col-md-4">
            <div class="form-group">  
        <?php echo form_label("Place of Residence*", "pt_place_of_residence");?>
        <?php echo form_input(array('type'=>'text', 'name'=>'pt_place_of_residence','placeholder'=>'Eg. New Tafo','class'=>'form-control')); ?>
            </div>
        </div>
     
    </div>

    

    <div class="form-group">  
<?php echo form_label("Nationality*", "pt_nationality");?>
<?php echo form_input(array('type'=>'text', 'name'=>'pt_nationality','placeholder'=>'Eg. Ghanaian','class'=>'form-control')); ?>
    </div>

    <div class="form-group">  
<?php echo form_label("Mobile Number*", "pt_mobile");?>
<?php echo form_input(array('type'=>'text', 'name'=>'pt_mobile','placeholder'=>'Eg. 0267979001','class'=>'form-control')); ?>
    </div>
    <div class="form-group">  
<?php echo form_label("First Attendance*", "pt_first_attendance");?>
<?php echo form_input(array('type'=>'date', 'name'=>'pt_first_attendance','class'=>'form-control')); ?>
    </div>
        
        
        <section class="section">
            <h3>Next Of Kin</h3>
            
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">  
                <?php echo form_label("Name *", "pt_nok_name");?>
                <?php echo form_input(array('type'=>'text', 'name'=>'pt_nok_name','class'=>'form-control')); ?>
                    </div>

                    <div class="form-group">  
                <?php echo form_label("Date of Birth *", "pt_nok_date_of_birth");?>
                <?php echo form_input(array('type'=>'date', 'name'=>'pt_nok_date_of_birth','class'=>'form-control')); ?>
                    </div>

                    <div class="form-group">  
                <?php echo form_label(" Occupation *", "pt_nok_occupation");?>
                <?php echo form_input(array('type'=>'text', 'name'=>'pt_nok_occupation','class'=>'form-control')); ?>
                    </div>

                    <div class="form-group">  
                <?php echo form_label("Address ", "pt_nok_address");?>
                <?php echo form_input(array('type'=>'address', 'name'=>'pt_nok_address','class'=>'form-control')); ?>
                    </div>
                    <div class="form-group">  
                <?php echo form_label(" Residence *", "pt_nok_residence");?>
                <?php echo form_input(array('type'=>'text', 'name'=>'pt_nok_residence','class'=>'form-control')); ?>
                    </div>

                    <div class="form-group">  
                <?php echo form_label(" Phone *", "pt_nok_phone");?>
                <?php echo form_input(array('type'=>'text', 'name'=>'pt_nok_phone','class'=>'form-control')); ?>
                    </div>


                    <div class="form-group">  
                <?php echo form_label(" Nationality *", "pt_nok_occupation");?>
                <?php echo form_input(array('type'=>'text', 'name'=>'pt_nok_nationality','class'=>'form-control')); ?>
                    </div>
                    
                </div>
            </div>
            
            
        </section>
    
    <div class="form-group"?>
<?php 
echo form_input(array('type'=>'submit', 'name'=>'pt_submit',"value"=>'Add','class'=>'btn btn-success btn-lg'));
?>
    </div>
<?php echo form_close();?>
        
</div>
</div>