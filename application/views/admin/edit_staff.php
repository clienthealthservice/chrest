
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$ci =& get_instance();
$class=$ci->router->fetch_class();
?>

<?php echo validation_errors(); ?>
<h3> New Staff</h3>

<div>
    <div class="form">
<?php echo form_open($class."/staff/".$staff_info[0]->pk_staff_id);?>
        
        
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">  
            <?php echo form_label("First Name <sup>*</sup>", "stf_f_name");?>
            <?php echo form_input(array('type'=>'text', 'name'=>'stf_f_name','placeholder'=>'Firstname','class'=>'form-control','value'=>$staff_info[0]->staff_first_name)); ?>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">  
            <?php echo form_label("Last name*", "stf_l_name");?>
            <?php echo form_input(array('type'=>'text', 'name'=>'stf_l_name','placeholder'=>'Lastname','class'=>'form-control', 'value'=>$staff_info[0]->staff_last_name)); ?>
                </div>
            </div>
        </div>
    

    
        <div class="row">
            <div class="col-sm-2">
                <div class="form-group">  
            <?php echo form_label("Username *", "stf_username");?>
            <?php echo form_input(array('type'=>'text', 'name'=>'stf_username','placeholder'=>'Username','class'=>'form-control','value'=>$staff_info[0]->staff_username,'required'=>'')); ?>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">  
            <?php echo form_label("Email *", "stf_email");?>
            <?php echo form_input(array('type'=>'email', 'name'=>'stf_email','required'=>'','placeholder'=>'Username','class'=>'form-control','value'=>$staff_info[0]->staff_email,'required'=>'')); ?>
                </div>
                
            </div>
            <div class="col-sm-2">
                <div class="form-group">  
            <?php echo form_label("New Password *", "stf_password");?>
            <?php echo form_input(array('type'=>'password', 'name'=>'stf_password','placeholder'=>'Password','class'=>'form-control','required'=>'')); ?>
                </div>
            </div>
            <div class="form-group">  
                <div class="row">
                    <div class="col-lg-2">
                        <?php echo form_label(" Gender *", "stf_gender");?>
                    </div>
                    <div class="col-lg-2">
                        <?php $gender=$staff_info[0]->staff_gender;?>
                        <div class="row">
                            <input type="radio" value="male" name='stf_gender' <?php if($gender=='male'){echo 'checked=checked';}?>> Male
                        </div>
                        <div class="row">
                            <input type="radio"  value="female" name='stf_gender' <?php if($gender=='female'){echo 'checked=checked';}?>> Female
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-sm-2">
                <label for="stf_role" > Role <sup>*</sup></label>
                  <div class="form-group">
                    <?php $role=$staff_info[0]->staff_gender;?>
                        <select name="stf_role" class="form-control" required="">
                            <option value="1" <?php if($role==1){echo 'selected=selected';}?>>Admin</option>
                            <option value="2" <?php if($role==2){echo 'selected=selected';}?>>Doctor</option>
                            <option value="3" <?php if($role==3){echo 'selected=selected';}?>>Pharmacist</option>
                            <option value="4" <?php if($role==4){echo 'selected=selected';}?>>Nurse</option>
                            <option value="5" <?php if($role==5){echo 'selected=selected';}?>>Lab Technician</option>
                            <option value="6" <?php if($role==6){echo 'selected=selected';}?>>Receptionist</option>
                            </select>
                </div>
            </div>
          
            
        </div>
    
    

    
        <div class="row">
            <div class='col-sm-2'>
                <div class="form-group">  
            <?php echo form_label("Mobile Number*", "stf_mobile");?>
            <?php echo form_input(array('type'=>'text', 'name'=>'stf_mobile','placeholder'=>'Eg. 0267979001','class'=>'form-control','value'=>$staff_info[0]->staff_phone_number)); ?>
                </div>
            </div>
        </div>
        
    
    
    
    <div class="form-group">
        <?php 
        echo form_input(array('type'=>'submit', 'name'=>'stf_submit',"value"=>'Submit','class'=>'btn btn-success btn-lg'));
        ?>
    </div>
<?php echo form_close();?>
        
</div>
</div>
