
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$ci =& get_instance();
$class=$ci->router->fetch_class();

if($this->input->get('r')){
    $role_number= $this->input->get('r');  
}  else {
    $role_number=none;
}
?>

<?php echo validation_errors(); ?>
<h3> New Staff</h3>

<div>
    <div class="form">
<?php echo form_open_multipart($class."/staff");?>
        
        
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">  
            <?php echo form_label("First Name <sup>*</sup>", "stf_f_name");?>
            <?php echo form_input(array('type'=>'text', 'name'=>'stf_f_name','placeholder'=>'Firstname','class'=>'form-control','value'=> set_value('stf_f_name',$this->input->post('stf_f_name')))); ?>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">  
            <?php echo form_label("Last name*", "stf_l_name");?>
            <?php echo form_input(array('type'=>'text', 'name'=>'stf_l_name','placeholder'=>'Lastname','class'=>'form-control','value'=> set_value('stf_l_name',$this->input->post('stf_l_name')))); ?>
                </div>
            </div>
        </div>
    

    
        <div class="row">
            <div class="col-sm-2">
                <div class="form-group">  
            <?php echo form_label("Username *", "stf_username");?>
            <?php echo form_input(array('type'=>'text', 'name'=>'stf_username','placeholder'=>'Username','class'=>'form-control','value'=> set_value('stf_username',$this->input->post('stf_username')),'required'=>'')); ?>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="form-group">  
            <?php echo form_label("Email *", "stf_email");?>
            <?php echo form_input(array('type'=>'email', 'name'=>'stf_email','required'=>'','placeholder'=>'Username','class'=>'form-control','value'=> set_value('stf_email',$this->input->post('stf_email')),'required'=>'')); ?>
                </div>
                
            </div>
            <div class="col-sm-2">
                <div class="form-group">  
            <?php echo form_label("Password *", "stf_password");?>
            <?php echo form_input(array('type'=>'password', 'name'=>'stf_password','placeholder'=>'Password','class'=>'form-control','value'=> set_value('stf_password',$this->input->post('stf_password')),'required'=>'')); ?>
                </div>
            </div>
            <div class="col-sm-2">
                
                    
                        <?php echo form_label("Gender *", "stf_gender");?>
                    
                        <div class="row">
                            <?php echo form_radio(array('type'=>'radio', 'id'=>'stf_male','name'=>'stf_gender','value'=>'male'))." Male"; ?>
                        </div>
                        <div class="row">
                            <?php echo form_radio(array('type'=>'radio', 'name'=>'stf_female','name'=>'stf_gender','value'=>'female'))." Female"; ?>
                        </div>
                    

               
            </div>
            <div class="col-sm-2">
              
                <div class="form-group">
                    <?php
                    if(is_int($role_number)){
                        echo form_label("Role <sup>*</sup>", "stf_role");
                     $options=array(
                         '1'=>'Admin',
                         '2'=>'Doctor',
                         '3'=>'Pharmacist',
                         '4'=>'Nurse',
                         '5'=>'Lab Technician',
                         '6'=>'Receptionist'
                     );   
                     $attributes="class='form-control' required";
                     echo form_dropdown('stf_role', $options,'6',$attributes);
                    }else{ 
                      
                        echo form_input(array( 'name'=>'stf_role','type'=>'hidden','value'=>$role_number,'class'=>'form-control')); 
                        
                    } 
                 ?>
                 </div>
            </div>
            
        </div>
    
    

    
        <div class="row">
            <div class='col-sm-2'>
                <div class="form-group">  
            <?php echo form_label("Mobile Number*", "stf_mobile");?>
            <?php echo form_input(array('type'=>'text', 'name'=>'stf_mobile','placeholder'=>'Eg. 0267979001','class'=>'form-control','value'=> set_value('stf_mobile',$this->input->post('stf_mobile')))); ?>
                </div>
            </div>
            <div class='col-sm-4'>
                <div class="form-group">  
            <?php echo form_label("Image*", "stf_image");?>
            <?php echo form_upload(array('type'=>'text', 'name'=>'stf_image','placeholder'=>'Eg. 0267979001','value'=> set_value('stf_image',$this->input->post('stf_mobile')))); ?>
                </div>
            </div>
        </div>
    
    
    <div class="form-group">
        <?php 
        echo form_input(array('type'=>'submit', 'name'=>'stf_submit',"value"=>'Add','class'=>'btn btn-success btn-lg'));
        ?>
    </div>
<?php echo form_close();?>
        
</div>
</div>
