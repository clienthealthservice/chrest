<?php $staff= Staff_m::get_inactive_staffs()// Argument value=2 Bcuz doctors have access level of 2?>

<table id="chres_table" class="table table-striped">
        <thead>
            <tr>
                <th>Name</th>
                <th>Email</th>
                <th>Phone</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
<?php foreach($staff as $staff){?>
       <tr id="<?php echo $staff->pk_staff_id?>">
                  <td> <?php echo $staff->staff_first_name ." ". $staff->staff_last_name ?> </td>
                  <td><?php echo $staff->staff_email?> </td>
                  <td><?php  echo $staff->staff_phone_number ?> </td>
                 <td>
                     <div class='btn-group btn-group-sm' role='group' >
                         <a href='<?php echo site_url($this->router->fetch_class()."/reactivate_staff/".$staff->pk_staff_id)?>' class='btn btn-danger' > Re-activate </a>
                     </div>
                 </td>
        </tr>
<?php } ?>
        </tbody>
  </table>