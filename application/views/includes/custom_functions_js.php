<script>
//Remove patient function
        
        function remove_patient(pt_num){
             
            if (confirm('This action will delete patient')){
                    $.ajax({
                        url:"<?php echo site_url(); ?>" + "/receptionist/deactivate_pt/"+pt_num, 
                        success: function(result){
                        $("#msg").html('<div class="alert alert-info">Patient number with  number '+pt_num+' removed from records</div>');
                        $('#tr_'+pt_num).slideLeft(500);
                        },
                        error: function(error){
                          $('#msg').html("<div class='alert alert-info'>Sorry</div>");
                        }
                    });
            }
        }
        
        
        //This function sets a doctors request to completed
        
        function accept_lab_request(request_id){
             
            if (confirm('Are you sure to accept this request')){
                    $.ajax({
                        url:"<?php echo site_url(); ?>" + "/lab_technician/accept_request/"+request_id, 
                        success: function(result){
                        $("#msg").html('<div class="alert alert-info">Request  number '+request_id+' accepted</div>');
                        $('#request_'+request_id).fadeOut(500);
                        },
                        error: function(error){
                          $('#msg').html("<div class='alert alert-info'>Sorry</div>");
                        }
                    });
            }
        }
        
        //Remove patient function
        
        function save_lab_request(){
         $('#lab_report_requested').submit(function(event){
                event.preventDefault();
            if (confirm('Confirm Submition?')){
                    $.ajax({
                        url:"<?php echo site_url(); ?>" + "/lab_technician/save_report/",
                        type: 'POST',
                        data:{
                            requesting_doctor:$('#requesting_doctor').val(), 
                            report_type:$('#report_type').val(),
                            result_description:$('#result_description').val(),
                            recommendation:$('#recommendation').val(),
                            save_report:$('#save_report').val(),
                            attending_technician_id:$('#attending_technician_id').val()
                        },
                        beforeSend: function(){
                                    $("#form-inputs").slideUp();
                                     $("button.btn").html("Saving...");
                                   },
                        success: function(result){
                        if(results=='successfull'){
                            $("#msg").html('<div class="alert alert-info">Lab report saved successfully</div>');
                        }else{
                            $("#msg").html(result);
                            $("#form-inputs").slideDown();
                        }
                        
                        
                        },
                        error: function(error){
                            $("#form-inputs").slideDown();
                            $("button.btn").html("Save");
                          $('#msg').html("<div class='alert alert-info'>Sorry</div>");
                        }
                    });
            }
          });
        }
  </script>   