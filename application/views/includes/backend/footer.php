<!-- Footer-->
</div>
<!-- jQuery -->
    
    <script src="<?php echo base_url('assets/plugins/jQuery/jQuery-1.12.0.min.js')?>"></script>
    <script src="<?php echo base_url('assets/plugins/jQueryUI/jquery-ui.min.js')?>"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
      $.widget.bridge('uibutton', $.ui.button);
    </script>
    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url('assets/js/bootstrap.min.js')?>"></script>
    
    <script src="<?php echo base_url('assets/js/plugins/tinymce/tinymce.min.js')?>"> </script>
    <script type="text/javascript">
  tinymce.init({
    selector: 'textarea'
  });
  </script>
    <!-- Plugins -->
    <script src="<?php echo base_url('assets/plugins/sparkline/jquery.sparkline.min.js"')?>"> </script>
    <script src="<?php echo base_url('assets/js/plugins/ui-checkbox/checkbox.js')?>"></script>
    <script src="<?php echo base_url('assets/js/plugins/datatables/datatables.min.js')?>"></script>
    <script src="<?php echo base_url('assets/js/plugins/datepicker/datepicker.min.js')?>"></script>
    <script src="<?php echo base_url('assets/js/plugins/daterangepicker/daterangepicker.js')?>"></script>
    
    <script src="<?php echo base_url('assets/js/plugins/datatables/dataTables.bootstrap.min.js')?>"></script>
    <script src="<?php echo base_url('assets/js/plugins/datatables/datatableButtons.min.js')?>"></script>
    <script src="<?php echo base_url('assets/js/plugins/datatables/datatableButtonBootstrap.min.js')?>"></script>
    <script src="<?php echo base_url('assets/js/plugins/datatables/datatableButtonFlash.min.js')?>"></script>
    <script src="<?php echo base_url('assets/js/plugins/datatables/jszip.min.js')?>"></script>
    <script src="<?php echo base_url('assets/js/plugins/datatables/pdfmake.min.js')?>"></script>
    <script src="<?php echo base_url('assets/js/plugins/datatables/vfs_font.min.js')?>"></script>
    <script src="<?php echo base_url('assets/js/plugins/datatables/buttons.html5.min.js')?>"></script>
    
    <script src="<?php echo base_url('assets/js/plugins/datatables/buttons.print.min.js')?>"></script>
    
     <!-- jvectormap -->
    <script src="<?php echo base_url('assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')?>"></script>
    <script src="<?php echo base_url('assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')?>"></script>
    
    <script src="<?php echo base_url('assets/plugins/knob/jquery.knob.js')?>"></script>
    <!-- daterangepicker -->
    <script src="<?php echo base_url('assets/js/plugins/daterangepicker/moment.min.js')?>"></script>
    <script src="<?php echo base_url('assets/plugins/daterangepicker/daterangepicker.js')?>"></script>
    <!-- date picker -->
    <script src="<?php echo base_url('assets/plugins/datepicker/bootstrap-datepicker.js')?>"></script>
    
    
    
    <!-- date picker -->
    
    <script src="<?php echo base_url('assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')?>"></script>
    <!-- slimscroll -->
    <script src="<?php echo base_url('assets/plugins/slimScroll/jquery.slimscroll.min.js')?>"></script>
    
    <!-- Custom JS -->
     <script src="<?php echo base_url('assets/js/plugins/morris/morris-data.js')?>"></script>
    <script src="<?php echo base_url('assets/js/app.min.js')?>"></script>
    <script src="<?php echo base_url('assets/js/pages/dashboard.js')?>"></script>
    <script src="<?php echo base_url('assets/js/demo.js')?>"></script>
    
    <script>
        //DataTables function
    $('#chres_table').DataTable(
            
            );
    
    //Date range picker
        $('#date_range').datepicker();
        //Date range picker
        $('[type]=date').datepicker();
    
</script>
<script>
    //fill the lab request model with the patient id on click
    $('a.lab_request').click(function() {
        $('#mdl_pt_name').val($(this).data('name'));
        $('#mdl_pt_num').val($(this).data('number')); 
    });
</script>
<script>
    //fill the lab repot modal with the patient id on click
    $('a.generate_report').click(function() {
        console.log($(this).data('request_type'));
        $('#patient_name').val($(this).data('patient_name'));
        $('#patient_num').val($(this).data('patient_number'));
        $('#request_id').val($(this).data('request_id'));
        $('#report_type').val($(this).data('request_type'));
        
        $('#requesting_doctor').val($(this).data('request_doctor_id'));
        $('#requesting_doctor_name').val($(this).data('request_doctor_name'));
    });
</script>

<script>
    //Funuction Calls
    save_lab_request();
    
    //switches
    //$("[name='nhis']").bootstrapSwitch();
   
   $($("#nhis")).click(function(){
       console.log("hyyy");
       if($("#nhis").is(":checked")){
            $("#nhis_details").slideDown();
        }else{
            $("#nhis_details").slideUp();
        }
   })
        
    
</script>

</body>

</html>
