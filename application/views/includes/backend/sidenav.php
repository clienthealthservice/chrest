<!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
                <img src="<?php echo base_url('assets/uploads/images/'.$staff_image_thumb_name)  ?>" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p><?php echo $staff_name ?></p>
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <li class="active treeview">
               <?php if($access_level==1){?>
                    <li>
                        <a href="<?php echo site_url($ci->router->fetch_class()."/dashboard");?>"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                    </li>
                    <?php } ?>
                    <?php if($access_level==6){?>
                    <li>
                        <a href="<?php echo site_url('receptionist/');?>" data-toggle="collapse" data-target="#receptionist"><i class="fa fa-wheelchair"></i> Receptionist </a>
                        
                    </li>
                    <?php } ?>
                    
                    <?php if($access_level==4){?>
                    <li>
                        <a href="<?php echo site_url('nurse/');?>" data-toggle="collapse" data-target="#nurse"> <i class="fa fa-user-md"></i>  Nurse </a>
                        
                    </li>
                    <?php }?>
                    
                    <?php if($access_level==2){?>
                    <li>
                        <a href="<?php echo site_url('doctor/');?>" data-toggle="collapse" data-target="#doctor"><i class="fa fa-user-md"></i> Doctor</a>
                        
                    </li>
                    <?php } ?>
                    
                    <?php if($access_level==3){?>
                    <li>
                        <a href="<?php echo site_url('pharmacist/');?>" data-toggle="collapse" data-target="#pharmacist"> <i class="fa fa-medkit"></i> Pharmacist</a>
                        
                    </li>
                    <?php }?>
                    <?php if($access_level==5){?>
                    <li>
                        <a href="<?php echo site_url('lab_technician/dashboard');?>" data-toggle="collapse" data-target="#labTech">
                            <i class="fa fa-tachometer" aria-hidden="true"></i> Dashboard </a>
                    </li>
                    <li>
                        <a href="<?php echo site_url('lab_technician/patients');?>" data-toggle="collapse" data-target="#labTech">
                            <i class="fa fa-wrench"></i> Patients</a>
                        
                    </li>
                    <?php } ?>
                    
                    
                    
                    <?php if ($access_level==1){?>
                        <li>
                            <a href="<?php echo site_url('admin/patient')?>" data-toggle="collapse" data-target="#receptionist">
                                <i class="fa fa-wheelchair"></i> Patient 
                            </a>   
                        </li>
                        <li>
                            <a href="<?php echo site_url('admin/receptionist')?>" data-toggle="collapse" data-target="#receptionist">
                                <i class="fa fa-wheelchair"></i> Receptionist 
                            </a>   
                        </li>
                        
                        <li>
                            <a href="<?php echo site_url('admin/lab_technician')?>" data-toggle="collapse" data-target="#receptionist">
                                <i class="fa fa-wrench"></i> Lab Technician 
                            </a>   
                        </li>
                        <li>
                            <a href="<?php echo site_url('admin/nurse')?>" data-toggle="collapse" data-target="#receptionist">
                                <i class="fa fa-plus-square"></i> Nurse 
                            </a>   
                        </li>
                        <li>
                            <a href="<?php echo site_url('admin/pharmacist')?>" data-toggle="collapse" data-target="#receptionist">
                                <i class="fa fa-medkit"></i> Pharmacist 
                            </a>   
                        </li>
                        <li>
                            <a href="<?php echo site_url('admin/doctor')?>" data-toggle="collapse" data-target="#receptionist">
                                <i class="fa fa-user-md"></i> Doctor 
                            </a>   
                        </li>
                        
                        
                        
                    <?php } ?>
                        
                     <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#pharmacist"> <i class="fa fa-unlock-alt"></i> Account <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="pharmacist" class="collapse">
                            <li>
                                <a href="<?php echo site_url('account/update_profile')?>"><i class="fa fa-edit"></i> Edit Info</a>
                            </li>
                            <li>
                                <a href="<?php echo site_url('account/change_password')?>"> <i class="fa fa-key"></i> Change password</a>
                            </li>
                        </ul>
                    </li>
              
           
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>
<!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
 
            <!-- /.navbar-collapse -->