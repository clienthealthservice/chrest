<?php $this->load->view('includes/backend/header')?>

    <div class="content-wrapper">
        <div class="content-header">
          <h1>
            <?php echo strtoupper($this->router->fetch_class()) ; ?> <small><?php echo $page_title ?></small>
          </h1>
          <ol class="breadcrumb">
            <li><a href= "../<?php echo $this->router->fetch_class() ; ?>"><i class="fa fa-dashboard"></i> <?php echo strtoupper($this->router->fetch_class()) ; ?></a></li>
            <li class="active"><?php echo $page_title ?></li>
          </ol>
        </div>
        <div class="content body">
            <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">                       
                        <div class="row">
                            <?php if (isset($message)){ ?>
                            <div class="col-lg-12">
                                
                            <div id="msg"><?php echo $message ?> </div>
                            
                            </div>
                            <?php } ?>
                            <?php if ($this->session->flashdata('message')){ ?>
                            <div class="col-lg-12">
                                
                            <div id="msg"><?php echo $this->session->flashdata('message') ?> </div>
                            
                            </div>
                            <?php } ?>
                             <div id="msg"> </div>
                        </div>
                        <ol>
                            
                        </ol>
                    </div>
                </div>
                <!-- /.row -->
                <?php echo validation_errors()?>
                <?php $this->load->view($main_view)?>
            
        </div>
            <!-- /.container-fluid -->
    </div>

<?php $this->load->view('includes/backend/footer')?>