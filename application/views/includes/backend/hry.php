<?php 
$ci =& get_instance();
$access_level= $ci->session->userdata('access_level');
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?php echo $page_title ?></title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo  base_url('assets/css/bootstrap.min.css')?>" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo base_url('assets/css/AdminLTE.css')?>" rel="stylesheet">
     <link href="<?php echo base_url('assets/css/skins/_all-skins.min.css')?>" rel="stylesheet">
     <link href="<?php echo base_url('assets/plugins/iCheck/flat/blue.css')?>" rel="stylesheet">
     <link href="<?php echo base_url('assets/css/hms.css')?>" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="<?php echo base_url('assets/plugins/morris/morris.css')?>" rel="stylesheet">
    
   
    <link href="<?php echo base_url('assets/plugins/jvectormap/jquery-jvectormap-1.2.2.css')?>" rel="stylesheet">
  
    <link href="<?php echo base_url('assets/plugins/datepicker/datepicker3.css')?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')?>" rel="stylesheet">
    <!--Font-->
    <link href="<?php echo base_url('assets/font-awesome/css/font-awesome.min.css')?>" rel="stylesheet" type="text/css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
   

</head>
    
<body class="skin-blue fixed" data-spy="scroll" data-target="#scrollspy">
    <div class="wrapper">
    <!-- Navigation -->
        <header class="main-header">
            <a href="" class="logo">
              <!-- LOGO -->
              Client Health Record System
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
              <!-- Navbar Right Menu -->
              <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                  <!-- Messages: style can be found in dropdown.less-->
                  <li class="dropdown messages-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                      <i class="fa fa-envelope-o"></i>
                      <span class="label label-success">4</span>
                    </a>
                    <ul class="dropdown-menu">
                      <li class="header">You have 4 messages</li>
                      <li>
                        <!-- inner menu: contains the actual data -->
                        <ul class="menu">
                          <li><!-- start message -->
                            <a href="#">
                              <div class="pull-left">
                                <img src="<?php echo base_url('assets/images/user2-160x160.jpg')?>" class="img-circle" alt="User Image">
                              </div>
                              <h4>
                                Sender Name
                                <small><i class="fa fa-clock-o"></i> 5 mins</small>
                              </h4>
                              <p>Message Excerpt</p>
                            </a>
                          </li><!-- end message -->
                          ...
                        </ul>
                      </li>
                      <li class="footer"><a href="#">See All Messages</a></li>
                    </ul>
                  </li>
                  <!-- Notifications: style can be found in dropdown.less -->
                  <li class="dropdown notifications-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                      <i class="fa fa-bell-o"></i>
                      <span class="label label-warning">10</span>
                    </a>
                    <ul class="dropdown-menu">
                      <li class="header">You have 10 notifications</li>
                      <li>
                        <!-- inner menu: contains the actual data -->
                        <ul class="menu">
                          <li>
                            <a href="#">
                              <i class="ion ion-ios-people info"></i> Notification title
                            </a>
                          </li>
                          ...
                        </ul>
                      </li>
                      <li class="footer"><a href="#">View all</a></li>
                    </ul>
                  </li>
                  <!-- Tasks: style can be found in dropdown.less -->
                  <li class="dropdown tasks-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                      <i class="fa fa-flag-o"></i>
                      <span class="label label-danger">9</span>
                    </a>
                    <ul class="dropdown-menu">
                      <li class="header">You have 9 tasks</li>
                      <li>
                        <!-- inner menu: contains the actual data -->
                        <ul class="menu">
                          <li><!-- Task item -->
                            <a href="#">
                              <h3>
                                Design some buttons
                                <small class="pull-right">20%</small>
                              </h3>
                              <div class="progress xs">
                                <div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                  <span class="sr-only">20% Complete</span>
                                </div>
                              </div>
                            </a>
                          </li><!-- end task item -->
                          ...
                        </ul>
                      </li>
                      <li class="footer">
                        <a href="#">View all tasks</a>
                      </li>
                    </ul>
                  </li>
                  <!-- User Account: style can be found in dropdown.less -->
                  <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                      <img src="<?php echo base_url('assets/images/user2-160x160.jpg')?>" class="user-image" alt="User Image">
                      <span class="hidden-xs">Alexander Pierce</span>
                    </a>
                    <ul class="dropdown-menu">
                      <!-- User image -->
                      <li class="user-header">
                        <img src="<?php echo base_url('assets/images/user2-160x160.jpg')?>" class="img-circle" alt="User Image">
                        <p>
                          Alexander Pierce - Web Developer
                          <small>Member since Nov. 2012</small>
                        </p>
                      </li>
                      <!-- Menu Body -->
                      <li class="user-body">
                        <div class="col-xs-4 text-center">
                          <a href="#">Followers</a>
                        </div>
                        <div class="col-xs-4 text-center">
                          <a href="#">Sales</a>
                        </div>
                        <div class="col-xs-4 text-center">
                          <a href="#">Friends</a>
                        </div>
                      </li>
                      <!-- Menu Footer-->
                      <li class="user-footer">
                        <div class="pull-left">
                          <a href="#" class="btn btn-default btn-flat">Profile</a>
                        </div>
                        <div class="pull-right">
                          <a href="<?php echo site_url('logout')?>" class="btn btn-default btn-flat">Sign out</a>
                        </div>
                      </li>
                    </ul>
                  </li>
                </ul>
              </div>
            </nav>
            
          </header>
    <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p>Alexander Pierce</p>
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>
          <!-- search form -->
          <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
              <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
            </div>
          </form>
          <!-- /.search form -->
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <li class="active treeview">
               <?php if($access_level==1){?>
                    <li>
                        <a href="<?php echo site_url($ci->router->fetch_class()."/dashboard");?>"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                    </li>
                    <?php } ?>
                    <?php if($access_level==6){?>
                    <li>
                        <a href="<?php echo site_url($ci->router->fetch_class());?>" data-toggle="collapse" data-target="#receptionist"><i class="fa fa-wheelchair"></i> Receptionist </a>
                        
                    </li>
                    <?php } ?>
                    
                    <?php if($access_level==4){?>
                    <li>
                        <a href="<?php echo site_url($ci->router->fetch_class());?>" data-toggle="collapse" data-target="#nurse"> <i class="fa fa-user-md"></i>  Nurse </a>
                        
                    </li>
                    <?php }?>
                    
                    <?php if($access_level==2){?>
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#doctor"><i class="fa fa-user-md"></i> Doctor</a>
                        
                    </li>
                    <?php } ?>
                    
                    <?php if($access_level==3){?>
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#pharmacist"> <i class="fa fa-medkit"></i> Pharmacist <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="pharmacist" class="collapse">
                            <li>
                                <a href="#">Dropdown Item</a>
                            </li>
                            <li>
                                <a href="#">Dropdown Item</a>
                            </li>
                        </ul>
                    </li>
                    <?php }?>
                    <?php if($access_level==5){?>
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#labTech">
                            <i class="fa fa-wrench"></i> Lab Technician <i class="fa fa-fw fa-caret-down"></i></a>
                        
                    </li>
                    <?php } ?>
                    
                    
                    
                    <?php if ($access_level==1){?>
                        <li>
                            <a href="<?php echo site_url('admin/patient')?>" data-toggle="collapse" data-target="#receptionist">
                                <i class="fa fa-wheelchair"></i> Patient 
                            </a>   
                        </li>
                        <li>
                            <a href="<?php echo site_url('admin/receptionist')?>" data-toggle="collapse" data-target="#receptionist">
                                <i class="fa fa-wheelchair"></i> Receptionist 
                            </a>   
                        </li>
                        
                        <li>
                            <a href="<?php echo site_url('admin/lab_technician')?>" data-toggle="collapse" data-target="#receptionist">
                                <i class="fa fa-wrench"></i> Lab Technician 
                            </a>   
                        </li>
                        <li>
                            <a href="<?php echo site_url('admin/nurse')?>" data-toggle="collapse" data-target="#receptionist">
                                <i class="fa fa-plus-square"></i> Nurse 
                            </a>   
                        </li>
                        <li>
                            <a href="<?php echo site_url('admin/pharmacist')?>" data-toggle="collapse" data-target="#receptionist">
                                <i class="fa fa-medkit"></i> Pharmacist 
                            </a>   
                        </li>
                        <li>
                            <a href="<?php echo site_url('admin/doctor')?>" data-toggle="collapse" data-target="#receptionist">
                                <i class="fa fa-user-md"></i> Doctor 
                            </a>   
                        </li>
                    <?php } ?>
              
           
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>
<!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
 
            <!-- /.navbar-collapse -->

    <div id="content-wrapper">
        <div class="content-header">
          <h1>
            AdminLTE Documentation
            <small>Current version 2.3.0</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Documentation</li>
          </ol>
        </div>
        <div class="content body">
            <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            <?php echo strtoupper($this->router->fetch_class()) ; ?> <small><?php echo $page_title ?></small>
                        </h1>
                        
                        <div class="row">
                            <?php if (isset($message)){ ?>
                            <div class="col-lg-12">
                                
                            <div id="msg"><?php echo $message ?> </div>
                            
                            </div>
                            <?php } ?>
                            <?php if ($this->session->flashdata('message')){ ?>
                            <div class="col-lg-12">
                                
                            <div id="msg"><?php echo $this->session->flashdata('message') ?> </div>
                            
                            </div>
                            <?php } ?>
                        </div>
                        <ol>
                            
                        </ol>
                    </div>
                </div>
                <!-- /.row -->

<?php $this->load->view($main_view)?>
            
        </div>
            <!-- /.container-fluid -->
    </div>
    
<!-- Footer-->
</div>
<!-- jQuery -->
    <script src="<?php echo base_url('assets/plugins/jQuery/jQuery-2.1.4.min.js')?>"></script>
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
      $.widget.bridge('uibutton', $.ui.button);
    </script>
    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url('assets/js/bootstrap.min.js')?>"></script>
    
    <script src="<?php echo base_url('assets/js/plugins/tinymce/tinymce.min.js')?>"> </script>
    <script type="text/javascript">
  tinymce.init({
    selector: 'textarea'
  });
  </script>
    
    <script src="<?php echo base_url('assets/plugins/sparkline/jquery.sparkline.min.js"')?>"> </script>
    <script src="<?php echo base_url('assets/js/plugins/datatables/datatables.min.js')?>"></script>
     <!-- jvectormap -->
    <script src="<?php echo base_url('assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')?>"></script>
    <script src="<?php echo base_url('assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')?>"></script>
    
    <script src="<?php echo base_url('assets/plugins/knob/jquery.knob.js')?>"></script>
    <!-- daterangepicker -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
    <script src="<?php echo base_url('assets/plugins/daterangepicker/daterangepicker.js')?>"></script>
    <!-- date picker -->
    <script src="<?php echo base_url('assets/plugins/datepicker/bootstrap-datepicker.js')?>"></script>
    
    
    <!-- date picker -->
    
    <script src="<?php echo base_url('assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')?>"></script>
    <!-- slimscroll -->
    <script src="<?php echo base_url('assets/plugins/slimScroll/jquery.slimscroll.min.js')?>"></script>
    
    <!-- Custom JS -->
    <script src="<?php echo base_url('assets/js/app.min.js')?>"></script>
    <script src="<?php echo base_url('assets/js/pages/dashboard.js')?>"></script>
    <script src="<?php // echo base_url('assets/js/plugins/morris/morris-data.js')?>"></script>
    <script src="<?php echo base_url('assets/js/demo.js')?>"></script>
    <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
    <script>
        //Remove patient function
        
        function remove_patient(pt_num){
             
            if (confirm('This action will delete patient')){
                    $.ajax({
                        url:"<?php echo site_url(); ?>" + "/receptionist/deactivate_pt/"+pt_num, 
                        success: function(result){
                        $("#msg").html('<div class="alert alert-info">Patient number with  number '+pt_num+' removed from records</div>');
                        $('#tr_'+pt_num).slideLeft(500);
                        },
                        error: function(error){
                          $('#msg').html("<div class='alert alert-info'>Sorry</div>");
                        }
                    });
            }
        }
    </script>
    <script>
        
        //DataTables function
$(document).ready(function() {
    var table = $('#chres_table').DataTable();
    
} );
</script>
<script>
    //fill the lab request model with the patient id on click
    $('a.lab_request').click(function() {
        $('#mdl_pt_num').val($(this).data('value'));
        
    });
</script>

</body>

</html>
