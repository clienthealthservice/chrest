<?php 
$ci =& get_instance();
$access_level= $ci->session->userdata('access_level');
$staff_id= $ci->session->userdata('staff_id');
$staff_name= $ci->session->userdata('staff_name');
$staff_role= $ci->session->userdata('staff_role');
$staff_image_thumb_name= Staff_m::get_staff('staff_image_thumb_name','pk_staff_id', $staff_id);
$staff_image_name= Staff_m::get_staff('staff_image_name','pk_staff_id', $staff_id);

$skin;
switch ($access_level) {
    case 1: $skin='skin-black';
        break;
    case 2: $skin='skin-blue';
        break;
    case 3: $skin='skin-green';
        break;
    case 4: $skin='skin-red';
        break;
    case 5: $skin='skin-yellow';
        break;
    case 6: $skin='skin-purple';
        break;

    default:
        break;
}


?>


<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?php echo $page_title ?></title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo  base_url('assets/css/bootstrap.min.css')?>" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo base_url('assets/css/AdminLTE.css')?>" rel="stylesheet">
     <link href="<?php echo base_url('assets/css/skins/_all-skins.min.css')?>" rel="stylesheet">
     <link href="<?php echo base_url('assets/plugins/iCheck/flat/blue.css')?>" rel="stylesheet">
     <link href="<?php echo base_url('assets/css/hms.css')?>" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="<?php echo base_url('assets/plugins/morris/morris.css')?>" rel="stylesheet">
    
    <!-- Datatable CSS -->
    
     <link href="<?php echo base_url('assets/css/plugins/datatable/dataTables.bootstrap.css')?>" rel="stylesheet">
     <link href="<?php echo base_url('assets/css/plugins/datatable/buttons.jqueryui.css')?>" rel="stylesheet">
    
     <!--Datepicker -->
      <link href="<?php echo base_url('assets/css/plugins/datepicker/datepicker.css')?>" rel="stylesheet">
      
      <link href="<?php echo base_url('assets/css/plugins/daterangepicker/daterangepicker-bs3.css')?>" rel="stylesheet">
     <!-- Bootstrap switch -->
      <link href="<?php echo base_url('assets/css/plugins/ui-checkbox/checkbox.css')?>" rel="stylesheet">
     
    
   
    <link href="<?php echo base_url('assets/plugins/jvectormap/jquery-jvectormap-1.2.2.css')?>" rel="stylesheet">
  
    <link href="<?php echo base_url('assets/plugins/datepicker/datepicker3.css')?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')?>" rel="stylesheet">
    <!--Font-->
    <link href="<?php echo base_url('assets/font-awesome/css/font-awesome.min.css')?>" rel="stylesheet" type="text/css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
   
<script src="<?php echo base_url('assets/plugins/jQuery/jQuery-2.1.4.min.js')?>"></script>
<script src="<?php echo base_url('assets/js/custom_functions.js')?>"></script>
</head>
 Client Health Record System (CHREST) 
<body class="<?php echo $skin ?> fixed" data-spy="scroll" data-target="#scrollspy">
    <div class="wrapper">
    <!-- Navigation -->
        <header class="main-header">
            <a href="" class="logo">
              <!-- LOGO -->
              Client Health Record System
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
              <!-- Navbar Right Menu -->
              <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                   <?php /* 
                  <!-- Messages: style can be found in dropdown.less-->
                  <li class="dropdown messages-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                      <i class="fa fa-envelope-o"></i>
                      <span class="label label-success">4</span>
                    </a>
                    <ul class="dropdown-menu">
                      <li class="header">You have 4 messages</li>
                      <li>
                        <!-- inner menu: contains the actual data -->
                        <ul class="menu">
                          <li><!-- start message -->
                            <a href="#">
                              <div class="pull-left">
                                <img src="<?php echo base_url('assets/images/user2-160x160.jpg')?>" class="img-circle" alt="User Image">
                              </div>
                              <h4>
                                Sender Name
                                <small><i class="fa fa-clock-o"></i> 5 mins</small>
                              </h4>
                              <p>Message Excerpt</p>
                            </a>
                          </li><!-- end message -->
                          ...
                        </ul>
                      </li>
                      <li class="footer"><a href="#">See All Messages</a></li>
                    </ul>
                  </li>
                  
                  <!-- Notifications: style can be found in dropdown.less -->
                  <li class="dropdown notifications-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                      <i class="fa fa-bell-o"></i>
                      <span class="label label-warning">10</span>
                    </a>
                    <ul class="dropdown-menu">
                      <li class="header">You have 10 notifications</li>
                      <li>
                        <!-- inner menu: contains the actual data -->
                        <ul class="menu">
                          <li>
                            <a href="#">
                              <i class="ion ion-ios-people info"></i> Notification title
                            </a>
                          </li>
                          ...
                        </ul>
                      </li>
                      <li class="footer"><a href="#">View all</a></li>
                    </ul>
                  </li>
                  <!-- Tasks: style can be found in dropdown.less -->
                  <li class="dropdown tasks-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                      <i class="fa fa-flag-o"></i>
                      <span class="label label-danger">9</span>
                    </a>
                    <ul class="dropdown-menu">
                      <li class="header">You have 9 tasks</li>
                      <li>
                        <!-- inner menu: contains the actual data -->
                        <ul class="menu">
                          <li><!-- Task item -->
                            <a href="#">
                              <h3>
                                Design some buttons
                                <small class="pull-right">20%</small>
                              </h3>
                              <div class="progress xs">
                                <div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                  <span class="sr-only">20% Complete</span>
                                </div>
                              </div>
                            </a>
                          </li><!-- end task item -->
                          ...
                        </ul>
                      </li>
                      <li class="footer">
                        <a href="#">View all tasks</a>
                      </li>
                    </ul>
                  </li>
                  <!-- User Account: style can be found in dropdown.less -->
                    */ ?>
                  <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                      <img src="<?php echo base_url('assets/uploads/images/'.$staff_image_thumb_name)  ?>" class="user-image" alt="User Image">
                      <span class="hidden-xs"><?php echo $staff_name?></span>
                    </a>
                    <ul class="dropdown-menu">
                      <!-- User image -->
                      <li class="user-header">
                        <img src="<?php echo base_url('assets/uploads/images/'.$staff_image_thumb_name)  ?>" class="img-circle" alt="User Image">
                        <p>
                          <?php echo $staff_name ?>
                            <small><i class="fa fa-black-tie"></i><?php echo ucwords($staff_role)?></small>
                        </p>
                      </li>
                      <!-- Menu Body -->
                      
                      <!-- Menu Footer-->
                      <li class="user-footer">
                        <div class="pull-left">
                          <a href="<?php echo site_url('account')?>" class="btn btn-default btn-flat">Account</a>
                        </div>
                        <div class="pull-right">
                          <a href="<?php echo site_url('logout')?>" class="btn btn-default btn-flat">Sign out</a>
                        </div>
                      </li>
                    </ul>
                  </li>
                </ul>
              </div>
              
            </nav>
            
          </header>
    <?php include 'sidenav.php' ?>
    