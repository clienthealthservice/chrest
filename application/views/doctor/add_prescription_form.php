<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php echo validation_errors(); 

//get the rescent lab report
$lab_entry= Lab_records_m::get_patient_last_reports_entry($patient_number);

?>

<div class="container">
<div class="row">
  <div class="col-md-6">
       <div class="panel-body">
           
           <div class="row">
               <div class="col-md-2">
                   <?php Patient_m::get($patient_number,'pt_first_name');?>
               </div>
           </div>
         <div class="form">
         <?php echo form_open("doctor/add_prescription/".$patient_number);?>
             
             <input name="attending_doctor_id" value="<?php echo $this->session->userdata('staff_id')?>"  hidden/>
             <div class="form-group">
                 <input value="<?php echo $patient_number ?>" name="patient_number"  hidden/>
                 <input name="last_report_number"  hidden/>
             </div>
             
             <div class="form-group">
         <?php echo form_label("Symptoms", "pt_symptoms");?>
         <?php echo form_textarea(array('type'=>'text_area','id' =>'chrestextarea','name'=>'symptoms','class'=>'form-control')); ?>
             </div>
             <hr>
             <div class="form-group">
         <?php echo form_label("Diagnosis", "diagnosis");?>
         <?php echo form_textarea(array('type'=>'text', 'name'=>'diagnosis','class'=>'form-control')); ?>
             </div>
             
             <hr>
             
             <div class="form-group">
         <?php echo form_label("Prescription", "prescription");
         echo form_textarea(array( 'name'=>'prescription','class'=>'form-control'));
            ?>
             </div>
             
             <hr>
             
             <div class="form-group">
         <?php echo form_label("Date", "date");?>
         <?php echo form_input(array('type'=>'date', 'name'=>'date','class'=>'form-control')); ?>
             </div>
             
             <div class="form-group">
         <?php echo form_label("Status", "pt_status");?>
         <?php echo form_dropdown('status',array('IP'=>'In-Patient', 'OPD'=>'Out-Patient','DC'=>'Discharged'),'admitted'); ?>
             </div>
             <div class="form-group"?>
         <?php echo form_input(array('type'=>'submit', 'name'=>'submit_prescriptions',"value"=>'Send','class'=>'btn btn-success btn-block'));
         ?>
             </div>
         <?php echo form_close();?>
         </div>
       </div>
    




</div>

    <div class="col col-sm-5">
        <h2>Last Reports Entries</h2>   
        <div class="row">
          <h3>Nurse</h3> 
          <ul class="list-group">
          <?php 
          $nurse_last_entry= Nurse_records::get_patient_last_entry($patient_number);
          if( is_array($nurse_last_entry)){
              foreach( $nurse_last_entry as $entry){?>
                <li class="list-group-item">Temperature <i class="label label-default"><?php echo $entry->temperature ?> <sup>0</sup> C </i></li>
                <li class="list-group-item">Weight <i class="label label-default"> <?php echo $entry->weight ?> KG </i></li>
                <li class="list-group-item"> Blood Pressure <i class="label label-default"> <?php echo $entry->blood_pressure ?> mm/Hg </i></li>

             <?php }
           } else{
               echo 'No nurse entry yet';
           }
?>
        </ul>
        </div>
        
        <div class="row">
            <h3>Lab</h3> 
            <div class="list-group">
            <?php
            
              if( is_array( $lab_entry )){
              foreach( $lab_entry as $entry){?>           
                <a href="#" class="list-group-item">
                  <h4 class="list-group-item-heading">Request Type</h4>
                  <p class="list-group-item-text"><?php echo $entry->report_type ?></p>
                </a>
                <a href="#" class="list-group-item">
                  <h4 class="list-group-item-heading">Description</h4>
                  <p class="list-group-item-text"><?php echo $entry->result_description ?></p>
                </a>
                
                <a href="#" class="list-group-item">
                  <h4 class="list-group-item-heading">Recommendations</h4>
                  <p class="list-group-item-text"><?php echo $entry->recommendations ?></p>
                </a>

              <?php }} else{
               echo 'No lab record yet';
           } ?>
            </div>
        </div>
    </div>
</div>
</div>
