

<script language="JavaScript"> 
        print_button("Print");
    </script>
<?php if(isset($record)){ $result=$record[0];}else{$result='Invalid record';} ?>

<div class="row">
    <div class="col-md-6"> 
        <p>Patient Name: <?php echo Patient_m::get_patient('pt_first_name', $result['fk_patient_number']).' '.Patient_m::get_patient('pt_last_name', $result['fk_patient_number'])   ?></p>
        <p>Sex: <?php echo Patient_m::get_patient('pt_gender', $result['fk_patient_number'])?></p>
        <p>Nationality: <?php echo Patient_m::get_patient('pt_nationality', $result['fk_patient_number'])?></p>
    </div>
    <div class="col-md-6">
        <div class=" pull-right">
            <p>Doctor Name: Dr.  <?php echo Staff_m::get_staff('staff_first_name', 'pk_staff_id', $result['fk_attending_doctor']).' '.Staff_m::get_staff('staff_last_name', 'pk_staff_id', $result['fk_attending_doctor']) ?></p>
            <p> Date: <?php echo date('D , m M Y', strtotime($result['date_of_attendance']))  ?></p>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <div class="list-group">
            <a class="list-group-item" >
              <h4 class="list-group-item-heading"><i class="fa fa-wheelchair"></i> Symptom</h4>
              <p class="list-group-item-text"> <?php  echo $result['symptoms'] ?></p>
            </a>
          </div>
    </div>
    
    <div class="col-md-4">
        <div class="list-group">
            <a class="list-group-item">
              <h4 class="list-group-item-heading"> <i class="fa fa-binoculars"></i> Diagnosis</h4>
              <p class="list-group-item-text"> <?php  echo $result['diagnosis'] ?></p>
            </a>
          </div>
    </div>

        <div class="col-md-4">
            <div class="list-group">
            <a class="list-group-item">
              <h4 class="list-group-item-heading"> <i class="fa fa-file-text"></i> Prescription</h4>
              <p class="list-group-item-text"> <?php  echo $result['prescription'] ?></p>
            </a>
          </div>
        </div>
    
    
</div>
    
    <div class="row">
        
        <div class="col col-md-4">
         <?php 
          $nurse_last_entry= Nurse_records::get_patient_last_entry($result['fk_patient_number']);
          if( is_array($nurse_last_entry)){
              foreach( $nurse_last_entry as $entry){?>
                <li class="list-group-item">Temperature <i class="label label-default"><?php echo $entry->temperature ?> <sup>0</sup> C </i></li>
                <li class="list-group-item">Weight <i class="label label-default"> <?php echo $entry->weight ?> KG </i></li>
                <li class="list-group-item"> Blood Pressure <i class="label label-default"> <?php echo $entry->blood_pressure ?> mm/Hg </i></li>

             <?php }
           } else{
               echo 'No nurse entry yet';
           }
?>
        </div>
        
      
        <div class="col col-sm-5">
            <div class="list-group">
            <?php
            $date=date('D M Y', strtotime($result['date_of_attendance']));
            $lab_entry= Lab_records_m::get_patient_report_by_date($result['fk_patient_number'],$date);
              if( is_array( $lab_entry )){
              foreach( $lab_entry as $entry){?>           
                <a href="#" class="list-group-item">
                  <h4 class="list-group-item-heading">Request Type</h4>
                  <p class="list-group-item-text"><?php echo $entry->report_type ?></p>
                </a>
                <a href="#" class="list-group-item">
                  <h4 class="list-group-item-heading">Description</h4>
                  <p class="list-group-item-text"><?php echo $entry->result_description ?></p>
                </a>
                
                <a href="#" class="list-group-item">
                  <h4 class="list-group-item-heading">Recommendations</h4>
                  <p class="list-group-item-text"><?php echo $entry->recommendations ?></p>
                </a>

              <?php }} else{
               echo 'No lab record yet';
           } ?>
            </div>
        
    </div>
    </div>



