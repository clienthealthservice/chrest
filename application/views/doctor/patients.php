<?php $ci =& get_instance();?>
<?php $patients=Patient_m::get_patients_general();?>

<table id="chres_table" class="table table-striped">
        <thead>
            <tr>
                <th>Name</th>
                <th>Patient Number</th>
                <th>Phone</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
<?php foreach($patients as $patient){?>
       <tr id="<?php echo $patient->pk_pt_id?>">
                  <td> <?php echo $patient->pt_first_name ." ". $patient->pt_last_name ?> </td>
                  <td><?php echo $patient->pt_number?> </td>
                  <td><?php  echo $patient->pt_mobile ?> </td>
                 <td>
                     <div class='btn-group btn-group-sm' role='group' >
                         <a  href='#rqt_lab'data-toggle="modal" data-number="<?php echo $patient->pt_number; ?> "data-name=" <?php echo $patient->pt_first_name ." ". $patient->pt_last_name ; ?> "data-target="#rqt_lab" class='btn btn-info lab_request'>Request Lab. Report</a>
                         <a href='<?php echo site_url($ci->router->fetch_class()."/records/".$patient->pt_number)?>' class='btn btn-warning'><i class="fa fa-clipboard"></i> View Records</a>
                         
                         <a href='<?php echo site_url('doctor/add_prescription/'.$patient->pt_number)?>' class='btn btn-danger'>Add Record</a>
                     </div>
                 </td>
        </tr>
<?php } ?>
        </tbody>
  </table>

<!-- Modal -->
            <div class="modal fade" id="rqt_lab" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Lab Request </h4>
              </div>
              <div class="modal-body">
                  
                 <?php echo form_open($this->router->fetch_class().'/request_lab/');?>
                    <input name="attending_doctor_id" value="<?php echo $this->session->userdata('staff_id')?>" hidden>
                    
                        Patients Name:  
                      <input id="mdl_pt_name" class="form-control" type="text" name="rqt_pt_name" readonly>
                      
                        Patients Number:  
                      <input id="mdl_pt_num" class="form-control" type="text" name="rqt_pt_number" readonly>
                      
                      Laboratory Test Type:  
                      <select id="report_type" class="form-control" name="rqt_lab_test_type">
                          <option> Select request type</option>
                           <option value="Blood Test" > Blood Test</option>
                           <option value="Urinal Test" > Urinal Test</option>
                           <option value="HIV Test" > HIV Test</option>
                           <option value="Hepatitis A Test" > Hepatitis A Test</option>
                           <option value="Hepatitis B Test" > Hepatitis B Test</option>
                           <option value="Hepatitis C Test" > Hepatitis C Test</option>
                           <option value="Liver function Test" > Liver function Test </option>
                       </select> 
                     <!-- <input  class="form-control" type="text" name="rqt_lab_test_type" > -->
               
                      <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                          <input  name="submit_lab_request"type="submit" value="Request" class="btn btn-primary">
                      </div>
              <?php echo form_close()?>
                  
                
              </div>
              
            </div>
            </div>
            </div>

