
<?php if(isset($records)&&  is_array($records)){ ?>
    <script language="JavaScript"> 
    print_button("Print records");
   </script>
   <div class="row">
<?php 
     foreach ($records as $record) { ?>
         <div class="col-sm-6 col-md-4">
            <div class="panel panel-info">
              <div class="panel-heading">
                  <i>REC#: <?php echo $record->record_number ?></i> <br>
                  
                  <i class="fa fa-calendar"></i> <?php echo date(" jS  F Y",strtotime($record->date_recorded));  ?> <br>
                  <i class="fa fa-clock-o"></i><?php echo date(" h:i:s A",strtotime($record->date_recorded));  ?>
                  
                </div>
                  <div class="panel-body">
                    <div class="r_prescription"><?php echo $record->prescription ?></div>
                    <?php 
                    $s_staff_name= $this->session->userdata('staff_name');
                    $db_staff_name= Staff_m::get_staff('staff_first_name','pk_staff_id', $record->fk_attending_doctor) ;
                        if ($s_staff_name == $db_staff_name){ ?>
                        <p><i class="fa fa-user-md">You generated this</i></p>
                            
                   <?php     }else{?>
                          <p><i class="fa fa-user-md"></i> Dr. <?php echo  Staff_m::get_staff('staff_first_name','pk_staff_id', $record->fk_attending_doctor) ?></p>  
                   <?php     }  ?>
                  </div>
                  <div class="panel-footer">
                  
                <p><a href="<?php echo site_url('doctor/record/'.$record->record_number)?>" class="btn btn-primary" role="button"><i class="fa fa-eye"></i> View Record</a> <a href="<?php echo site_url('doctor/delete/'.$record->record_number)?>" class="btn btn-danger"> <i class="fa fa-trash"></i> Delete</a></p>
              </div>
              
            </div>
          </div>
         
<?php          
     }
}  else {
    echo 'There is no record for this Patient as at a now.';
}
?>
</div>