<?php $pending_requests= Lab_request_m::get_pending_reports();?>
<div class="row">
    <div class="col-sm-3">
        <div class="info-box bg-aqua">
            <!-- Apply any bg-* class to to the icon to color it -->
            <span class="info-box-icon "><i class="fa fa-file-text-o" aria-hidden="true"></i></span>
            <div class="info-box-content">
              <span class="info-box-text">Total Requests</span>
              <span class="info-box-number"><?php echo Lab_records_m::count_total_lab_requests() ?></span>
            </div><!-- /.info-box-content -->
          </div><!-- /.info-box -->
    </div>
    <div class="col-sm-3">
        <div class="info-box bg-yellow">
            <!-- Apply any bg-* class to to the icon to color it -->
            <span class="info-box-icon"><i class="fa fa-file-word-o"></i></span>
            <div class="info-box-content">
              <span class="info-box-text">Request Queue</span>
              <span class="info-box-number"><?php echo Lab_records_m::count_pending_lab_requests() ?></span>
            </div><!-- /.info-box-content -->
          </div><!-- /.info-box -->
    </div>
    <div class="col-sm-3">
        <div class="info-box bg-green">
            <!-- Apply any bg-* class to to the icon to color it -->
            <span class="info-box-icon"><i class="fa fa-file-zip-o"></i></span>
            <div class="info-box-content">
              <span class="info-box-text">Completed</span>
              <span class="info-box-number"><?php echo Lab_records_m::count_completed_lab_requests() ?></span>
            </div><!-- /.info-box-content -->
          </div><!-- /.info-box -->
    </div>
    
   
</div>

 <section>
        <h3>Requests Queues </h3>
        <div class="row">
        <?php 
        if(is_array($pending_requests)){
            foreach ($pending_requests as $request){
            ?>
            <div id="request_<?php echo $request->request_id ?>" class="col-md-3">
              <div class="box box-solid">
                <div class="box-header with-border">
                  <i class="fa fa-paperclip"></i>
                  <h3 class="box-title"><?php echo $request->request_type  ?></h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <dl>
                      <dt><small>Patient Name: </small>  <?php echo $request->patient_name  ?></dt>
                      <dd><small>Patient Number: </small>
                         <?php echo $request->patient_number  ?>
                      </dd>
                      <dd><small>Type:</small> 
                          
                              <?php echo $request->request_type  ?>
                         
                      </dd>
                  </dl>
                </div><!-- /.box-body -->
                <div class="box-footer">
                    From: Dr. <?php echo $doctorName=Staff_m::get_staff_('staff_first_name', $request->requesting_staff_id).' '. Staff_m::get_staff_('staff_last_name', $request->requesting_staff_id)   ?> <br>
                    <a href="#generate_lab_report" 
                       data-toggle="modal" 
                       class="btn btn-info  btn-block generate_report"
                       data-target="#generate_lab_report"
                       data-request_id="<?php echo $request->request_id  ?>"
                       data-patient_name="<?php echo $request->patient_name  ?>"
                       data-patient_number="<?php echo $request->patient_number  ?>"
                       data-request_type="<?php echo $request->request_type ?>"
                       data-request_doctor_id="<?php echo $request->requesting_staff_id  ?>"
                       data-request_doctor_name="<?php echo $doctorName  ?>"
                    >
                        Generate Lab Report
                    </a> 
                </div>
              </div><!-- /.box -->
            </div>
            <?php } ?>
         <?php   }else{ ?>
             <div class="alert alert-info alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-info"></i> Info!</h4>
                No Lab requests found at the moment.
              </div>
         <?php } ?>
        </div>
    </section>

    
    <div class="modal fade" id="generate_lab_report" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Lab Request </h4>
              </div>
              <div class="modal-body">
                  
                  
        <?php echo form_open($this->router->fetch_class().'/save_report/',array('id'=>'lab_report_requested'));?>
                  <div id="form-inputs">
                  <input id="attending_technician_id" name="attending_technician_id" value="<?php echo $this->session->userdata('staff_id')?>" hidden>
                    Patients Name:  <input id="patient_name"   class="form-control"type="text" name="patient_name" readonly>
                    Patients Number:  <input id="patient_num"   class="form-control"type="text" name="patient_num" readonly>
                    <input type="text"  id="request_id" hidden="" >
                    
                  <div class="form-group">
                      Requesting Doctor 
                      <input id="requesting_doctor" type="text" name="requesting_doctor" hidden>
                       
                      <input type="text" id="requesting_doctor_name" class="form-control" name="requesting_doctor_name" readonly required>
                  </div>
                  <div class="form-group">
                       <label for="report_type"> Report Type:</label>
                     
                       <input id="report_type" class="form-control" type="text"  name="report_type" readonly required > 
                  </div>
                      
                  <div class="form-group">
                      
                  </div>    
                  <div class="form-group">
                      <label for="result_description">Result/Description</label>
                        
                      <textarea id="result_description" class="form-control" name="result_description">
                      </textarea>
                  </div>
                  <div class="form-group">
                      <label for="recommendation">Recommendation</label>
                      
                      <textarea id="recommendation" class="form-control" name="recommendation">
                      </textarea>
                  </div>
                </div>
                  <button id="save_report"  name="save_report" type="submit" class="btn btn-success btn-block">
                        Save 
                    </button>
                      
              <?php echo form_close()?>
                    </div>
                </div>
            </div>

          </div>
