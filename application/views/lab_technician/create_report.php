
<div class="row">
    <div class="col-md-6 col-md-offset-2">
        <?php echo form_open($this->router->fetch_class().'/save_report/');?>
                  <input name="attending_technician_id" value="<?php echo $this->session->userdata('staff_id')?>" hidden>
                 <?php if($patient_number){   ?> 
                  Patients Number:  <input   class="form-control"type="text" name="patient_num" value="<?php echo $patient_number?>" readonly>
                 <?php }else{?> 
                  Patients Number:  <input   class="form-control"type="text" name="patient_num">    
                 <?php }?>
                  <div class="form-group">
                      <label for="requesting_doctor">Requesting Doctor </label>
                      <select id="requesting_doctor" name="requesting_doctor" class="form-control">
                          
                      <option value="">Select a Doctor</option>
                      <?php $doctors= Staff_m::get_staffs(2) ;
                            if(is_array($doctors)){
                                foreach ($doctors as $doctor){?>
                          <option value="<?php echo $doctor->pk_staff_id?>">
                              <?php echo 'Dr.'. $doctor->staff_first_name.' '.$doctor->staff_last_name;?>
                          </option>
                      <?php
                                }
                            }
                              ?>
                      </select>
                  </div>
                  <div class="form-group">
                       <label for="report_type"> Report Type:</label>
                       
                       <input id="report_type" class="form-control" type="text"  name="report_type" > 
                  </div>
                      
                  <div class="form-group">
                      
                  </div>    
                  <div class="form-group">
                      <label for="result_description">Result/Description</label>
                        
                      <textarea class="form-control" name="result_description">
                      </textarea>
                  </div>
                  <div class="form-group">
                      <label for="recommendation">Recomendation</label>
                      
                      <textarea id="recommendation" class="form-control" name="recommendation">
                      </textarea>
                  </div>
                      
                  
                      <div class="">
                         
                          <input  name="save_report"type="submit" value="Save" class="btn btn-success btn-block">
                      </div>
              <?php echo form_close()?>
    </div>
</div>
                 
                  
                
         