<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE HTML>

<html>
	<head>
		<title>Client Health Record System</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
                <link rel="stylesheet" href="<?php echo base_url('assets/helios/assets/css/main.css')?>" />
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
	</head>
        <style>
            .brand_logo{
                border-radius: 180px;
            }
        </style>
	<body class="homepage">
		<div id="page-wrapper">

			<!-- Header -->
				<div id="header">

					<!-- Inner -->
						<div class="inner">
							<header>
                                                            <img class="brand_logo" width="200px"  height="200px"src="<?php echo base_url('assets/images/brand_logo.png')?>">
								<h1><a href="index.html" id="logo">Welcome to <br>Client Health Record System</a></h1>
								<hr />
								<p>Best health care</p>
							</header>
							<footer>
								<a href="<?php echo site_url('login')?>" class="button circled scrolly">login</a>
							</footer>
						</div>

					

				</div>

		</div>
		<!-- Scripts -->
			<script src="<?php echo base_url('assets/helios/assets/js/jquery.min.js')?>"></script>
			<script src="<?php echo base_url('assets/helios/assets/js/jquery.dropotron.min.js')?>"></script>
			<script src="<?php echo base_url('assets/helios/assets/js/jquery.scrolly.min.js')?>"></script>
			<script src="<?php echo base_url('assets/helios/assets/js/jquery.onvisible.min.js')?>"></script>
			<script src="<?php echo base_url('assets/helios/assets/js/skel.min.js')?>"></script>
			<script src="<?php echo base_url('assets/helios/assets/js/util.js')?>"></script>
			<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
			<script src="<?php echo base_url('assets/helios/assets/js/main.js')?>"></script>

	</body>
</html>