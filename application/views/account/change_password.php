<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$ci =& get_instance();
$class=$ci->router->fetch_class();
$staff_id= $ci->session->userdata('staff_id');
?>
<section>
        <h2>Change Password</h2>
            <div class="form">
            <?php echo form_open("account/change_password/");?>  

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">  
                    <?php echo form_label("Current Password <sup>*</sup>", "stf_current_password");?>
                    <?php echo form_input(array('type'=>'password', 'name'=>'stf_current_password','placeholder'=>'Current Password','class'=>'form-control')); ?>
                        </div>
                    </div>

                </div>
              
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">  
                    <?php echo form_label("New Password <sup>*</sup>", "stf_new_password");?>
                    <?php echo form_input(array('type'=>'password', 'name'=>'stf_new_password','placeholder'=>'New Password','class'=>'form-control')); ?>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">  
                    <?php echo form_label("New Password  Confirm<sup>*</sup>", "stf_new_passwordconf");?>
                    <?php echo form_input(array('type'=>'password', 'name'=>'stf_new_passwordconf','placeholder'=>'Confirm Password','class'=>'form-control')); ?>
                        </div>
                    </div>
                </div>
            
                
                <div class="form-group">
                    <?php 
                    echo form_input(array('type'=>'submit', 'name'=>'stf_change_password',"value"=>'Change','class'=>'btn btn-success btn-lg'));
                    ?>
                </div>
            <?php echo form_close();?>
        </div>
    </section>