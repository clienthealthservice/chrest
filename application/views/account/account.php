<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$ci =& get_instance();
$class=$ci->router->fetch_class();
$staff_id= $ci->session->userdata('staff_id');
?>

<?php echo validation_errors(); ?>
<h3> New Staff</h3>

<div>
    <section>
         <div class="form">
            <?php echo form_open_multipart($class."/account_update/");?>
        
        
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">  
            <?php echo form_label("First Name <sup>*</sup>", "stf_f_name");?>
            <?php echo form_input(array('type'=>'text', 'name'=>'stf_f_name','placeholder'=>'Firstname','class'=>'form-control','value'=> Staff_m::get_staff('staff_first_name', 'pk_staff_id',$staff_id) )); ?>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">  
            <?php echo form_label("Last name*", "stf_l_name");?>
            <?php echo form_input(array('type'=>'text', 'name'=>'stf_l_name','placeholder'=>'Lastname','class'=>'form-control','value'=> Staff_m::get_staff('staff_last_name', 'pk_staff_id',$staff_id))); ?>
                </div>
            </div>
        </div>
    

    
        <div class="row">
            <div class="col-sm-2">
                <div class="form-group">  
            <?php echo form_label("Username *", "stf_username");?>
            <?php echo form_input(array('type'=>'text', 'name'=>'stf_username','placeholder'=>'Username','class'=>'form-control','value'=> Staff_m::get_staff('staff_username', 'pk_staff_id',$staff_id),'required'=>'')); ?>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="form-group">  
            <?php echo form_label("Email *", "stf_email");?>
            <?php echo form_input(array('type'=>'email', 'name'=>'stf_email','required'=>'','placeholder'=>'Username','class'=>'form-control','value'=> Staff_m::get_staff('staff_email', 'pk_staff_id',$staff_id),'required'=>'')); ?>
                </div>
                
            </div>
            
            <div class="col-sm-2">
                <?php echo form_label("Gender *", "stf_gender"); 
                    $gender=Staff_m::get_staff('staff_gender', 'pk_staff_id',$staff_id)
                ?>
                

                <div class="row">
                    <?php echo form_radio(array('type'=>'radio', 'id'=>'stf_male','name'=>'stf_gender','value'=>'male'))." Male"; ?>
                </div>
                <div class="row">
                    <?php echo form_radio(array('type'=>'radio', 'name'=>'stf_female','name'=>'stf_gender','value'=>'female'))." Female"; ?>
            </div>
                    

               
            </div>
            <div class="col-sm-2">
                <?php echo form_label("Role <sup>*</sup>", "stf_role");?>
                <div class="form-group">
                    <?php
                     $options=array(
                         'value'=> Staff_m::get_staff_('staff_role',$staff_id),
                         'required'=>'',
                         'readonly'=>'',
                         'class'=>'form-control'
                     );   
                     $attributes="class='form-control' required";
                     echo form_input($options)?>
                 </div>
            </div>
            
        </div>
    
    

    
        <div class="row">
            <div class='col-sm-2'>
                <div class="form-group">  
            <?php echo form_label("Mobile Number*", "stf_mobile");?>
            <?php echo form_input(array('type'=>'text', 'name'=>'stf_mobile','placeholder'=>'Eg. 0267979001','class'=>'form-control','value'=> Staff_m::get_staff_('staff_phone_number',$staff_id))); ?>
                </div>
            </div>
            <div class='col-sm-4'>
                <div class="form-group">  
            <?php echo form_label("Image*", "stf_image");?>
                    <img class="img-thumbnail" src="<?php echo base_url('assets/uploads/images/'.Staff_m::get_staff_('staff_image_thumb_name',$staff_id))?>" height="100px" width="100px">
            <?php echo form_upload(array('type'=>'text', 'name'=>'stf_image','placeholder'=>'Eg. 0267979001','class'=>'form-control','value'=> set_value('stf_image',$this->input->post('stf_mobile')))); ?>
                </div>
            </div>
        </div>
    
    
    <div class="form-group">
        <?php 
        echo form_input(array('type'=>'submit', 'name'=>'stf_update_account',"value"=>'Save','class'=>'btn btn-success btn-lg'));
        ?>
    </div>
<?php echo form_close();?>
        
</div>
</section>
</div>
