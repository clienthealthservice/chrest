<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php echo validation_errors(); ?>
<style>
body{
  background-color: #222D32
}
#head{
    color: #D0D4D4;
}
</style>
<div class="container">
    <div id="head" class="row text-center" >
      <div class="col-md-4 col-md-offset-4" style="margin-top: 2% ">
          <a class=" brand logo" href="<?php echo site_url()?>">
              <img src="<?php echo base_url('assets/images/logo.png')?> " height="60">
          </a>
          <div class="row">
      <h2> Client Health Record System</h2>
      </div>
      </div>
      
  </div>
<div class="row">
  <div class="col-md-4 col-md-offset-4">
    <div class="login-panel panel panel-default">
        <div class="panel-heading text-center"><h3>Login</h3></div>
       <div class="panel-body">
         <div class="form">
         <?php echo form_open("login");?>
             <div class="form-group">
         <?php echo form_label("Username", "staff_username");?>
         <?php echo form_input(array('type'=>'text', 'name'=>'staff_username','placeholder'=>'Username','class'=>'form-control')); ?>
             </div>
             <div class="form-group">
         <?php echo form_label("Password", "user_password");
         echo form_input(array('type'=>'password', 'name'=>'staff_password','placeholder'=>'Password','class'=>'form-control'));
            ?>
             </div>
             <div class="form-group"?>
         <?php echo form_input(array('type'=>'submit', 'name'=>'staff_login',"value"=>'Send','class'=>'btn btn-success btn-block'));
         ?>
             </div>
         <?php echo form_close();?>
                 <?php //echo substr('andrew',0,1).substr('chamamme',0,1).random_string('numeric', 7);?>
                 <?php //echo sha1('woofer');?>
         </div>
       </div>
    </div>




</div>
    
</div>
    <div >
    <div class="row" >
        <div class="col col-md-4 col-md-offset-4 " style="background-color:#eee">
            <table id="chres_table" class="table">
                <thead>
                    <tr>
                        <th>Staff</th>
                        <th>Username</th>
                        <th>Password</th>
                        <th>Copy</th>
                      
                    </tr>
                </thead>
                <tbody>
                    <tr >
                                <td> Administrator </td>
                                <td>admin </td>
                                <td>admin </td>
                                <td> <button class="btn btn-info">Copy</button></td>
                    </tr>
                    <tr >
                                <td> Doctor  </td>
                                <td>doctor</td>
                                <td>doctor </td>
                                <td><button class="btn btn-info">Copy</button> </td>
                    </tr>
                    <tr >
                                <td> Pharmacist  </td>
                                <td>pharmacist</td>
                                <td>pharmacist </td>
                               <td> <button class="btn btn-info">Copy</button> </td>
                    </tr>
                    <tr data-name="technician">
                                <td> Laboratory Technician  </td>
                                <td>labtech</td>
                                <td>labtech </td>
                                <td> <button class="btn btn-info">Copy</button> </td>
                    </tr>
                    <tr >
                                <td> Nurse  </td>
                                <td>nurse</td>
                                <td>nurse </td>
                               <td> <button class="btn btn-info">Copy</button> </td>
                    </tr>
                    <tr>
                                <td> Receptionist  </td>
                                <td>recept1</td>
                                <td>receptionist </td>
                               <td> <button class="btn btn-info">Copy</button> </td>
                    </tr>
                </tbody>
            </table>
            
        </div>
    </div>
    </div>
</div>
