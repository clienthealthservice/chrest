<?php if(is_array($patients)){ ?>
    <table id="chres_table" class="table table-striped">
        <thead>
            <tr>
                <th>Name</th>
                <th>NHIS Status</th>
                
                <th>Patient Number</th>
                <th>Gender</th>
                <th>Phone</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
<?php foreach ($patients as $patient){ ?>
        <tr id="<?php echo $patient->pk_pt_id?>">
                  <td> <?php echo $patient->pt_first_name ." ". $patient->pt_last_name ?> </td>
                  <td><?php 
                    if(!empty($patient->next_renewal_date)&&$patient->next_renewal_date > date("Y-m-d")){ echo '<span class="label label-pill label-success">Active </span>'; }
                    if(!empty($patient->next_renewal_date)&&$patient->next_renewal_date <= date("Y-m-d")){ echo '<span class="label label-pill label-danger">Expired </span>';; }
                    if(empty($patient->next_renewal_date)){ echo '<span class="label label-pill label-warning"> Not Set </span>';; }
                    ?> </td>
                  <td><?php echo $patient->pt_number?> </td>
                  <td><?php echo $patient->pt_gender?> </td>
                  <td><?php  echo $patient->pt_mobile ?> </td>
                 <td>
                     <div class='btn-group btn-group-sm' role='group' >
                         <a href='<?php echo site_url($this->router->fetch_class()."/prescriptions/".$patient->pt_number)?>' class='btn btn-info'><i class="fa fa-file-text"> </i> Prescriptions</a>
                     </div>
                 </td>
        </tr>
 <?php   }
}
?>
    </tbody>
  </table>