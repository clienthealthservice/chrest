
<h1 id="pending_h1">Pending Precriptions</h1> 
    <script language="JavaScript"> 
        print_button("Print Prescription(s)");
    </script>
<div class="row">
    

<?php
//get the prescription variable from the controller
/**
 * Check if prescription is a an array or a string.
 */
if (is_array($pending_prescriptions)){//its an array so go ahead and output each
  
    foreach ($pending_prescriptions as $prescription ){?>

            <div class="col-lg-4 col-md-6">
                <div class="panel panel-danger">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-md-6">
                                <i class="fa fa-wheelchair"> </i> <?php echo $prescription->pt_first_name.' '. $prescription->pt_last_name;?>
                            </div>
                            
                            <div class="col-md-6">
                                <i class="fa fa-globe"> </i> <?php echo $prescription->pt_nationality;?>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-6">
                                <i class="fa fa-calendar"></i> <?php echo date(" jS  F Y",strtotime($prescription->date_recorded));  ?> <br>
                                 <i class="fa fa-clock-o"></i><?php echo date(" h:i:s A",strtotime($prescription->date_recorded));  ?>
                            </div>
                            
                        </div>
                    </div>
                    <div class="panel-body">
                        <h4>Prescription</h4>
                        <p ><?php echo $prescription->prescription;?></p>
                        
                        <small><span class="label label-info"> Prescribed by </span> Dr. <?php echo $prescription->staff_first_name .' '. $prescription->staff_last_name ;?></small>
                    </div>
                    
                        <div class="panel-footer">
                            <button class="btn btn-success pull-left"  data-toggle="modal" data-target="#myModal<?php echo $prescription->record_number?>"><i class="fa fa-medkit"></i> Issue</button>
                            
                            
                            <div class="clearfix"></div>
                        </div>
                   
                </div>
            </div>

                    <!-- Modal -->
            <div class="modal fade" id="myModal<?php echo $prescription->record_number ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Issue Medication <?php echo $prescription->pt_first_name.' '. $prescription->pt_last_name;?></h4>
              </div>
              <div class="modal-body">
                  
                 <?php echo form_open($this->router->fetch_class().'/issue/');?>
                  <input name="attending_doctor_id" value="<?php echo $prescription->pk_staff_id?>" hidden>
                      Patients Number:  <input  class="form-control"type="text" name="patient_num" value="<?php echo $prescription->pt_number?>" readonly>
                      Doctors Record Number:  <input  class="form-control"type="text" name="dct_record_num" value="<?php echo $prescription->record_number?>" readonly>
                      Lab Record Number:  <input  class="form-control"type="text" name="dct_record_number" value="<?php echo $prescription->record_number?>" readonly >
                      
                      Prescription:  
                      <div class="panel panel-default">
                          <textarea  name="prescription" style="width:100%" readonly>
                          <?php echo html_entity_decode( $prescription->prescription)?>
                          </textarea>
                      </div>
                      
                      Description:  
                      <textarea class="form-control" name="description">
                      </textarea>
                  
                      <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                          <input  name="submit_issue"type="submit" value="Save" class="btn btn-primary">
                      </div>
              <?php echo form_close()?>
                  
                
              </div>
              
            </div>
            </div>
            </div>
            <?php 
        
    }
}else{//its not an array which mean that apparently there was no record found for this patient
    echo 'No prescription found';
}
?>
</div>
 
<hr>

<div id="issued_presc">
    
<h1>Issued Precriptions</h1> 
<div class="row">
<?php
if (is_array($issued_prescriptions)){//its an array so go ahead and output each
  
    foreach ($issued_prescriptions as $prescription ){?>

            <div class="col-lg-4 col-md-6">
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-md-6">
                                <i class="fa fa-wheelchair"> </i> <?php echo $prescription->pt_first_name.' '. $prescription->pt_last_name;?>
                            </div>
                            
                            <div class="col-md-6">
                                <i class="fa fa-globe"> </i> <?php echo $prescription->pt_nationality;?>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-6">
                                <i class="fa fa-calendar"></i> <?php echo date(" jS  F Y",strtotime($prescription->date_recorded));  ?> <br>
                                 <i class="fa fa-clock-o"></i><?php echo date(" h:i:s A",strtotime($prescription->date_recorded));  ?>
                            </div>
                            
                        </div>
                    </div>
                    <div class="panel-body">
                        
                        <h4>Prescription</h4>
                        <p ><?php echo $prescription->prescription;?></p>
                        
                        <small>span class="label label-info"> Prescribed by </span>  Dr. <?php echo $prescription->staff_first_name .' '. $prescription->staff_last_name ;?></small>
                    </div>
                    
                        <div class="panel-footer">
                            
                            <div class="clearfix"></div>
                        </div>
                   
                </div>
            </div>
            <?php 
        
    }
}
?>
</div>
</div>
