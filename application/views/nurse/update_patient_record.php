    
      <div class="row">
          <div class="col-md-4">   
             <div>
                 <?php echo validation_errors(); ?>
                     <div class="well">
                         
                        <form action="<?php echo site_url('nurse/save/')?>" method="post">
                           
                            <input name="pt_number" value="<?php echo $pt_number?>" hidden/>
                            <div class="form-group">
                                <label for="pt_weight">Patient Weight</label>
                                <div class="input-group">
                                    <span class="input-group-addon" id="basic-addon2"><i class="fa fa-child"></i> </span>
                                    <input type="text" class="form-control" name="pt_weight" placeholder="eg.10" required>
                                    <span class="input-group-addon" id="basic-addon2"> Kg </span>
                                </div>

                                <br>
                                
                                <label for="pt_temperature">Patient Temperature</label>
                                <div class="input-group">
                                    
                                    <span class="input-group-addon" id="basic-addon2"><i class="fa fa-fire"></i> </span>
                                    <input type="text" class="form-control" name="pt_temperature" placeholder="eg.10" required>
                                    <span class="input-group-addon" id="basic-addon2"><sup>o</sup>C</span>
                                </div>
                                <br>
                                <label for="pt_temperature">Patient Blood Pressure</label>
                                <div class="input-group">
                                    <span class="input-group-addon" id="basic-addon2"><i class="fa fa-heart"></i> </span>
                                    <input type="text" class="form-control" name="pt_blood_pressure" placeholder="eg.10" required>
                                    
                                    <span class="input-group-addon" id="basic-addon2">mm Hg</span>
                                </div>
                            </div>
                            <input name="save_record" class="btn btn-success btn-block" value="Save" type="submit">
                        </form>


                    </div>
                        
                    </div>
                  </div>
          
          <div class="col-md-8">
              <h4>Patient's Previous Records</h4>
              
              <?php if(isset($patient_previous_records)){
                 $ci =& get_instance();?>
                <table class="table table-bordered">
                      <thead>
                          <tr>
                              <th>Weight (KG)</th>
                              <th>Temperature (<sup>o</sup>C)</th>
                              <th>Blood Pressure (mm HG)</th>
                              <th>Date</th>
                          </tr>
                      </thead>
                      <tbody>
              <?php foreach($patient_previous_records as $record){?>
                            <tr>
                                <td> <?php echo $record->weight?> </td>
                                <td> <?php  echo $record->temperature?> </td>
                                <td><?php  echo $record->blood_pressure ?> </td>
                                <td> <?php echo date(" D M m Y",strtotime($record->date_created)) ;  ?> </td>
                            </tr>
              <?php } ?>
                      </tbody>
                </table>
              <?php }else{
                  echo "No previous record found for this patient";
                } 
              ?>
          </div>
                
              
      </div>