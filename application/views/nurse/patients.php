<?php $ci =& get_instance();?>
<table id="chres_table" class="table table-striped">
        <thead>
            <tr>
                <th>Name</th>
                <th>Gender</th>
                <th>Patient Number</th>
                <th>Phone</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
<?php foreach($patients as $patient){?>
       <tr id="<?php echo $patient->pk_pt_id?>">
                  <td> <?php echo $patient->pt_first_name ." ". $patient->pt_last_name ?> </td>
                  <td><?php echo $patient->pt_gender?> </td>
                  <td><?php echo $patient->pt_number?> </td>
                  <td><?php  echo $patient->pt_mobile ?> </td>
                 <td>
                     <div class='btn-group btn-group-sm' role='group' ><a href='<?php echo site_url($ci->router->fetch_class()."/patients/".$patient->pt_number)?>' class='btn btn-info'>Update Record</a></div>
                 </td>
        </tr>
<?php } ?>
        </tbody>
  </table>