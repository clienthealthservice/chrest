



<div class="panel panel-default">
  <div class="panel-body">
      <div class="row">
          <div class="col-sm-4">
             <a class="btn btn-info btn-lg btn-block" role="button" data-toggle="collapse" href="#temp" aria-expanded="false" aria-controls="collapseExample">
                 <i class="fa fa-wheelchair"></i> Add Temperature
            </a>
              
              
              <div class="collapse" id="temp">
                  <div class="panel panel-default">
                    <div class="panel-body">
                     <div class="well">
                        <form action="<?php echo site_url('nurse/add_temperature')?>" method="post">
                            <div class="form-group">
                                <label for="pt_number">Patient Number:</label>
                                <input type="text" class="form-control" name="pt_number" placeholder="eg. A11223344" required>

                                <br>
                                
                                <label for="pt_temperature">Patient Temperature</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" name="pt_temperature" placeholder="eg.10" required>
                                    <span class="input-group-addon" id="basic-addon2"><sup>0</sup>C</span>
                                </div>
                            </div>
                            <button class="btn btn-success btn-block"  type="submit"> Saves </button>
                        </form>


                    </div>
                        
                    </div>
                  </div>
                
              </div>
          </div>
          <div class="col-sm-4">
                <a class="btn btn-primary btn-lg btn-block" role="button" data-toggle="collapse" href="#add_weight" aria-expanded="false" aria-controls="add_weight">
                   <i class="fa fa-child"></i> Pateint Weight
                </a>
              
              
              <div class="collapse" id="add_weight">
                  <div class="panel panel-default">
                    <div class="panel-body">
                     <div class="well">
                        <form action="<?php echo site_url('nurse/add_weight')?>" method="post">
                            <div class="form-group">
                                <label for="pt_number">Patient Number:</label>
                                <input type="text" class="form-control" name="pt_number" placeholder="eg. A11223344" required>

                                <br>
                                <label for="pt_weight">Patient Weight</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" name="pt_weight" placeholder="eg.10" required>
                                    <span class="input-group-addon" id="basic-addon2">KG</span>
                                </div>
                            </div>
                            <button class="btn btn-success btn-block"  type="submit"> Saves </button>
                        </form>


                    </div>
                        
                    </div>
                  </div>
                
              </div>          
          </div>
          <div class="col-sm-4">
              <a class="btn btn-danger btn-lg btn-block" role="button" data-toggle="collapse" href="#add_bp" aria-expanded="false" aria-controls="collapseExample">
                   <i class="fa fa-heartbeat"></i> Pateint B.P
                </a>
              
              
              <div class="collapse" id="add_bp">
                  <div class="panel panel-default">
                    <div class="panel-body">
                     <div class="well">
                        <form action="<?php echo site_url('nurse/add_blood_pressure')?>" method="post">
                            <div class="form-group">
                                <label for="pt_number">Patient Number:</label>
                                <input type="text" class="form-control" name="pt_number" placeholder="eg. A11223344" required>

                                <br>
                                <label for="pt_pressure">B.P</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" name="pt_pressure" placeholder="eg.120/80" required>
                                    <span class="input-group-addon" id="basic-addon2">mm Hg</span>
                                </div>
                            </div>
                            <button class="btn btn-success btn-block"  type="submit"> Save</button>
                        </form>


                    </div>
                        
                    </div>
                  </div>
                
              </div> 
          </div>
      </div>
   
   
   
 </div>
</div>


<!-- Modal -->
<div class="modal fade" id="editPatient" tabindex="-1" role="dialog" aria-labelledby="editPatientLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Edit Patient Info</h4>
      </div>
      <div class="modal-body">
        <form action="">
            <label for="patient_number">Patient Number</label>
            <input type="text" class="form-control" placeholder="Patient number">
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>