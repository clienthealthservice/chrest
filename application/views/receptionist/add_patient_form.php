<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$ci =& get_instance();
$class=$ci->router->fetch_class();
?>
<?php echo validation_errors(); ?>
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        
<h3 class="text-center">New Patient</h3>
<div>
    <div class="form">
<?php echo form_open_multipart($class."/add_patient");?>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">  
            <?php echo form_label("Patient First Name*", "pt_f_name");?>
            <?php echo form_input(array('type'=>'text', 'name'=>'pt_f_name','placeholder'=>'Firstname','class'=>'form-control','value'=> set_value('pt_f_name',$this->input->post('pt_f_name')))); ?>
                </div>
            </div>
              
            <div class="col-md-4">
                <div class="form-group">  
            <?php echo form_label("Patient Middle Name", "pt_m_name");?>
            <?php echo form_input(array('type'=>'text', 'name'=>'pt_m_name','placeholder'=>'Middle name','class'=>'form-control','value'=> set_value('pt_m_name',$this->input->post('pt_m_name')))); ?>
                </div>
            </div>
            
            <div class="col-md-4">
                <div class="form-group">  
            <?php echo form_label("Patient last name*", "pt_l_name");?>
            <?php echo form_input(array('type'=>'text', 'name'=>'pt_l_name','placeholder'=>'Lastname','class'=>'form-control','value'=> set_value('pt_l_name',$this->input->post('pt_l_name')))); ?>
                </div>
            </div>
            </div>


    

    
  

    <div class="form-group">  
        <div class="row">
            <div class="col-lg-2">
                <?php echo form_label("Gender *", "pt_gender");?>
            </div>
            <div class="col-lg-2">
                <div class="row">
                    <?php echo form_radio(array('type'=>'radio', 'id'=>'pt_male','name'=>'pt_gender','value'=>'male'))." Male"; ?>
                </div>
                <div class="row">
                    <?php echo form_radio(array('type'=>'radio', 'name'=>'pt_female','name'=>'pt_gender','value'=>'female'))." Female"; ?>
                </div>
            </div>
            
        </div>
    </div>

        
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">  
            <?php echo form_label("Occupation", "pt_occupation");?>
            <?php echo form_input(array('type'=>'text', 'name'=>'pt_occupation','placeholder'=>'Occupation','class'=>'form-control','value'=> set_value('pt_occupation',$this->input->post('pt_occupation')))); ?>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">  
            <?php echo form_label("Address", "pt_l_name");?>
            <?php echo form_input(array('type'=>'text', 'name'=>'pt_address','placeholder'=>'Address','class'=>'form-control','value'=> set_value('pt_address',$this->input->post('pt_address')))); ?>
                </div>
            </div>
        </div>   
    
    

    
    <div class="row">
        <div class="col-md-4">
            <div class="form-group">  
        <?php echo form_label("Date of birth *", "pt_date_of_birth");?>
        <?php echo form_input(array('type'=>'date', 'name'=>'pt_date_of_birth','class'=>'form-control','value'=> set_value('pt_date_of_birth',$this->input->post('pt_date_of_birth')))); ?>
            </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">  
        <?php echo form_label("Place of Birth*", "pt_place_of_birth");?>
        <?php echo form_input(array('type'=>'text', 'name'=>'pt_place_of_birth','placeholder'=>'place of birth','class'=>'form-control','value'=> set_value('pt_place_of_birth',$this->input->post('pt_place_of_birth')))); ?>
          </div>  
        </div>
        <div class="col-md-4">
            <div class="form-group">  
        <?php echo form_label("Place of Residence*", "pt_place_of_residence");?>
        <?php echo form_input(array('type'=>'text', 'name'=>'pt_place_of_residence','placeholder'=>'Eg. New Tafo','class'=>'form-control','value'=> set_value('pt_place_of_residence',$this->input->post('pt_place_of_residence')))); ?>
            </div>
        </div>
        
        <div class="form-group">  
            <?php echo form_label(" Image *", "pt_image");?>
            <?php echo form_upload(array('type'=>'text', 'name'=>'pt_image','value'=> set_value('pt_image',$this->input->post('pt_image')))); ?>
        </div>
     
    </div>

    

    
        
        <div class="row">
            <div class="col-md-4">
                <div class="form-group"> 
                <?php echo form_label("Nationality*", "pt_nationality");?>
                <?php echo form_input(array('type'=>'text', 'name'=>'pt_nationality','placeholder'=>'Eg. Ghanaian','class'=>'form-control','value'=> set_value('pt_nationality',$this->input->post('pt_nationality')))); ?>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">  
            <?php echo form_label("Mobile Number*", "pt_mobile");?>
            <?php echo form_input(array('type'=>'text', 'name'=>'pt_mobile','placeholder'=>'Eg. 0267979001','class'=>'form-control','value'=> set_value('pt_mobile',$this->input->post('pt_mobile')))); ?>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">  
            <?php echo form_label("First Attendance*", "pt_first_attendance");?>
            <?php echo form_input(array('type'=>'date', 'name'=>'pt_first_attendance','class'=>'form-control','value'=> set_value('pt_occupation',$this->input->post('pt_fisrt_attendance')))); ?>
                </div>
            </div>
        </div>
        
        <!--NHIS fields starts here -->
        <div class="row">
            <h4>Health Insurance</h4>
            <div class="col-md-12">
                <div class="form-group">  
                    <div class="ui toggle checkbox">
                        <?php echo form_checkbox(array('id'=>'nhis','name'=>'nhis','class'=>'' ,'value'=>'true','data-on-text'=>'Available','data-off-text'=>'Unavailable')); ?>
                        <?php echo form_label("Is NHIS available ?", "nhis");?>    
                    </div>
                </div>  
            </div>
        </div>
            <div class="row">    
            <div id="nhis_details" style="display:none">
                <div class="col-md-4">
                    <div class="form-group">  
                <?php echo form_label("Membership Number *", "nhis_membership_number");?>
                <?php echo form_input(array('type'=>'text', 'name'=>'nhis_membership_number','placeholder'=>'Eg. 21755196','class'=>'form-control','value'=> set_value('nhis_membership_number',$this->input->post('nhis_membership_number')))); ?>
                    </div>
                </div>
            
                <div class="col-md-4">
                    <div class="form-group">  
                <?php echo form_label("Next Renewal *", "nhis_next_renewal");?>
                <?php echo form_input(array('type'=>'date', 'name'=>'nhis_next_renewal','placeholder'=>'Eg. 02/03/2003','class'=>'form-control','value'=> set_value('nhis_next_renewal',$this->input->post('nhis_next_renewal')))); ?>
                    </div>
                </div>
            <div class="col-md-4">
                <div class="form-group">  
            <?php echo form_label("Expiry Date *", "nhis_expiry_date");?>
            <?php echo form_input(array('type'=>'date', 'name'=>'nhis_expiry_date','placeholder'=>'Eg. 02/03/2009','class'=>'form-control','value'=> set_value('nhis_expiry_date',$this->input->post('nhis_expiry_date')))); ?>
                </div>
            </div>
            </div>
        </div>
    <!--NHIS fields end here -->
        <section class="section">
            <h3>Next Of Kin</h3>
            
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">  
                <?php echo form_label("FirstName *", "pt_nok_f_name");?>
                <?php echo form_input(array('type'=>'text', 'name'=>'pt_nok_f_name','class'=>'form-control','value'=> set_value('pt_nok_f_name',$this->input->post('pt_nok_f_name')))); ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">  
                <?php echo form_label("Lastname *", "pt_nok_l_name");?>
                <?php echo form_input(array('type'=>'text', 'name'=>'pt_nok_l_name','class'=>'form-control','value'=> set_value('pt_nok_l_name',$this->input->post('pt_nok_l_name')))); ?>
                    </div>
                </div>
                    
             </div>
                    
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">  
                <?php echo form_label("Date of Birth *", "pt_nok_date_of_birth");?>
                <?php echo form_input(array('type'=>'date', 'name'=>'pt_nok_date_of_birth','class'=>'form-control','value'=> set_value('pt_nok_date_of_birth',$this->input->post('pt_nok_date_of_birth')))); ?>
                    </div> 
                </div>
                <div class="col-md-4">
                    <div class="form-group">  
                <?php echo form_label(" Occupation *", "pt_nok_occupation");?>
                <?php echo form_input(array('type'=>'text', 'name'=>'pt_nok_occupation','class'=>'form-control','value'=> set_value('pt_nok_occupation',$this->input->post('pt_nok_occupation')))); ?>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">  
                <?php echo form_label(" Nationality *", "pt_nok_occupation");?>
                <?php echo form_input(array('type'=>'text', 'name'=>'pt_nok_nationality','class'=>'form-control','value'=> set_value('pt_nok_nationality',$this->input->post('pt_nok_nationality')))); ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">  
                <?php echo form_label("Address ", "pt_nok_address");?>
                <?php echo form_input(array('type'=>'address', 'name'=>'pt_nok_address','class'=>'form-control','value'=> set_value('pt_nok_address',$this->input->post('pt_nok_address')))); ?>
                    </div> 
                </div>
                <div class="col-md-4">
                    <div class="form-group">  
                <?php echo form_label(" Residence *", "pt_nok_residence");?>
                <?php echo form_input(array('type'=>'text', 'name'=>'pt_nok_residence','class'=>'form-control','value'=> set_value('pt_nok_residence',$this->input->post('pt_nok_residence')))); ?>
                    </div>
                </div>
                <div class="col-md-4">
                    
                </div>
                <div class="col-md-4">
                    <div class="form-group">  
                <?php echo form_label(" Phone *", "pt_nok_phone");?>
                <?php echo form_input(array('type'=>'text', 'name'=>'pt_nok_phone','class'=>'form-control','value'=> set_value('pt_nok_phone',$this->input->post('pt_nok_phone')))); ?>
                    </div>

                </div>
                    
                </div>
            </div>
                    

                    

                    
                    
                </div>
           
            
            
        </section>
    
    <div class="form-group"?>
<?php 
echo form_input(array('type'=>'submit', 'name'=>'pt_submit',"value"=>'Add','class'=>'btn btn-success btn-lg'));
?>
    </div>
<?php echo form_close();?>
        
</div>
</div>
    </div>
</div>
