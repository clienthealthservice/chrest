<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$ci =& get_instance();
$class=$ci->router->fetch_class();
?>

<?php echo validation_errors(); ?>
<h3 class="text-center">Patient Info </h3>
<div>
    <div class="form" style="margin: 5px">
        <div class="user-header">
            <img src="<?php echo base_url('assets/uploads/images/'.Patient_m::get_image_url($patient_number))?>" class="img-responsive" alt="User Image" width="100px" height="100px">
        </div>
<?php echo form_open($class."/edit_patient/".$patient_number);?>
        <div class="row">
            <div class="col-sm-4">
                <div class="form-group">  
            <?php echo form_label("Patient First Name*", "pt_f_name");?>
            <?php echo form_input(array('type'=>'text', 'name'=>'pt_f_name','placeholder'=>'Firstname','class'=>'form-control','value'=>  Patient_m::get($patient_number, 'pt_first_name'))); ?>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">  
            <?php echo form_label("Patient Middle Name", "pt_m_name");?>
            <?php echo form_input(array('type'=>'text', 'name'=>'pt_m_name','placeholder'=>'Middle name','class'=>'form-control','value'=>  Patient_m::get($patient_number, 'pt_middle_name'))); ?>
                </div>
            </div>
            
            <div class="col-sm-4">
                <div class="form-group">  
            <?php echo form_label("Patient last name*", "pt_l_name");?>
            <?php echo form_input(array('type'=>'text', 'name'=>'pt_l_name','placeholder'=>'Lastname','class'=>'form-control','value'=>  Patient_m::get($patient_number, 'pt_last_name'))); ?>
                </div>
            </div>
        </div>
    

    <div class="form-group">  
        <div class="row">
            <div class="col-lg-2">
                <?php echo form_label("Patient Gender *", "pt_gender");?>
            </div>
            <div class="col-lg-2">
                <?php $gender=Patient_m::get($patient_number, 'pt_gender');?>
                <div class="row">
                    <input type="radio" value="male" name='pt_gender' <?php if($gender=='male'){echo 'checked=checked';}?>> Male
                </div>
                <div class="row">
                    <input type="radio"  value="female" name='pt_gender' <?php if($gender=='female'){echo 'checked=checked';}?>> Female
                </div>
            </div>
            
        </div>
    </div>

    <div class="form-group">  
<?php echo form_label("Occupation", "pt_occupation");?>
<?php echo form_input(array('type'=>'text', 'name'=>'pt_occupation','placeholder'=>'Occupation','class'=>'form-control','value'=>  Patient_m::get($patient_number, 'pt_occupation'))); ?>
    </div>
    <div class="form-group">  
<?php echo form_label("Address", "pt_l_name");?>
<?php echo form_input(array('type'=>'text', 'name'=>'pt_address','placeholder'=>'Address','class'=>'form-control','value'=>  Patient_m::get($patient_number, 'pt_address'))); ?>
    </div>

    
    <div class="row">
        <div class="col-md-4">
            <div class="form-group">  
        <?php echo form_label("Date of birth *", "pt_date_of_birth");?>
        <?php echo form_input(array('type'=>'date', 'name'=>'pt_date_of_birth','class'=>'form-control','value'=>  Patient_m::get($patient_number, 'pt_date_of_birth'))); ?>
            </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">  
        <?php echo form_label("Place of Birth*", "pt_place_of_birth");?>
        <?php echo form_input(array('type'=>'text', 'name'=>'pt_place_of_birth','placeholder'=>'place of birth','class'=>'form-control','value'=>  Patient_m::get($patient_number, 'pt_place_of_birth'))); ?>
          </div>  
        </div>
        <div class="col-md-4">
            <div class="form-group">  
        <?php echo form_label("Place of Residence*", "pt_place_of_residence");?>
        <?php echo form_input(array('type'=>'text', 'name'=>'pt_place_of_residence','placeholder'=>'Eg. New Tafo','class'=>'form-control','value'=>  Patient_m::get($patient_number, 'pt_place_of_residence'))); ?>
            </div>
        </div>
     
    </div>

    

    <div class="form-group">  
<?php echo form_label("Nationality*", "pt_nationality");?>
<?php echo form_input(array('type'=>'text', 'name'=>'pt_nationality','placeholder'=>'Eg. Ghanaian','class'=>'form-control','value'=>  Patient_m::get($patient_number, 'pt_nationality'))); ?>
    </div>

    <div class="form-group">  
<?php echo form_label("Mobile Number*", "pt_mobile");?>
<?php echo form_input(array('type'=>'text', 'name'=>'pt_mobile','placeholder'=>'Eg. 0267979001','class'=>'form-control','value'=>  Patient_m::get($patient_number, 'pt_mobile'))); ?>
    </div>
    <div class="form-group">  
<?php echo form_label("First Attendance*", "pt_first_attendance");?>
<?php echo form_input(array('type'=>'date', 'name'=>'pt_first_attendance','class'=>'form-control','value'=>  Patient_m::get($patient_number, 'pt_first_attendance'))); ?>
    </div>

    <div class="form-group">
        
        <!--NHIS fields starts here -->
        <div class="row">
            <h4>Health Insurance</h4>
            <div class="col-md-12">
                <div class="form-group">  
                    <div class="ui toggle checkbox">
                        <?php echo form_checkbox(array('id'=>'nhis','name'=>'nhis','class'=>'' ,'value'=>'true','data-on-text'=>'Available','data-off-text'=>'Unavailable')); ?>
                        <?php echo form_label("Is NHIS available ?", "nhis");?>    
                    </div>
                </div>  
            </div>
        </div>
            <div class="row">    
            <div id="nhis_details" style="display:none">
                <div class="col-md-4">
                    <div class="form-group">  
                <?php echo form_label("Membership Number *", "nhis_membership_number");?>
                <?php echo form_input(array('type'=>'text', 'name'=>'nhis_membership_number','placeholder'=>'Eg. 21755196','class'=>'form-control','value'=> set_value('nhis_membership_number',$this->input->post('nhis_membership_number')))); ?>
                    </div>
                </div>
            
                <div class="col-md-4">
                    <div class="form-group">  
                <?php echo form_label("Next Renewal *", "nhis_next_renewal");?>
                <?php echo form_input(array('type'=>'date', 'name'=>'nhis_next_renewal','placeholder'=>'Eg. 02/03/2003','class'=>'form-control','value'=> set_value('nhis_next_renewal',$this->input->post('nhis_next_renewal')))); ?>
                    </div>
                </div>
            <div class="col-md-4">
                <div class="form-group">  
            <?php echo form_label("Expiry Date *", "nhis_expiry_date");?>
            <?php echo form_input(array('type'=>'date', 'name'=>'nhis_expiry_date','placeholder'=>'Eg. 02/03/2009','class'=>'form-control','value'=> set_value('nhis_expiry_date',$this->input->post('nhis_expiry_date')))); ?>
                </div>
            </div>
            </div>
        </div>
    <!--NHIS fields end here -->
<?php 
echo form_input(array('type'=>'submit', 'name'=>'pt_submit',"value"=>'Save','class'=>'btn btn-success btn-lg'));
?>
    </div>
<?php echo form_close();?>
        
</div>
</div>
