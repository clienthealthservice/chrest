<?php 
    $patients= Patient_m::get_patients_general();
    $ci =& get_instance();
   ?>
<div>
   <a role="button" href="<?php echo site_url('/receptionist/add_patient') ?>" class="btn btn-warning pull-right"><i class="fa fa-wheelchair"></i> Add a Patient</a>
</div>

<table id="chres_table" class="table table-striped">
        <thead>
            <tr>
                <th>Name</th>
                 <th>NHIS Status</th>;
                <th>Patient Number</th>
                <th>Gender</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
<?php foreach($patients as $patient){?>
       <tr id="tr_<?php echo  $patient->pt_number ?>">
                  <td> <?php echo $patient->pt_first_name ." ". $patient->pt_last_name ?> </td>
                  <?php if(!empty($patient->next_renewal_date)&&$patient->next_renewal_date >date("Y-m-d")){?>
                    <td> <span class="label label-pill label-success">Active </span>
                <?php  }
                  if(!empty($patient->next_renewal_date)&& $patient->next_renewal_date <= date("Y-m-d")){?>
                     <td> <span class="label label-pill label-danger">Expired </span> </td>;
                  <?php };
                  if(empty($patient->next_renewal_date)){ ?>
                     <td><span class="label label-pill label-warning"> Not set </span></td>
                  <?php } ?> 
                  <td><?php echo $patient->pt_number?> </td>
                  <td><?php echo $patient->pt_gender?> </td>
                  
                 <td>
                     <div class='btn-group btn-group-sm' role='group' >
                         <a onclick='remove_patient("<?php echo $patient->pt_number ?>")' class='btn btn-danger'>Delete</a>
                         <a href='<?php echo site_url($ci->router->fetch_class()."/edit_patient/".$patient->pt_number)?>' class='btn btn-info'>Edit</a>
                         
                     </div>
                     <a class="btn btn-info" onclick="mark_patient_as_present('<?php echo $patient->pt_number ?>')"><i class="fa fa-book"></i> Consultation </a>
                 </td>
        </tr>
<?php } ?>
        </tbody>
  </table>

<!-- Modal -->
<div class="modal fade" id="editPatient" tabindex="-1" role="dialog" aria-labelledby="editPatientLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Edit Patient Info</h4>
      </div>
      <div class="modal-body">
        <form action="">
            <label for="patient_number">Patient Number</label>
            <input type="text" class="form-control" placeholder="Patient number">
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>